#!/bin/bash

containers_ids="$(docker ps -a | awk '{print $1}')"

ITER=0
for i in ${containers_ids[@]}; 
do
    container_name="$(docker ps -a | grep $i | awk '{print $NF}')"
    printf "${i}-${container_name}\n"
    docker cp $i:/StaBitLizer/StaBitLizer/settings.py ./settingsBackup/$container_name
    docker rm --force $i
    docker run -d -it --name $container_name stabitlizer:latest python3
    container_new_id="$(docker ps -a | grep $container_name | awk '{print $1}')"
    docker cp ./settingsBackup/$container_name $container_new_id:/StaBitLizer/StaBitLizer/settings.py
done

