#!/usr/bin/env python3.6

import os
import persistqueue
import utils
import telegram

PATH = os.path.dirname(os.path.abspath(__file__))

def main():

    ackqNEGMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVEMP')
    equalSize = 0
    bot = telegram.Bot(token=utils.getBotID())
    telegram_id = 423136569  
    telegram_id_2 = 13053900
    listYes = []
    listNo = []
   
    while ackqNEGMP.size > 0:
        try:
            uid = ackqNEGMP.get() 
            rep = utils.closePosSign(uid, 0)
            ackqNEGMP.ack(uid)
            print("USER: "+str(uid)+" NEGMP done")
            listYes.append(utils.getUserMail(uid))
        except:
            equalSize += 1
            if equalSize < 5:
                ackqNEGMP.nack(uid)
                print("USER: "+str(uid)+" NEGMP NOT done")
            else:
                ackqNEGMP.ack_failed(uid)
                equalSize = 0
                print("USER: "+str(uid)+" NEGMP NOT done ACK FAILED")
                listNo.append(utils.getUserMail(uid))

    if len(listYes) > 0 or len(listNo) > 0:
        message = buildMessage(listYes, listNo)
        bot.send_message(chat_id=telegram_id, text=message)
        bot.send_message(chat_id=telegram_id_2, text=message)

def buildMessage(listYes, listNo):

    if not listYes:
        addressesYes = "None"
    else:
        addressesYes = '\n\t\t'.join(map(str, listYes))
    if not listNo:
        addressesNo = "None"
    else:
        addressesNo = '\n\t\t'.join(map(str, listNo))
    message = "CLOSESIGN NEGMP COMPLETED:\n\n- Accounts CORRECTLY closed: \n\t\t"+addressesYes+"\n\n- Accounts FAILED at closing: \n\t\t"+addressesNo

    return message


  
if __name__== "__main__":
    main()
