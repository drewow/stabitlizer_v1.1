#!/usr/bin/env python3.6

import os
import persistqueue
import utils
import telegram

PATH = os.path.dirname(os.path.abspath(__file__))

def main():

    ackqNEU = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEUTRAL')
    equalSize = 0
    bot = telegram.Bot(token=utils.getBotID())
    telegram_id = 423136569  
    telegram_id_2 = 13053900
    listYes = []
    listNo = []

    while ackqNEU.size > 0:
        try:
            uid = ackqNEU.get() 
            rep = utils.closePosSign(uid, 0)
            ackqNEU.ack(uid)
            print("USER: "+str(uid)+" NEU done")
            listYes.append(utils.getUserMail(uid))
        except ValueError:
            equalSize += 1
            if equalSize < 5:
                ackqNEU.nack(uid)
                print("USER: "+str(uid)+" NEU NOT done")
            else:
                ackqNEU.ack_failed(uid)
                equalSize = 0
                print("USER: "+str(uid)+" NEU NOT done ACK FAILED")
                listNo.append(utils.getUserMail(uid))

    if len(listYes) > 0 or len(listNo) > 0:
        message = buildMessage(listYes, listNo)
        bot.send_message(chat_id=telegram_id, text=message)
        bot.send_message(chat_id=telegram_id_2, text=message)

def buildMessage(listYes, listNo):

    if not listYes:
        addressesYes = "None"
    else:
        addressesYes = '\n\t\t'.join(map(str, listYes))
    if not listNo:
        addressesNo = "None"
    else:
        addressesNo = '\n\t\t'.join(map(str, listNo))
    message = "CLOSESIGN NEU COMPLETED:\n\n- Accounts CORRECTLY closed: \n\t\t"+addressesYes+"\n\n- Accounts FAILED at closing: \n\t\t"+addressesNo

    return message


  
if __name__== "__main__":
    main()
