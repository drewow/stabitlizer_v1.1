import utils
import subprocess

users = utils.getAllAccounts()

for user in users:
    bashCommand2 = 'docker ps -a | grep '+str(user)
    process2 = subprocess.Popen(bashCommand2, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output2, error = process2.communicate()
    outputSecond = str(output2.decode('utf-8'))
    print(outputSecond)
