import os
import sys
import yaml
from pathlib import Path

PATH = os.path.dirname(os.path.abspath(__file__))

"""
# 	Load the MESSAGES file
"""

config_file = PATH+'/config.yaml'
my_file = Path(config_file)
if (my_file.is_file()):
    with open(config_file) as fp:
        config = yaml.load(fp)
else:
    print('config.yaml file does not exist. Please, make sure the file is present in the current directory')
    sys.exit()

MESSAGES = {}
MESSAGES['disclaimer'] = config['MESSAGES']['disclaimer']
MESSAGES['welcome'] = config['MESSAGES']['welcome']
MESSAGES['welcomeExt'] = config['MESSAGES']['welcomeExt']
MESSAGES['welcome1'] = config['MESSAGES']['welcome1']
MESSAGES['welcomeNOT'] = config['MESSAGES']['welcomeNOT']
MESSAGES['default'] = config['MESSAGES']['default']
MESSAGES['create'] = config['MESSAGES']['create']
MESSAGES['createNotPossible'] = config['MESSAGES']['createNotPossible']
MESSAGES['update'] = config['MESSAGES']['update']
MESSAGES['showing_id'] = config['MESSAGES']['showing_id']
MESSAGES['updateLeverage'] = config['MESSAGES']['updateLeverage']
MESSAGES['updateParams'] = config['MESSAGES']['updateParams']
MESSAGES['updateAccountNot'] = config['MESSAGES']['updateAccountNot']
MESSAGES['automatic'] = config['MESSAGES']['automatic']
MESSAGES['manual'] = config['MESSAGES']['manual']
MESSAGES['settingsInvalid'] = config['MESSAGES']['settingsInvalid']
MESSAGES['pairsGood'] = config['MESSAGES']['pairsGood']
MESSAGES['pairsInvalid'] = config['MESSAGES']['pairsInvalid']
MESSAGES['startGood'] = config['MESSAGES']['startGood']
MESSAGES['startInvalid'] = config['MESSAGES']['startInvalid']
MESSAGES['stepGood'] = config['MESSAGES']['stepGood']
MESSAGES['stepInvalid'] = config['MESSAGES']['stepInvalid']
MESSAGES['intervalGood'] = config['MESSAGES']['intervalGood']
MESSAGES['intervalInvalid'] = config['MESSAGES']['intervalInvalid']
MESSAGES['spreadGood'] = config['MESSAGES']['spreadGood']
MESSAGES['spreadInvalid'] = config['MESSAGES']['spreadInvalid']
MESSAGES['start'] = config['MESSAGES']['start']
MESSAGES['run'] = config['MESSAGES']['run']
MESSAGES['stop'] = config['MESSAGES']['stop']
MESSAGES['key_format_NOT'] = config['MESSAGES']['key_format_NOT']
MESSAGES['key_format_OK'] = config['MESSAGES']['key_format_OK']
MESSAGES['mail_format_NOT'] = config['MESSAGES']['mail_format_NOT']
MESSAGES['mail_format_OK'] = config['MESSAGES']['mail_format_OK']
MESSAGES['key_format_OK_upd'] = config['MESSAGES']['key_format_OK_upd']
MESSAGES['secret_format_NOT'] = config['MESSAGES']['secret_format_NOT']
MESSAGES['keysNotLegit'] = config['MESSAGES']['keysNotLegit']
MESSAGES['updateAlone'] = config['MESSAGES']['updateAlone']
MESSAGES['keysCorrect'] = config['MESSAGES']['keysCorrect']
MESSAGES['keysCorrect_upd'] = config['MESSAGES']['keysCorrect_upd']
MESSAGES['insufficientFunds'] = config['MESSAGES']['insufficientFunds']
MESSAGES['leverageType'] = config['MESSAGES']['leverageType']
MESSAGES['bitcoinNot'] = config['MESSAGES']['bitcoinNot']
MESSAGES['accountGoodCreation'] = config['MESSAGES']['accountGoodCreation']
MESSAGES['leverageNotGood'] = config['MESSAGES']['leverageNotGood']
MESSAGES['leverageNotGood2'] = config['MESSAGES']['leverageNotGood2']
MESSAGES['leverageGood'] = config['MESSAGES']['leverageGood']
MESSAGES['refreshCorrect'] = config['MESSAGES']['refreshCorrect']
MESSAGES['refreshNotCorrect'] = config['MESSAGES']['refreshNotCorrect']
MESSAGES['canceled'] = config['MESSAGES']['canceled']
MESSAGES['canceledCreate'] = config['MESSAGES']['canceledCreate']
MESSAGES['deleteNot'] = config['MESSAGES']['deleteNot']
MESSAGES['deleteConfirmation'] = config['MESSAGES']['deleteConfirmation']
MESSAGES['deleteYes'] = config['MESSAGES']['deleteYes']
MESSAGES['deleteNotNot'] = config['MESSAGES']['deleteNotNot']
MESSAGES['deleteNotValidCommand'] = config['MESSAGES']['deleteNotValidCommand']
MESSAGES['creditsNot'] = config['MESSAGES']['creditsNot']
MESSAGES['creditsOk'] = config['MESSAGES']['creditsOk']
MESSAGES['receiverExists'] = config['MESSAGES']['receiverExists']
MESSAGES['receiverNot'] = config['MESSAGES']['receiverNot']
MESSAGES['creditsSentOk'] = config['MESSAGES']['creditsSentOk']
MESSAGES['creditsSentNOK'] = config['MESSAGES']['creditsSentNOK']
MESSAGES['notifyNot'] = config['MESSAGES']['notifyNot']
MESSAGES['notifyConfirmation'] = config['MESSAGES']['notifyConfirmation']
MESSAGES['notifyYes'] = config['MESSAGES']['notifyYes']
MESSAGES['notifyNotNot'] = config['MESSAGES']['notifyNotNot']
MESSAGES['notifyNotValidCommand'] = config['MESSAGES']['notifyNotValidCommand']