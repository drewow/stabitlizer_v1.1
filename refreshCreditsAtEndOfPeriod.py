#!/usr/bin/env python3.6

import datetime
import logging
import os
import sys
import telegram
import utils

PATH = os.path.dirname(os.path.abspath(__file__))

"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(utils.getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=utils.getBotID())

def main():
    try:
        setOfUsers = utils.getLicenseRenewAccounts()
        logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+": routine to set credits to zero")
        print("DATE: " +str(datetime.datetime.today())+" - =======================================")
        for row in setOfUsers:
            user_id = row[0]
            print("=======================================")
            print("Setting credits to zero for user: "+str(user_id))
            statusKeys = utils.checkIfAPIKeysValid(user_id)
            if not statusKeys:
                continue
            utils.setActiveCreditsToZero(user_id)
            # Now, depending on the number of credits the user has in open credits, we need to update his active credits
            bitcoin = float(utils.fetch_balance(user_id)) / 1e8
            status, creds, balance, needed = utils.moveCreditsFromOpen(user_id, bitcoin)
            if status == True:
                bot.send_message(chat_id=user_id, text="The new monthly period starts now. In accordance with your current balance ("+str(balance)+" BTC), you have consumed "+str(creds)+" credits. You still have "+str(needed)+" unused credits")
                print("The new monthly period starts now. In accordance with your current balance ("+str(balance)+" BTC), you have consumed "+str(creds)+" credits. You still have "+str(needed)+" unused credits")
            else:
                bot.send_message(chat_id=user_id, text="WARNING: you don't have enough credits ("+str(creds)+") to run the bot with your current balance ("+str(balance)+" BTC). You need to purchase at least "+str(needed)+" more credits to launch the bot again. Please, contact you agent.")
                print("WARNING: you don't have enough credits ("+str(creds)+") to run the bot with your current balance ("+str(balance)+" BTC). You need to purchase at least "+str(needed)+" more credits to launch the bot again. Please, contact you agent.")

            print("=======================================")
        if len(setOfUsers) == 0:
            print("No users to renew today")
        sys.stdout.flush() 
    except ValueError:
        print("Problem while checking licenses")
  
if __name__== "__main__":
    main()
