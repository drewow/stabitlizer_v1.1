#!/usr/bin/env python3.6

import datetime
import json
import logging
import math
import os
import sys

import MySQLdb
import ccxt
import requests
import telegram

import utils

PATH = os.path.dirname(os.path.abspath(__file__))

def getBotID():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 5;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


def getBotMexDir():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 4;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=getBotID())


def getTypeOfNet():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT value FROM config WHERE id = 6;"
        cursor.execute(sql)
        result = cursor.fetchone()
        if int(result[0]) == 1:
            return False
        else:
            return True
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()

def getNotifAvail(user_id):

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT notifications FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        if int(result[0]) == 1:
            return 1
        else:
            return 0
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


def getBTCUSDprice():
    link = "https://api.coindesk.com/v1/bpi/currentprice/USD.json"
    data = json.loads(requests.get(link).text)
    priceBitcoin = float(data['bpi']['USD']['rate_float'])
    return priceBitcoin

def getSettings(user_id, sett):
    try:
        with open(PATH + '/' + str(user_id) + '/' + "settings.py", "rb") as file:
            content = file.readlines()
            for item in content:
                theKey = item.decode('utf-8').split("=", 1)[0]
                # logger.info("Value of theKey: "+theKey)
                try:
                    theValue = item.decode('utf-8').split("=", 1)[1]
                    # logger.info("Value of theValue: "+theValue)
                    if sett in theKey:
                        return theValue
                except:
                    continue

        #logger.info("Settings value correctly retrieved")
    except:
        logger.info("Settings value PROBLEM while retrieving")

def getCreditPrice():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT value FROM config WHERE id = 3;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


def getAvailableCredits(user_id):

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT credits FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        logger.info("Retrieving credits: "+str(int(result[0])))
        return int(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()

def getAllAccounts():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id FROM user_data;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()



def checkLicense(user_id):
    sett1 = 'API_KEY'
    sett2 = 'API_SECRET'
    apiKey = getSettings(user_id, sett1).replace('"', '').strip()
    apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

    try:
        bitmexx = ccxt.bitmex({
            'apiKey': apiKey,
            'secret': apiSecret,
        })

        # In case we have Keys from Testnet
        if getTypeOfNet():
            if 'test' in bitmexx.urls:
                bitmexx.urls['api'] = bitmexx.urls['test']

        bodyBalance = bitmexx.fetch_balance()

        # Funds in the account of the user and associated credits to the account
        funds = float(bodyBalance['BTC']['total'])
        logger.info("Current funds:"+str(funds))
        priceBitcoin = float(getBTCUSDprice())
        logger.info("Current price of BitCoind:"+str(priceBitcoin))
        creditPrice = float(utils.getCreditPrice())
        logger.info("Current creditPrice:"+str(creditPrice))

        creditsPotential = math.ceil(funds*priceBitcoin/creditPrice)
        logger.info("Credits associated to the current user according to his/her funds: " + str(creditsPotential))

        # Let's get how many credits the user has 
        user_credits = float(getAvailableCredits(user_id))

        # Let's check the distance between the current acquired credits and generated credits according to current funds
        if user_credits != 0:
            percentage = creditsPotential/user_credits
            creditsPlus = creditsPotential - user_credits;
        else:
            percentage = -1
            creditsPlus = 0

        logger.info("Percentage of deviation of license: "+str(percentage)+" for user: "+str(user_id))
        return (percentage, int(creditsPlus))

    except:
        logger.info("Exception when calling the licenseChecker")

def checkCredits(user_id):
    sett1 = 'API_KEY'
    sett2 = 'API_SECRET'
    apiKey = getSettings(user_id, sett1).replace('"', '').strip()
    apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

    try:
        bitmexx = ccxt.bitmex({
            'apiKey': apiKey,
            'secret': apiSecret,
        })

        # In case we have Keys from Testnet
        if getTypeOfNet():
            if 'test' in bitmexx.urls:
                bitmexx.urls['api'] = bitmexx.urls['test']

        # Let's get how many credits the user has 
        user_credits = float(getAvailableCredits(user_id))

        # Let's check the distance between the current acquired credits and generated credits according to current funds
        if user_credits == 0:
            creditsRenewed = 0
        else:
            creditsRenewed = 1

        return creditsRenewed

    except:
        logger.info("Exception when calling the checkCredits")


def main():
    try:
        setOfUsers = getAllAccounts()
        logger.info("\n")
        logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+": license checking routine")
        for row in setOfUsers:
            user_id = row[0]
            logger.info("=======================================")
            logger.info("Checking license for user: "+str(user_id))
            creditsRenewed = checkCredits(user_id)
            # Get current date 
            currentDate = datetime.datetime.now()
            # Delta for checking
            today = datetime.datetime.today()
            seventhDay = datetime.datetime(today.year, today.month, 7)
            value = currentDate > seventhDay

            if creditsRenewed == 0 and currentDate < seventhDay:
                logger.info(str(user_id)+": user did not update his credits for the ongoing month. :|")
                bot.send_message(chat_id=user_id, text="WARNING: You did not renew your credits for the ongoing month. You have until the 7th of the ongoing month to renew your credits. Otherwise, your account will be suspended.")
            elif creditsRenewed == 0 and currentDate > seventhDay:
                logger.info(str(user_id)+": user did not update his credits for the ongoing month ant deadline is passed. Account suspended :(")
                bot.send_message(chat_id=user_id, text="WARNING: you did not renew your credits before the 7th of the ongoing month. Account suspended. Please, contact your agent to renew your subscription.")
            else:
                logger.info(str(user_id)+": subscription correctly renewed and up to date")
            logger.info("=======================================")
        sys.stdout.flush() 
    except:
        print("Problem while checking licenses")
  
if __name__== "__main__":
    main()
