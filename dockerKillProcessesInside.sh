#!/bin/bash

        directory=$1

	docker exec $directory sh -c '
	for pid in $(ps -ef | awk '"'"'{print $2}'"'"'); do kill -9 $pid; done
	'
	ps -ef | grep defunct | grep -v grep | awk 'NR == 1 {print $3}' | xargs sudo kill -9
        sleep 3
	docker start $directory
