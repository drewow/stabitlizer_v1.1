#!/usr/bin/env python3.6
import binascii
import hashlib
from datetime import datetime
from datetime import timedelta

import requests
import MySQLdb
import math
import subprocess
import jwt
import os
import textVars
from flask import Flask, jsonify, make_response, request
import csv

import utils

app = Flask(__name__)

PATH = os.path.dirname(os.path.abspath(__file__))

@app.route('/signBias', methods=['POST'])
def signBias():
    try:
        name = request.get_data()
        stringArray = name.decode('utf-8').split(",")
        pp = float(stringArray[3])
        pn = float(stringArray[4])

        if len(stringArray) == 5:
            # First close sign
            if 'cn' in stringArray[2]:
                utils.closeSign('-')
            elif 'cp' in stringArray[2]:
                utils.closeSign('+')
            elif 'ca' in stringArray[2]:
                utils.closeSign('=')

            # Perk positive
            if pp <= 3 and pp > 0 and pn <= 3 and pn > 0:
                utils.changePerkForAll([pp, pn])

            print(str(name.decode('utf-8')))

            # Let's write to CSV
            BTCUSD = utils.getBTCUSDprice()
            row = [datetime.now().strftime("%Y-%m-%d %H:%M:%S"), BTCUSD, stringArray[0], stringArray[1], stringArray[2], pp, pn]
            with open('log.csv', 'a') as csvFile:
                writer = csv.writer(csvFile)
                writer.writerow(row)
            csvFile.close()

            return jsonify({'Message': "closeSign running, wait for a couple of minutes to complete "})

    except:
        return jsonify({'error': "invalid command"})


@app.route('/DEVSignBias', methods=['POST'])
def signBiasDEV():
    try:
        name = request.get_data()
        stringArray = name.decode('utf-8').split(",")
        pp = float(stringArray[3])
        pn = float(stringArray[4])

        if len(stringArray) == 5:
            print(str(name.decode('utf-8')))

            # Let's write to CSV
            BTCUSD = utils.getBTCUSDprice()
            row = [datetime.now().strftime("%Y-%m-%d %H:%M:%S"), BTCUSD, stringArray[0], stringArray[1], stringArray[2], pp, pn]
            with open('logDEV.csv', 'a') as csvFile:
                writer = csv.writer(csvFile)
                writer.writerow(row)
            csvFile.close()

            return jsonify({'Message': "signBiasDEV complete "})

    except:
        return jsonify({'error': "invalid command"})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
