#!/usr/bin/env python3.6

import os
import persistqueue
import utils
import telegram

PATH = os.path.dirname(os.path.abspath(__file__))

def main():

    ackqPOS = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVE')
    equalSize = 0
    bot = telegram.Bot(token=utils.getBotID())
    telegram_id = 423136569  
    telegram_id_2 = 13053900
    listYes = []
    listNo = []
   
    while ackqPOS.size > 0:
        try:
            uid = ackqPOS.get() 
            rep = utils.closePosSign(uid, +1)
            ackqPOS.ack(uid)
            print("USER: "+str(uid)+" POS done")
            listYes.append(utils.getUserMail(uid))
        except:
            equalSize += 1
            if equalSize < 5:
                ackqPOS.nack(uid)
                print("USER: "+str(uid)+" POS NOT done")
            else:
                ackqPOS.ack_failed(uid)
                equalSize = 0
                print("USER: "+str(uid)+" POS NOT done ACK FAILED")
                listNo.append(utils.getUserMail(uid))

    if len(listYes) > 0 or len(listNo) > 0:
        message = buildMessage(listYes, listNo)
        bot.send_message(chat_id=telegram_id, text=message)
        bot.send_message(chat_id=telegram_id_2, text=message)

def buildMessage(listYes, listNo):

    if not listYes:
        addressesYes = "None"
    else:
        addressesYes = '\n\t\t'.join(map(str, listYes))
    if not listNo:
        addressesNo = "None"
    else:
        addressesNo = '\n\t\t'.join(map(str, listNo))
    message = "CLOSESIGN POS COMPLETED:\n\n- Accounts CORRECTLY closed: \n\t\t"+addressesYes+"\n\n- Accounts FAILED at closing: \n\t\t"+addressesNo

    return message

  
if __name__== "__main__":
    main()
