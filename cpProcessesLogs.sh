#!/bin/bash

fileName1="RES1.dat"
fileName2="RES2.dat"
fileName3="NOK1.dat"
fileName4="NOK2.dat"
fileName5="OK.dat"
fileName6="EXEC.dat"

python2 ./uploadProcessesLogs.py ${fileName1}
python2 ./uploadProcessesLogs.py ${fileName2}
python2 ./uploadProcessesLogs.py ${fileName3}
python2 ./uploadProcessesLogs.py ${fileName4}
python2 ./uploadProcessesLogs.py ${fileName5}
python2 ./uploadProcessesLogs.py ${fileName6}
