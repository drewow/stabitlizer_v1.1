#!/bin/bash

        directory=$1

        docker exec $directory sh -c " 	
        if ! screen -list | grep -q $directory; then
            cd 'StaBitLizer'
	    #screen -L -Sdm $directory ./scriptRunMM.sh $directory
	    ./scriptRunMM.sh $directory
        else
	    echo 'Screen session already running'
        fi
        "
