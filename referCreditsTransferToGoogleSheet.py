#!/usr/bin/env python3.6

import datetime

import gspread
from oauth2client.service_account import ServiceAccountCredentials

import utils


def main():

    scope = ['https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/StaBitLizer_v1.1/client_secret.json', scope)
    client = gspread.authorize(creds)

    # Open a worksheet from spreadsheet with one shot
    sheet = client.open("SBLAccountStatusSummary")
    worksheet2 = sheet.worksheet('CreditTransferLog')

    transactions = utils.getAllTransfers()
    cells = []
    row_num = 1
    for transaction in transactions:
        f = '%Y-%m-%d %H:%M:%S'
        
        dateTransaction = str(transaction[1])
        dateTransaction = datetime.datetime.strptime(dateTransaction, f)
        
        sender_id = transaction[2]
        mail_sender = utils.getUserMail(sender_id)
       
        receiver_id = transaction[3]
        mail_receiver = utils.getUserMail(receiver_id)

        credits = transaction[4]

        cells.append(gspread.Cell(row_num + 1, 1, str(dateTransaction)))
        cells.append(gspread.Cell(row_num + 1, 2, mail_sender))
        cells.append(gspread.Cell(row_num + 1, 3, mail_receiver))
        cells.append(gspread.Cell(row_num + 1, 4, str(credits)))


        row_num += 1

    worksheet2.update_cells(cells)

if __name__ == '__main__':
    main()
