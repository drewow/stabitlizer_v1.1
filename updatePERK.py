#!/usr/bin/env python3.6

import os
import time
import sys
import yaml
import ccxt
import subprocess
import logging
import bitmex
import json
import string
import random
import requests
import math
from pathlib import Path
import datetime
import MySQLdb
import telegram
import utils
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from telegram import MessageEntity, TelegramObject, ChatAction

PATH = os.path.dirname(os.path.abspath(__file__))


def perkUpd(user_id):

    try:

        dictChange = {
            'PERK':'[1, 1]'
        }
        utils.updateSettings(user_id, dictChange)

    except ValueError:
        print("Exception when calling PositionApi->position_update_leverage")

def main():
    setOfUsers = utils.getAllAccounts()
    userNum = 0
    for row in setOfUsers:
        try:
            user_id = row[0]
            sett1 = 'PERK'
            userNum += 1
            if True:#perkInt == -2:
                print("--------------------------------------")
                print("Checking perk for user: "+str(user_id))
                print("Updating for user: "+str(user_id))
                perkUpd(user_id)
                print("Now with user: "+str(userNum))
                print("--------------------------------------")
            else:
                print("--------------------------------------")
                print("Perk for for user NOT -2: "+str(user_id))
                print("--------------------------------------")

        except:
            print("Continuing for user: "+str(user_id))
            print("Now with user: "+str(userNum))
            print("--------------------------------------")
            continue
    sys.stdout.flush() 
  
if __name__== "__main__":
    main()
