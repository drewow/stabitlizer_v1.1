#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

from pathlib import Path
import subprocess
import logging
import telegram
import datetime
import os
import sys
import yaml
import requests
import shutil
import math
import threading
import utils
import persistqueue

from time import sleep

PATH = os.path.dirname(os.path.abspath(__file__))


"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
logger.info("Running "+sys.argv[0])

"""
# 	Load the MESSAGES file
#	Set botname / token
"""

config_file = PATH+'/config.yaml'
my_file = Path(config_file)
if (my_file.is_file()):
    with open(config_file) as fp:
        config = yaml.load(fp)
else:
    pprint('config.yaml file does not exist. Please, make sure the file is present in the current directory')
    sys.exit()

MESSAGES = {}
MESSAGES['disclaimer'] = config['MESSAGES']['disclaimer']
MESSAGES['welcome'] = config['MESSAGES']['welcome']
MESSAGES['welcomeExt'] = config['MESSAGES']['welcomeExt']
MESSAGES['welcome1'] = config['MESSAGES']['welcome1']
MESSAGES['welcomeNOT'] = config['MESSAGES']['welcomeNOT']
MESSAGES['default'] = config['MESSAGES']['default']
MESSAGES['create'] = config['MESSAGES']['create']
MESSAGES['createNotPossible'] = config['MESSAGES']['createNotPossible']
MESSAGES['update'] = config['MESSAGES']['update']
MESSAGES['showing_id'] = config['MESSAGES']['showing_id']
MESSAGES['updateLeverage'] = config['MESSAGES']['updateLeverage']
MESSAGES['updateParams'] = config['MESSAGES']['updateParams']
MESSAGES['updateAccountNot'] = config['MESSAGES']['updateAccountNot']
MESSAGES['automatic'] = config['MESSAGES']['automatic']
MESSAGES['manual'] = config['MESSAGES']['manual']
MESSAGES['settingsInvalid'] = config['MESSAGES']['settingsInvalid']
MESSAGES['pairsGood'] = config['MESSAGES']['pairsGood']
MESSAGES['pairsInvalid'] = config['MESSAGES']['pairsInvalid']
MESSAGES['startGood'] = config['MESSAGES']['startGood']
MESSAGES['startInvalid'] = config['MESSAGES']['startInvalid']
MESSAGES['stepGood'] = config['MESSAGES']['stepGood']
MESSAGES['stepInvalid'] = config['MESSAGES']['stepInvalid']
MESSAGES['intervalGood'] = config['MESSAGES']['intervalGood']
MESSAGES['intervalInvalid'] = config['MESSAGES']['intervalInvalid']
MESSAGES['spreadGood'] = config['MESSAGES']['spreadGood']
MESSAGES['spreadInvalid'] = config['MESSAGES']['spreadInvalid']
MESSAGES['start'] = config['MESSAGES']['start']
MESSAGES['run'] = config['MESSAGES']['run']
MESSAGES['stop'] = config['MESSAGES']['stop']
MESSAGES['key_format_NOT'] = config['MESSAGES']['key_format_NOT']
MESSAGES['key_format_OK'] = config['MESSAGES']['key_format_OK']
MESSAGES['mail_format_NOT'] = config['MESSAGES']['mail_format_NOT']
MESSAGES['mail_format_OK'] = config['MESSAGES']['mail_format_OK']
MESSAGES['key_format_OK_upd'] = config['MESSAGES']['key_format_OK_upd']
MESSAGES['secret_format_NOT'] = config['MESSAGES']['secret_format_NOT']
MESSAGES['keysNotLegit'] = config['MESSAGES']['keysNotLegit']
MESSAGES['updateAlone'] = config['MESSAGES']['updateAlone']
MESSAGES['keysCorrect'] = config['MESSAGES']['keysCorrect']
MESSAGES['keysCorrect_upd'] = config['MESSAGES']['keysCorrect_upd']
MESSAGES['insufficientFunds'] = config['MESSAGES']['insufficientFunds']
MESSAGES['leverageType'] = config['MESSAGES']['leverageType']
MESSAGES['bitcoinNot'] = config['MESSAGES']['bitcoinNot']
MESSAGES['accountGoodCreation'] = config['MESSAGES']['accountGoodCreation']
MESSAGES['leverageNotGood'] = config['MESSAGES']['leverageNotGood']
MESSAGES['leverageNotGood2'] = config['MESSAGES']['leverageNotGood2']
MESSAGES['leverageGood'] = config['MESSAGES']['leverageGood']
MESSAGES['refreshCorrect'] = config['MESSAGES']['refreshCorrect']
MESSAGES['refreshNotCorrect'] = config['MESSAGES']['refreshNotCorrect']
MESSAGES['canceled'] = config['MESSAGES']['canceled']
MESSAGES['canceledCreate'] = config['MESSAGES']['canceledCreate']
MESSAGES['deleteNot'] = config['MESSAGES']['deleteNot']
MESSAGES['deleteConfirmation'] = config['MESSAGES']['deleteConfirmation']
MESSAGES['deleteYes'] = config['MESSAGES']['deleteYes']
MESSAGES['deleteNotNot'] = config['MESSAGES']['deleteNotNot']
MESSAGES['deleteNotValidCommand'] = config['MESSAGES']['deleteNotValidCommand']
MESSAGES['creditsNot'] = config['MESSAGES']['creditsNot']
MESSAGES['creditsOk'] = config['MESSAGES']['creditsOk']
MESSAGES['receiverExists'] = config['MESSAGES']['receiverExists']
MESSAGES['receiverNot'] = config['MESSAGES']['receiverNot']
MESSAGES['creditsSentOk'] = config['MESSAGES']['creditsSentOk']
MESSAGES['creditsSentNOK'] = config['MESSAGES']['creditsSentNOK']
MESSAGES['notifyNot'] = config['MESSAGES']['notifyNot']
MESSAGES['notifyConfirmation'] = config['MESSAGES']['notifyConfirmation']
MESSAGES['notifyYes'] = config['MESSAGES']['notifyYes']
MESSAGES['notifyNotNot'] = config['MESSAGES']['notifyNotNot']
MESSAGES['notifyNotValidCommand'] = config['MESSAGES']['notifyNotValidCommand']
MAIL, KEY, SECRET, LEVERAGE, DELETE, RECEIVER, CREDITS, NOTIFY, POSCLOSES, VOLATILITY, SPREAD_MIN, LOOP_INTERVAL, POSSIGN, POSSIGNALONE, PERK, PERKU, PERKI, PERKII, OPENKI, MIGRATE, PASSONE, PASSCONF, NOTIFYALL, OSS, OSST = range(25)



# User dict for storing key-value users (user.id)
user_dict = {}

# Dict for storing transactions
transaction_dict = {}

# Dict for storing transactions
upd_dict = {}

# Dict for storing settings
sett_dict = {}

# User class for storing user data
class User:
    """
    A class used to represent a User

    ...

    Attributes
    ----------
    mail : str
        the email of the new user
    key : str
        the API key
    secret : str
        the secret key of the API
    bitcoin : int
        user balance in his BitMex account
    start : int
        ORDER_START_SIZE in settings.py of given user
    step : int
        ORDER_STEP_SIZE in settings.py of given user
    """
    def __init__(self, mail=None, key=None, passw=None):
        self.mail = mail
        self.key = key
        self.secret = None
        self.bitcoin = None
        self.start = None
        self.step = None
        self.passw = passw

# User class for storing credit transactions
class Transaction:
    def __init__(self, key):
        self.receiver_id = key
        self.credits = None

# User class for storing user data
class Update:
    def __init__(self, key):
        self.order_pairs = key
        self.order_start_size = None
        self.order_step_size = None
        self.interval = None
        self.min_spread = None

# User class for storing user data
class Sett:
    def __init__(self, perk=None, perk2=None):
        self.perk = perk
        self.perk2 = perk2
        self.op = None
        self.mail = None


############################
# Begin bot
bot = telegram.Bot(token=utils.getBotID())

# Bot error handler
def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))

###########################
# Begin Bot Commands

def start(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    logger.info("/start - "+user_name)

    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        textToParse = MESSAGES['welcomeNOT'] % user_name
        textToParse2 = MESSAGES['disclaimer']
        textToParse_ = "NOTE: by creating your account you accept the disclaimer and terms of use of StaBitLizer"
        logger.info(textToParse)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse2, parse_mode="HTML", disable_web_page_preview=1)
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse_, reply_markup=reply_markup)
    else:
        if userExists == 1:
            textToParse = MESSAGES['welcome1'] % user_name
            textToParse2 = MESSAGES['disclaimer']
            logger.info(textToParse)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse2, parse_mode="HTML", disable_web_page_preview=1)
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        else:
            textToParse = MESSAGES['welcome'] % user_name
            textToParse2 = MESSAGES['disclaimer']
            logger.info(textToParse)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse2, parse_mode="HTML", disable_web_page_preview=1)
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)

def welcome(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    logger.info("/welcome - "+user_name)
	
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        textToParse = MESSAGES['welcomeNOT'] % user_name
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
    else:
        if userExists == 1:
            textToParse = MESSAGES['welcome1'] % user_name
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        elif (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
            textToParse = MESSAGES['welcomeExt'] % user_name
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        else:
            textToParse = MESSAGES['welcome'] % user_name
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)


    logger.info(textToParse)

def default(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    logger.info("Command not found, running default - "+user_name)
	
    textToParse = MESSAGES['default'] % user_name

    userExists = utils.checkIfreceiverIdExists(user_id)

    logger.info(textToParse)

    if userExists == 0:
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text='Sorry. Your command is not valid.', parse_mode="HTML", disable_web_page_preview=1)
        textToParse = MESSAGES['welcomeNOT'] % user_name
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1,reply_markup=reply_markup)
    else:
        if userExists == 1:
            textToParse = MESSAGES['welcome1'] % user_name
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        else:
            textToParse = MESSAGES['welcome'] % user_name
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)

def run(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to start the bot but his/her profile does not exist: "+str(user_id))
        textToParse = "Sorry, you can't run the bot because your profile does not exist yet. Please, use /create to open an account in StaBitLizer."
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
    elif userExists == 1:
        logger.info("User "+user_name+" willing to start the bot but API keys were not set up before: "+str(user_id))
        textToParse = "Sorry, you can't run the bot because you did not set up your API keys yet. Please, use /updateKeys to configure the API keys of your bot."
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
    else:
        logger.info("/run: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "Initializing your bot"
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1)

        active_credits = utils.getCreditsActive(user_id)

        # Let's start the process and inform the user
        if active_credits > 0:
            execSh = PATH+'/start '+str(user_id)
            utils.refreshStopState(user_id, 0)
            process2 = subprocess.Popen(execSh.split(), stdout=subprocess.PIPE)
            output2, error = process2.communicate()
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text=str(output2.decode('utf-8')), parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        else:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            msg = bot.sendMessage(chat_id=chat_id, text="ALERT: you can't run your bot because you don't have enough credits. Please, type /credits for further information, and /refresh in case you have unused credits in order to launch your bot successfully. If you have more doubts, please, contact your agent. Thank you.", parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)

    
def stop(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to stop the bot but his/her profile does not exist: "+str(user_id))
        textToParse = "Sorry, you can't stop the bot because your profile does not exist yet. Please, use /create to open an account in StaBitLizer."
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        return ConversationHandler.END
    elif userExists == 1:
        logger.info("User "+user_name+" willing to stop the bot but he/she did not set up his/her API keys yet: "+str(user_id))
        textToParse = "Sorry, you can't stop the bot because you didn't setup your API keys yet. Please, use /updateKeys to configure the API keys of your bot. Thank you."
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/stop: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "One sec..."
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1)
        
        bashCommand2 = PATH + '/status '+str(user_id)
        process2 = subprocess.Popen(bashCommand2.split(), stdout=subprocess.PIPE)
        output2, error = process2.communicate()

        if 'not currently running' in str(output2) or 'overloaded' in str(output2):
            textToParse = "Bot not running. No need to stop"
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)
            return ConversationHandler.END
        else:
            # Let's start the process and inform the user
            execSh = PATH+'/kill '+str(user_id)
            rep = utils.closePos(user_id)
            subprocess.call([execSh], shell=True)
            textToParse = "Bot stopped successfully. Do you want to close open positions too?"
            custom_keyboard = [['Yes','No']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse, reply_markup=reply_markup)
            logger.info("POSCLOSE")
            return POSCLOSES


def stopConfirm(bot, update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    message = str(update.message.text)

    if (message.lower() == 'yes' or message.lower() == 'y'):
        logger.info("Close positions after stopping bot, as decided by user "+str(user_id))
        
        rep = utils.closePosYES(user_id)
        
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        if rep == -2:
            update.message.reply_text('ERROR: BitMex blocked the access to this account and it could not be stopped',reply_markup=reply_markup)
        elif rep == -1:
            update.message.reply_text('ERROR: API keys are not valid, nothing done',reply_markup=reply_markup)
        elif rep == 0:
            update.message.reply_text('ERROR: your account was not created yet. Please, type /create to proceed.',reply_markup=reply_markup)
        else:
            update.message.reply_text('Bot stopped and positions closed successfully',reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Positions not closed, as decided by user "+str(user_id))
        
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text('Bot stopped (positions not closed)',reply_markup=reply_markup)
        return ConversationHandler.END

def closePositionsAccordingToSign(bot, update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    mailUsed = utils.getUserMail(user_id)

    if (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        custom_keyboard = [['-','=','+'],['-MP','+MP'],['/cancel']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text('Please, select below the sign of positions you want to close now. REMEMBER, this CAN\'T be undone.',reply_markup=reply_markup)
        return POSSIGN
    else:
        custom_keyboard = [['Yes','No'],['/cancel']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text('You are about to revert your positions now. REMEMBER, this CAN\'T be undone. Are you sure?',reply_markup=reply_markup)
        return POSSIGNALONE


def closePosSignConfirm(bot, update):

    threads = []
    t = threading.Thread(target=parallelCloseSign, args=(bot, update))
    threads.append(t)
    t.start()

def closePosSignAlone(bot, update):

    logger.info("CLOSEPOSSIGNALONE")
    user_id = update.message.from_user.id
    user_id = utils.getMainID(user_id)

    message = str(update.message.text)
    update.message.reply_text('Closing positions. Please, wait.')

    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    if message.lower() == 'yes':
        logger.info("REVERTING")
        sign = utils.getCurrentPosSign(user_id)
        utils.closePosSign(user_id, sign)
        logger.info("Closing for user "+str(user_id)+" and sign: "+str(sign))
        update.message.reply_text('ATTENTION: your positions were reverted',reply_markup=reply_markup)
    else:
        logger.info("NOT REVERTING")
        update.message.reply_text('NOTE: your positions WERE NOT reverted',reply_markup=reply_markup)
    return ConversationHandler.END


def create(bot,update):

    telegram_id = update.message.from_user.id

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id

    # In order to create the account, funds must be greater than 0

    userExists = utils.checkIfTelegramIdExists(telegram_id)

    #if userExists == 0 or userExists == 1:
    if userExists == 0:
        logger.info("Profile folder for user "+user_name+" correctly created, with Telegram ID "+str(telegram_id))
        textToParse = MESSAGES['create'] % user_name
        update.message.reply_text(textToParse)
        return MAIL
    else:
        logger.info("Profile folder for user "+user_name+" already exists, with Telegram ID "+str(telegram_id))
        textToParse = MESSAGES['createNotPossible']
        update.message.reply_text(textToParse)
        return ConversationHandler.END

def mail(bot,update):
    
    telegram_id = update.message.from_user.id

    mailMsg = str(update.message.text)
    apiKey = "secret_bf2db269199cbeca643e8126a3564a27"
    user_name = utils.get_name(update.message.from_user)
    mailExists = utils.checkIfMailExists(mailMsg)

    r = requests.post("https://api.neverbounce.com/v4/single/check?key="+apiKey+"&email="+mailMsg+"").json()
    success = r['result']

    if 'invalid' in success:
        textToParse = MESSAGES['mail_format_NOT']
        update.message.reply_text(textToParse)
        logger.info("Format of mail not valid: %s ", mailMsg)
        return MAIL
    elif mailExists == True:
        textToParse = "This mail address already exists in StaBitLizer. NOTE: in case this mail address belongs to you (you might have already created the account via API interface), you can link it to your Telegram account. Click YES if you wish to do so, NO to type a different email. Thank you."
        custom_keyboard = [['Yes', 'No']]
        user = User(mail=mailMsg)
        user_dict[telegram_id] = user
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        logger.info("Mail already exists: %s ", mailMsg)
        return MIGRATE
    else:
        textToParse = MESSAGES['mail_format_OK']
        utils.createAccountDB(mailMsg, telegram_id)
        update.message.reply_text(textToParse)
        update.message.reply_text("GOOD: Your account was correctly created")
        utils.sendVerificationMail(mailMsg)
        textToParse = "IMPORTANT: An email with a verification link has been sent to the address given before. Click on that link to complete the account creation. Thank you."
        update.message.reply_text(textToParse)
        logger.info("Format of mail valid ( %s ) - Account in state 1 created", update.message.text)
        return ConversationHandler.END

def migrate(bot,update):

    telegram_id = update.message.from_user.id
    user = user_dict[telegram_id]
    mail = user.mail
    user_id = utils.getUserID(mail)

    message = str(update.message.text)

    if (message.lower() == 'yes' or message.lower() == 'y'):
        logger.info("Profile migrated by user "+str(user_id))
        utils.sendMigrationMail(mail, telegram_id, user_id)
        textToParse = "An email with a migration link has been sent to the address given before. Click on that link to complete the migration. Thank you."
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Profile NOT migrated by user " + str(user_id))
        textToParse = "Please, enter a different email."
        update.message.reply_text(textToParse)
        return MAIL

def key(bot,update):
    
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    API_KEY = update.message.text

    if len(API_KEY) != 24:
        textToParse = MESSAGES['key_format_NOT']
        update.message.reply_text(textToParse)
        logger.info("Format of API_KEY not valid - len ( %s ) not equal to 24 chars", update.message.text)
        return KEY
    else:
        textToParse = MESSAGES['key_format_OK']
        user = user_dict[user_id]
        user.key = API_KEY
        update.message.reply_text(textToParse)
        update.message.reply_text('NOTE: in case you need further assistance, please, visit following link on how to get your API keys from BitMex: https://www.dropbox.com/s/yuk0mhuyn7303pc/API%20KEYS%20EXTRACTION.pdf?dl=0')
        logger.info("Format of API_KEY valid ( %s ) - Proceeding to ask user for API_SECRET", update.message.text)
        return SECRET

def secret(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    API_SECRET = update.message.text

    if len(API_SECRET) != 48:
        textToParse = MESSAGES['secret_format_NOT'] 
        update.message.reply_text(textToParse)
        logger.info("Format of API_SECRET not valid - not equal to 48 chars")
        return SECRET
    else:
        user = user_dict[user_id]
        user.secret = API_SECRET
        logger.info("Format of API_SECRET valid ( %s ) - Proceeding to check validity of API_KEY and API_SECRET in BitMex API", update.message.text)
        logger.info("Value of current dict: %s and %s", user.key, user.secret)

        # Now we need to check if the data provided is legit
        try:

            funds = utils.fetch_balanceWithKeys(user.key, user.secret)
            logger.info("This is the balance of the user: "+str(funds))
            if int(funds) <= 0:
                textToParse = MESSAGES['insufficientFunds']
                update.message.reply_text(textToParse)
            user.bitcoin = str(funds)
            textToParse = MESSAGES['keysCorrect']
            update.message.reply_text(textToParse)
            return LEVERAGE

        except:
            logger.info("Keys provided NOT legit")
            textToParse = MESSAGES['keysNotLegit']
            update.message.reply_text(textToParse)
            textToParse = MESSAGES['updateAlone']
            update.message.reply_text(textToParse)
            return KEY


def deleteProf(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id

    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("Profile folder didn't exist before, cannot delete: "+str(user_id))
        textToParse = MESSAGES['deleteNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        #msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Profile folder exists, let's ask user confirmation: "+str(user_id))
        textToParse = MESSAGES['deleteConfirmation']
        custom_keyboard = [['Yes', 'No']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return DELETE

def deleteConf(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    message = str(update.message.text)

    if (message.lower() == 'yes' or message.lower() == 'y'):
        logger.info("Profile deleted, as decided by user "+str(user_id))

        utils.deleteContainerForUser(user_id)

        # Let's remove from database
        utils.removeFromDB(user_id)
        textToParse = MESSAGES['deleteYes']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    elif (message.lower() == 'no' or message.lower() =='n'):
        logger.info("Profile NOT deleted, as decided by user "+str(user_id))
        textToParse = MESSAGES['deleteNotNot']
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Profile NOT deleted, as decided by user "+str(user_id))
        textToParse = MESSAGES['deleteNotValidCommand']
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END

def sendCredits(bot,update):
    """Used to send credits to other user.

    Parameters
    ----------
    update : obj
        state update variable from Telegram
    """
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id

    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("Profile folder didn't exist before, can't send credits: "+str(user_id))
        textToParse = MESSAGES['creditsNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Profile folder exists, let's ask user how many credits and destination: "+str(user_id))
        textToParse = MESSAGES['creditsOk']
        update.message.reply_text(textToParse)
        return RECEIVER

def receiver(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    mail = str(update.message.text)
    receiver_id = utils.checkIfreceiverMailExists(mail)
 
    if (receiver_id is not None):
        transaction = Transaction(receiver_id)
        transaction_dict[user_id] = transaction
        logger.info("Receiver id "+str(receiver_id)+" exists in StaBitLizer")
        textToParse = MESSAGES['receiverExists']
        update.message.reply_text(textToParse)
        return CREDITS
    else:
        logger.info("Receiver mail does not exist. Try again")
        textToParse = MESSAGES['receiverNot']
        update.message.reply_text(textToParse)
        return RECEIVER

def credits(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    creditsToSend = str(update.message.text)
    receiver_id = transaction_dict[user_id].receiver_id
    rec_telegram_id = utils.getTelegramID(receiver_id)
    user_name = 'Dear User'
    chat_id = update.message.chat.id
    creditsInAccount = utils.getAvailableCredits(user_id)
    mail = utils.getUserMail(user_id)
    mailMsg = utils.getUserMail(receiver_id)

    if (int(creditsInAccount) >= int(creditsToSend)):
        logger.info("Correct amount of credits to send")
        textToParse = MESSAGES['creditsSentOk']
        userHasCredits = utils.checkIfUserHasCredits(receiver_id)
        utils.sendCreditsToDB(user_id, receiver_id, creditsToSend)
        update.message.reply_text(textToParse)
        creditsInAccountRec = utils.getAvailableCredits(receiver_id)
        accountState = int(utils.getAccountState(receiver_id))
        logger.info("The state is: "+str(accountState))

        if (accountState < 2):
            # After receiving credits, the bot is properly created
            user = User(mail=mailMsg)
            user_dict[receiver_id] = user
            logger.info('Entering threading part')
            apiKey = 'ss'
            apiSecret = 'ss'

            threads = []
            t = threading.Thread(target=workerFunc, args=(apiKey, apiSecret, receiver_id, user_name, user, bot, update))
            threads.append(t)
            t.start()

            logger.info("-----REACHED PART WHERE WORKERS SPLIT--------------")

        bot.send_message(chat_id=rec_telegram_id, text="Congratulations! You just received "+str(creditsToSend)+" credits from user "+str(mail))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        bot.send_message(chat_id=rec_telegram_id, text="Your current balance is: "+str(creditsInAccountRec)+" credits",reply_markup=reply_markup)
        if userHasCredits == 0:
            bot.send_message(chat_id=rec_telegram_id, text="NOTE: in order to run your bot, you should now set up your API keys by typing /updateKeys command and getting them from https://bitmex.com",reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Invalid amount of credits to send")
        textToParse = MESSAGES['creditsSentNOK']
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return CREDITS

def getCredits(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    balance = float(utils.fetch_balance(user_id))/1e8
    credits_open = utils.getCreditsOpen(user_id)
    credits_active = utils.getCreditsActive(user_id)
    credits_needed = math.ceil(balance * 10)
    additional_credits = credits_needed - credits_active
    creds_to_buy = credits_needed - credits_open
   

    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)

    formatDate = '%Y-%m-%d %H:%M:%S'
    creation_date = datetime.datetime.strptime(utils.getAccountCreationDate(user_id),formatDate)
    current_date = datetime.datetime.now()
    #TODO Replace 30 by the real number of days until the next license revision
    pendingDays = (creation_date.day - current_date.day) % 30
    if pendingDays == 0:
        pendingDays = 30

    if credits_open is None:
        update.message.reply_text("Your account does not exist yet in StaBitLizer (no credits). Type /create to set it up. Thank you.", reply_markup=reply_markup)
    else:
        if additional_credits > 0:
            if additional_credits <= credits_open:
                update.message.reply_text("Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. You can type /refresh to activate unused credits in your account and catch up with your balance (you still have "+str(pendingDays)+" days remaining until the current license period expires)", reply_markup=reply_markup)
            else:
                update.message.reply_text("Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. You don't have enough credit reserves to catch up with your current balance. Please, contact your agent to acquire at least "+str(creds_to_buy)+" credits to match your current balance (you still have "+str(pendingDays)+" remaining days until the current license period expires). NOTE: due to volatily, you might need to buy more or less credits as pointed here in order to have enough reserves in case your balance suddenly increases.", reply_markup=reply_markup)
        else:
            update.message.reply_text("Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. No need to refresh. NOTE: you still have "+str(pendingDays)+" remaining days until the current license period expires.", reply_markup=reply_markup)

def getId(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    mail = utils.getUserMail(user_id)

    logger.info("Showing email for user "+str(user_id))
    textToParse = MESSAGES['showing_id'] % mail
    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text(textToParse, reply_markup=reply_markup)

def updateKeys(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
   
    userExists = utils.checkIfreceiverIdExists(user_id)
    credits = utils.checkIfUserHasCredits(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    elif credits == 0:
        logger.info("User "+user_name+" willing to update settings but doesnt have credits: "+str(user_id))
        textToParse = 'WARNING: In order to update your API keys, you need credits. Please, purchase them by contacting your agent. Thank you.'
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/update: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = MESSAGES['update'] % user_name
        update.message.reply_text(textToParse)
        return KEY

def updatePerkAndOP(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id

    if (mailUsed != "juanba.tomas@gmail.com" and mailUsed != "Julian.b@bitcoin.org.hk"):
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/update: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "You will now update PERK and ORDER_PAIRS parameters in settings for ALL users. Beware that this cannot be undone."
        update.message.reply_text(textToParse)
        textToParse = "(1/4) Please, enter BUY PERK value first..."
        update.message.reply_text(textToParse)
        return PERKI

def perkUpdRow(bot,update):
    
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    perkValue = update.message.text

    if abs(float(perkValue)) > 3:
        textToParse = "BUY Perk value must be between 0 and 3. Please, try again..."
        update.message.reply_text(textToParse)
        logger.info("Format of PERK BUY not valid - len ( %s ) not equal to 24 chars", update.message.text)
        return PERKI
    else:
        textToParse = "(2/4) Good. Now enter SELL PERK value..."
        sett = Sett(perk=perkValue)
        sett_dict[user_id] = sett
        update.message.reply_text(textToParse)
        logger.info("Format of PERK BUY valid ( %s ) - Proceeding to ask user for SELL PERK", update.message.text)
        return PERKII


def perkUpdRow2(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    perkValue = update.message.text

    if abs(float(perkValue)) > 3:
        textToParse = "SELL Perk value must be between 0 and 3. Please, try again..."
        update.message.reply_text(textToParse)
        logger.info("Format of PERK SELL not valid - len ( %s ) not equal to 24 chars", update.message.text)
        return PERKII
    else:
        textToParse = "(3/4) Good. Now enter ORDER_PAIRS value."
        sett = sett_dict[user_id]
        sett.perk2 = perkValue
        sett_dict[user_id] = sett
        update.message.reply_text(textToParse)
        logger.info("Format of PERK SELL valid ( %s ) - Proceeding to ask user for ORDER_PAIRS", update.message.text)
        return OPENKI

def opUpdRow(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    openPairs = update.message.text

    if abs(float(openPairs)) < 0 or abs(float(openPairs)) > 6:
        textToParse = "ORDER_PAIRS value must be between 1 and 6. Please, try again..."
        update.message.reply_text(textToParse)
        logger.info("Format of OPEN_PAIRS not valid - not equal to 48 chars")
        return OPENKI
    else:
        sett = sett_dict[user_id]
        sett.op = openPairs
        logger.info("Value of current dict: %s and %s", sett.perk, sett.op)

        # Now we need to check if the data provided is legit
        dictChange = {
            'PERK': "["+str(sett.perk)+", "+str(sett.perk2)+"]",
            'ORDER_PAIRS':sett.op
        }

        usersHere = utils.getAllAccounts()
        for uid_ in usersHere:
            uid = uid_[0]
            try:
                utils.updateSettings(uid, dictChange)
                logger.info("ATTENTION: update for user "+str(uid)+", PERK: "+str(sett.perk)+", ORDER_PAIRS: "+str(sett.op))
            except:
                continue
        textToParse = "OPEN_PAIRS and PERK values correctly updated to all users."
        update.message.reply_text(textToParse)
        return ConversationHandler.END


def updateOrderStartStepSize(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    user_name = utils.get_name(update.message.from_user)

    if (mailUsed != "juanba.tomas@gmail.com" and mailUsed != "Julian.b@bitcoin.org.hk"):
        logger.info("User " + user_name + " willing to update settings but profile does not exist: " + str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/update: Profile folder for user " + user_name + " already exists: " + str(user_id))
        textToParse = "(1/3) Please, enter mail of user to update ORDER_START_SIZE and ORDER_STEP_SIZE..."
        update.message.reply_text(textToParse)
        return OSS


def oss_one(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    mail = update.message.text
    receiver_id = utils.checkIfreceiverMailExists(mail)

    if (receiver_id is None):
        textToParse = "Email does not exist. Please, try again..."
        update.message.reply_text(textToParse)
        logger.info("Email %s not valid", update.message.text)
        return OSS
    else:
        textToParse = "(2/3) Good. Now enter ORDER_START_SIZE / ORDER_STEP_SIZE value."
        sett = Sett()
        sett.mail = mail
        sett_dict[user_id] = sett
        update.message.reply_text(textToParse)
        logger.info("Format of PERK valid ( %s ) - Proceeding to ask user for API_SECRET", update.message.text)
        return OSST


def oss_two(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    oss = update.message.text

    if int(oss) < 1:
        textToParse = "ORDER_START_SIZE and ORDER_STEP_SIZE values must be greater than 1. Please, try again."
        update.message.reply_text(textToParse)
        logger.info("Value of ORDER_START_SIZE / ORDER_STEP_SIZE not valid")
        return OSST
    else:
        sett = sett_dict[user_id]
        mail = sett.mail
        receiver_id = utils.getUserID(mail)

        # Now we need to check if the data provided is legit
        dictChange = {
            'ORDER_START_SIZE': str(oss),
            'ORDER_STEP_SIZE': str(oss)
        }

        utils.updateSettings(receiver_id, dictChange)

        textToParse = "ORDER_START_SIZE and ORDER_STEP_SIZE correctly updated for user "+str(mail)
        update.message.reply_text(textToParse)
        return ConversationHandler.END

def updateLeverage(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
        update.message.reply_text(textToParse)
        return ConversationHandler.END
    elif userExists == 1:
        logger.info("User "+user_name+" willing to update Leverage but API keys were not set up before: "+str(user_id))
        textToParse = "Sorry, you can't update your leverage because you did not set up your API keys yet. Please, use /updateKeys to configure the API keys of your bot."
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/update: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = MESSAGES['updateLeverage'] % user_name
        update.message.reply_text(textToParse)
        return LEVERAGE

def updatePerk(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
        update.message.reply_text(textToParse)
        return ConversationHandler.END
    elif userExists == 1:
        logger.info("User "+user_name+" willing to update Leverage but API keys were not set up before: "+str(user_id))
        textToParse = "Sorry, you can't update your perk because you did not set up your API keys yet. Please, use /updateKeys to configure the API keys of your bot."
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        msg = bot.sendMessage(chat_id=chat_id, text=textToParse, parse_mode="HTML", disable_web_page_preview=1, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/update: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "(1/3) Let's update your BUY perk value first. Please, enter your new value next... Type /cancel to return to default state of the bot."
        update.message.reply_text(textToParse)
        return PERK

def updateLoopInterval(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    if not os.path.exists(PATH+'/'+str(user_id)):
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        update.message.reply_text(textToParse)
        return ConversationHandler.END
    else:
        logger.info("/loopInterval: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "Please, introduce the LOOP_INTERVAL value you want to set..."
        update.message.reply_text(textToParse)
        return LOOP_INTERVAL

def updLoopInterval(bot, update):

    try:
        loop_interval = float(update.message.text)
        telegram_id = update.message.from_user.id
        user_id = utils.getMainID(telegram_id)

        if isinstance(loop_interval, float):
            logger.info("Correct LOOP_INTERVAL given")
            textToParse = "LOOP_INTERVAL value correctly updated"

            # Let's generate dictChange
            dictChange = {
                'LOOP_INTERVAL': str(loop_interval),
            }
            
            utils.updateSettings(user_id, dictChange)
            update.message.reply_text(textToParse)
            return ConversationHandler.END
        else:
            logger.info("LOOP_INTERVAL value not correct")
            textToParse = "LOOP_INTERVAL value not correct. Please, try again."
            update.message.reply_text(textToParse)
            return LOOP_INTERVAL
    except:
        logger.info("LOOP_INTERVAL value not correct")
        textToParse = "LOOP_INTERVAL value not correct. Please, try again."
        update.message.reply_text(textToParse)
        return LOOP_INTERVAL

def updateMinSpread(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    if not os.path.exists(PATH+'/'+str(user_id)):
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        update.message.reply_text(textToParse)
        return ConversationHandler.END
    else:
        logger.info("/minSpread: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = "Please, introduce the MIN_SPREAD value you want to set..."
        update.message.reply_text(textToParse)
        return SPREAD_MIN

def updMinSpread(bot, update):

    try:

        min_spread = float(update.message.text)
        telegram_id = update.message.from_user.id
        user_id = utils.getMainID(telegram_id)

        if isinstance(min_spread, float):
            if min_spread >= 0.005 and min_spread <= 0.01:

                logger.info("Correct MIN_SPREAD given")
                textToParse = "MIN_SPREAD value correctly updated"

                # Let's generate dictChange
                dictChange = {
                    'MIN_SPREAD': str(min_spread),
                }
            
                utils.updateSettings(user_id, dictChange)
                update.message.reply_text(textToParse)
                return ConversationHandler.END
            else:
                logger.info("MIN_SPREAD value not correct")
                textToParse = "MIN_SPREAD value must be between 0.005 and 0.01. Please, try again."
                update.message.reply_text(textToParse)
            return SPREAD_MIN
        else:
            logger.info("MIN_SPREAD value not correct")
            textToParse = "MIN_SPREAD value not correct. Please, try again."
            update.message.reply_text(textToParse)
            return SPREAD_MIN
    except:
        logger.info("MIN_SPREAD value not correct")
        textToParse = "MIN_SPREAD value not correct. Please, try again."
        update.message.reply_text(textToParse)
        return SPREAD_MIN


def keyUpd(bot,update):
    
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    API_KEY = update.message.text

    if len(API_KEY) != 24:
        textToParse = MESSAGES['key_format_NOT']
        update.message.reply_text(textToParse)
        logger.info("Format of API_KEY not valid - len ( %s ) not equal to 24 chars", update.message.text)
        return KEY
    else:
        textToParse = MESSAGES['key_format_OK_upd']
        user = User(key=API_KEY)
        user_dict[user_id] = user
        update.message.reply_text(textToParse)
        logger.info("Format of API_KEY valid ( %s ) - Proceeding to ask user for API_SECRET", update.message.text)
        return SECRET

def secretUpd(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    API_SECRET = update.message.text

    if len(API_SECRET) != 48:
        textToParse = MESSAGES['secret_format_NOT'] 
        update.message.reply_text(textToParse)
        logger.info("Format of API_SECRET not valid - not equal to 48 chars")
        return SECRET
    else:
        user = user_dict[user_id]
        user.secret = API_SECRET
        logger.info("Format of API_SECRET valid ( %s ) - Proceeding to check validity of API_KEY and API_SECRET in BitMex API", update.message.text)
        logger.info("Value of current dict: %s and %s", user.key, user.secret)

        # Now we need to check if the data provided is legit
        try:

            bodyBalance = utils.fetch_balance(user_id)
            dictChange = {
                'API_KEY':user.key,
                'API_SECRET':user.secret,
                'ORDER_PAIRS':str(2)
            }

            # Let's check if the user has funds in his/her accountº
            funds = float(bodyBalance/1e8)
            if funds <= 0:
                textToParse = MESSAGES['insufficientFunds']
                update.message.reply_text(textToParse)
            utils.updateSettings(user_id, dictChange)
            user.bitcoin = str(funds)
            textToParse = MESSAGES['keysCorrect_upd']
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)
            if utils.checkIfreceiverIdExists(user_id) == 1:
                utils.setAccountState(user_id, 2)
                utils.refreshCredits(user_id)
                update.message.reply_text('NOTE: you can now run your bot by typing /run and stop it with /stop command likewise')
            
            leverage = utils.getLeverage(user_id)
            if leverage >= 100:
                leverage = 5
                res = utils.updateLeverage(user_id, leverage)
            return ConversationHandler.END

        except:
            logger.info("Keys provided NOT legit")
            textToParse = MESSAGES['keysNotLegit']
            update.message.reply_text(textToParse)
            textToParse = MESSAGES['updateAlone']
            update.message.reply_text(textToParse)
            return KEY


def leverage(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    username = update.effective_user.username
    leverageValue = update.message.text
    priceBitcoin = utils.getBTCUSDprice()
    logger.info("Value of bitcoin: "+str(priceBitcoin))
    user = user_dict[user_id]
    bitcoin = float(user.bitcoin)

    if (int(leverageValue) > 0 and int(leverageValue) < 6):

        logger.info("Leverage value under normal levels - correctly")

        startStepValue = math.floor(float(priceBitcoin)*bitcoin*0.03)        
        logger.info("startStepValue set up to: "+str(startStepValue)+", priceBitcoin: "+str(priceBitcoin)+", number bitcoins: "+str(bitcoin)+", leverage: "+str(leverageValue))
        
        apiKey = user.key
        apiSecret = user.secret

        logger.info('Leverage data for user '+str(user_id)+', with API_KEY:'+apiKey+'and API_SECRET:'+apiSecret)
        logger.info('Entering threading part')

        threads = []
        t = threading.Thread(target=workerFunc, args=(apiKey, apiSecret, user_id, username, user, bot, update))
        threads.append(t)
        t.start()

        logger.info("-----REACHED PART WHERE WORKERS SPLIT--------------")

        return ConversationHandler.END

    else:
        logger.info("Leverage value not set up correctly")
        textToParseNG = MESSAGES['leverageNotGood']
        update.message.reply_text(textToParseNG)
        return LEVERAGE

def workerFunc(apiKey, apiSecret, user_id, username, user, bot, update):
    
    try:
        logger.info("Creating account: " + str(username))

        # We create the new container for the new user
        resultFromCreation = utils.generateContainerForUser(user_id)
        # At account creation, we must decide if we are in testnet or not
        if utils.getTypeOfNet():
            base_url = 'https://testnet.bitmex.com/api/v1/'
        else:
            base_url = 'https://www.bitmex.com/api/v1/'
        
        dictChange = {
            'API_KEY':apiKey,
            'API_SECRET':apiSecret,
            'BASE_URL':base_url
        }

        logger.info("INSTANCE CREATED, NOW UPDATING SETTINGS")

        # Now that the instance and the account have been created, now settings have to be updated too
        utils.updateSettings(user_id, dictChange)
        logger.info("SETTINGS CORRECTLY UPDATED IN NEW INSTANCE")

        textToParse = 'Please, choose below:' #MESSAGES['accountGoodCreation']
        utils.setAccountState(user_id, 1)
        logger.info("Result from user: " + str(username))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return ConversationHandler.END
    except SyntaxError:
        logger.info("Exception when creating account")
        textToParseNG = MESSAGES['leverageNotGood2']
        update.message.reply_text(textToParseNG)
        update.message.reply_text('(2/5) Type your API key again from StaBitLizer...')
        return KEY

def parallelCloseSign(bot, update):
    
    logger.info("CLOSEPOSSIGNCONFIRM")

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    message = str(update.message.text)
    update.message.reply_text('Closing positions. Please, wait.')
    messageP = ''
    ackqNEG = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVE')
    ackqNEU = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEUTRAL')
    ackqNEGMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVEMP')
    ackqPOSMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVEMP')
    ackqPOS = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVE')
    
    if message.lower() == '-':
        logger.info("NEGATIVE")
        messageP = 'NEGATIVE'
        usersHere = utils.getUsersInPosFromLog(-1)
        for uid in usersHere:
            logger.info("Closing NEG for user "+str(uid))
            ackqNEG.put(uid)
    elif message.lower() == '=':
        logger.info("NEUTRAL")
        messageP = 'NEUTRAL'
        usersHere = utils.getAllAccounts()
        logger.info("EEEYY: This is the length of the array: "+str(len(usersHere)))
        us = 0
        for uid_ in usersHere:
            uid = uid_[0]
            ackqNEU.put(uid)
    elif message.lower() == '-mp':
        logger.info("NEGATIVE MP")
        messageP = 'NEGATIVE MP'
        usersHere = utils.getUsersInPosFromLog(-1)
        print("EEEE"+str(usersHere))
        for uid in usersHere:
            ackqNEGMP.put(uid)
            logger.info("Closing NEG MP for user "+str(uid))
    elif message.lower() == '+mp':
        logger.info("POSITIVE MP")
        messageP = 'POSITIVE MP'
        usersHere = utils.getUsersInPosFromLog(+1)
        print(str(usersHere))
        for uid in usersHere:
            ackqPOSMP.put(uid)
            logger.info("Closing POS MP for user "+str(uid))
    elif message.lower() == '+':
        logger.info("POSITIVE")
        messageP = 'POSITIVE'
        usersHere = utils.getUsersInPosFromLog(+1)
        for uid in usersHere:
            logger.info("Closing POS for user "+str(uid))
            ackqPOS.put(uid)

    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text('ATTENTION: users with '+str(messageP)+' positions added to the draining queue. Wait for some seconds until operations are completed.',reply_markup=reply_markup)
    return ConversationHandler.END


def leverageUpd(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    leverageValue = update.message.text
    priceBitcoin = utils.getBTCUSDprice()
    logger.info("Value of bitcoin: "+str(priceBitcoin))

    try:

        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = utils.getSettings(user_id,sett1).replace('"', '').strip()
        apiSecret = utils.getSettings(user_id,sett2).replace('"', '').strip()
        logger.info('Leverage data for user '+str(user_id)+', with API_KEY:'+apiKey+'and API_SECRET:'+apiSecret)

        bitcoin = float(utils.fetch_balance(user_id)/1e8)

        if (int(leverageValue) > 0 and int(leverageValue) < 6):

            startStepValue = math.floor(float(priceBitcoin)*bitcoin*0.03)        
            logger.info("startStepValue set up to: "+str(startStepValue)+", priceBitcoin: "+str(priceBitcoin)+", number bitcoins: "+str(bitcoin)+", leverage: "+str(leverageValue))
 
            dictChange = {
                'ORDER_START_SIZE':str(startStepValue), 
                'ORDER_STEP_SIZE':str(startStepValue)
            }
            utils.updateSettings(user_id, dictChange)
        
            # Choose leverage for a position.
            res = utils.updateLeverage(user_id, leverageValue)
            logger.info("Result from leveraging position: " + str(res))
            textToParse = MESSAGES['leverageGood']
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)
            return ConversationHandler.END
        else:
            logger.info("Leverage value not set up correctly")
            textToParseNG = MESSAGES['leverageNotGood']
            update.message.reply_text(textToParseNG)
            return LEVERAGE
    except:
        logger.info("Exception when calling PositionApi->position_update_leverage")
        textToParseNG = MESSAGES['leverageNotGood2']
        update.message.reply_text(textToParseNG)
        return LEVERAGE


def perkUpd(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    perkValue = update.message.text

    if abs(float(perkValue)) > 3:
        textToParse = "BUY Perk value must be between 0 and 3. Please, try again..."
        update.message.reply_text(textToParse)
        logger.info("Format of PERK BUY not valid - len ( %s ) not equal to 24 chars", update.message.text)
        return PERK
    else:
        textToParse = "(2/3) Good. Now enter SELL PERK value..."
        sett = Sett(perk=perkValue)
        sett_dict[user_id] = sett
        update.message.reply_text(textToParse)
        logger.info("Format of PERK BUY valid ( %s ) - Proceeding to ask user for SELL PERK", update.message.text)
        return PERKU

def perkUpd2(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    perkValue = update.message.text

    try:

        if abs(float(perkValue)) <= 3:

            sett = sett_dict[user_id]
            sett.perk2 = perkValue

            dictChange = {
                'PERK':"["+str(sett.perk)+", "+str(sett.perk2)+"]"
            }
            utils.updateSettings(user_id, dictChange)
        
            # Choose leverage for a position.
            textToParse = 'PERK values updated correctly'
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)
            return ConversationHandler.END
        else:
            textToParse = 'SELL perk value must be between 0 and 3. Please, try again.'
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)
            return PERKU

    except ValueError:
        logger.info("Exception when calling PERK UPDATE")
        textToParseNG = "There was a problem when setting up your perk value"
        update.message.reply_text(textToParseNG)
        return PERKU

def refresh(bot,update):


    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    state = int(utils.getAccountState(user_id))

    if state == 1:
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("WARNING: you can't use this command until you update your API keys with /updateKeys.",reply_markup=reply_markup)
    else:

        try:

            bitcoin = float(utils.fetch_balance(user_id))/1e8

            # Funds in the account of the user
            logger.info("Current funds: "+str(bitcoin))
            creds = math.ceil(bitcoin * 10)
            status, creds, balance, needed = utils.moveCreditsFromOpen(user_id, bitcoin)

            # Leverage value of the user
            leverageValue = utils.getLeverage(user_id)
       
            # Current price of bitcoin 
            priceBitcoin = utils.getBTCUSDprice()

            if (leverageValue > 5 or leverageValue < 1): 
                leverageValue = 5
            #startStepValue = math.floor(float(priceBitcoin)/89.0*(creds / 10)*float(leverageValue))        
            startStepValue = math.floor(float(priceBitcoin)*bitcoin*0.03)        
            logger.info("startStepValue set up to: "+str(startStepValue)+", priceBitcoin: "+str(priceBitcoin)+", number bitcoins: "+str(bitcoin)+", leverage: "+str(leverageValue))
 
            dictChange = {
                'ORDER_START_SIZE':str(startStepValue), 
                'ORDER_STEP_SIZE':str(startStepValue)
            }
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            if status == 1:
                utils.updateSettings(user_id, dictChange)
                update.message.reply_text("Your settings were adapted correctly. You consumed "+str(needed)+" additional credits, according to your current balance: "+str(balance),reply_markup=reply_markup)
            elif status == 2:
                utils.updateSettings(user_id, dictChange)
                update.message.reply_text("Your settings were adapted correctly. You consumed "+str(abs(needed))+" additional credits, according to your current balance: "+str(balance),reply_markup=reply_markup)
            elif status == 3:
                update.message.reply_text("NOTE: you have the exact number of credits corresponding to your current balance. No need to refresh.",reply_markup=reply_markup)
            else:
                update.message.reply_text("WARNING: your settings were not adapted. You don't have enough credits for your current balance.",reply_markup=reply_markup)
        except:
            logger.info("Exception when calling /refresh")
            textToParse = MESSAGES['refreshNotCorrect']
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text(textToParse,reply_markup=reply_markup)

def refreshAdmin(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    try:
        if len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
            receiver_mail = str(args[0])
            receiver_id = utils.checkIfreceiverMailExists(receiver_mail)
            if receiver_id is not None:

                bitcoin = float(utils.fetch_balance(receiver_id))/1e8

                # Funds in the account of the user
                logger.info("Current funds: "+str(bitcoin))
                status, creds, balance, needed = utils.moveCreditsFromOpenAdmin(receiver_id, bitcoin)

                # Current price of bitcoin 
                priceBitcoin = utils.getBTCUSDprice()

                startStepValue = math.floor(float(priceBitcoin)*bitcoin*0.03)        
                logger.info("startStepValue set up to: "+str(startStepValue)+", priceBitcoin: "+str(priceBitcoin)+", number bitcoins: "+str(bitcoin))
 
                dictChange = {
                    'ORDER_START_SIZE':str(startStepValue), 
                    'ORDER_STEP_SIZE':str(startStepValue)
                }
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                if status == 1:
                    utils.updateSettings(receiver_id, dictChange)
                    update.message.reply_text("Your settings were adapted correctly. You consumed "+str(needed)+" additional credits, according to your current balance: "+str(balance),reply_markup=reply_markup)
                elif status == 2:
                    utils.updateSettings(receiver_id, dictChange)
                    update.message.reply_text("Your settings were adapted correctly. You consumed "+str(abs(needed))+" additional credits, according to your current balance: "+str(balance),reply_markup=reply_markup)
                elif status == 3:
                    print("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE: "+str(user_id)+str(dictChange))
                    utils.updateSettings(receiver_id, dictChange)
                    update.message.reply_text("NOTE: you have the exact number of credits corresponding to your current balance. No need to refresh.",reply_markup=reply_markup)
            else:
                custom_keyboard = [['/bot', '/setup', '/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
                update.message.reply_text("WARNING: given user does not exist.",reply_markup=reply_markup)
        else:
            custom_keyboard = [['/bot', '/setup', '/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
            update.message.reply_text("WARNING: you need to be admin to proceed.",reply_markup=reply_markup)
    except:
        logger.info("Exception when calling /refreshAdmin")
        textToParse = MESSAGES['refreshNotCorrect']
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)

def refreshPerk(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    if len(args) == 2 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        try:
            perk1 = float(args[0])
            perk2 = float(args[1])
        except:
            custom_keyboard = [['/bot', '/setup', '/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
            update.message.reply_text("You must type the command followed by the two PERK values (BUY and SELL), separated by a blank space (eg: /refresPerk 1.1 2). Please, try again.", reply_markup=reply_markup)
        setOfUsers = utils.getAllAccounts(2)
        for row in setOfUsers:
            user_id = row[0]
            dictChange = {
                'PERK':"["+str(perk1)+", "+str(perk2)+"]"
            }
            try:
                utils.updateSettings(user_id, dictChange)
            except:
                continue
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("Perk updated for all users", reply_markup=reply_markup)
    elif mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk":
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("Please, type the command followed by the perk value", reply_markup=reply_markup)

def refreshOP(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    if len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        perk = int(args[0])
        setOfUsers = utils.getAllAccounts()
        for row in setOfUsers:
            user_id = row[0]
            dictChange = {
                'ORDER_PAIRS':str(perk)
            }
            try:
                utils.updateSettings(user_id, dictChange)
            except:
                continue
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("ORDER_PAIRS updated for all users", reply_markup=reply_markup)
    elif (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("Please, type the command followed by the ORDER_PAIRS value", reply_markup=reply_markup)
    else:
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("WARNING: you are not allowed to use this command", reply_markup=reply_markup)

def updateOP(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    try:
        if len(args) == 1:
            perk = int(args[0])
            if perk > 0 and perk < 7:
                dictChange = {
                    'ORDER_PAIRS':str(perk)
                }
                utils.updateSettings(user_id, dictChange)
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                update.message.reply_text("ORDER_PAIRS correctly updated", reply_markup=reply_markup)
            else:
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                update.message.reply_text("ERROR: ORDER_PAIRS must be between 1 and 6. Try again, please.", reply_markup=reply_markup)
        else:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("ERROR: please, type the command followed by ORDER_PAIRS value", reply_markup=reply_markup)
    except:
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("ERROR: there was a problem when running the command. Please, try again.", reply_markup=reply_markup)


def cancel(bot,update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    textToParse = MESSAGES['canceled']
    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text(textToParse, reply_markup=reply_markup)

    return ConversationHandler.END

def cancelCreate(bot,update):
    try:
        user = update.message.from_user
        user_id = user.id
        logger.info("Account for user %s was not correctly created, removing.", user.first_name)
        shutil.rmtree(PATH+'/'+str(user_id))
        textToParse = MESSAGES['canceledCreate']
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
    except:
        logger.info("Canceling account creation process failed")

    return ConversationHandler.END


def USDBTCget(bot,update):

    priceBitcoin, timeStamp = utils.getBTCUSDpriceAll()
    logger.info("Getting USD-BTC price")
    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text("Current price 1 BTC = " + priceBitcoin + ' USD, last update: ' + timeStamp, reply_markup=reply_markup)

def getLeverage(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    api_method = 'GET'
    get_order_url = '/api/v1/position'

    if len(args) == 0:

        try:
            lever = utils.getLeverage(user_id)
            logger.info("LEVERAGE VALUE: "+str(lever))

            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Current leverage: "+str(lever),reply_markup=reply_markup)
        
        except:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Leverage could not be retrieved correctly. Please, try again in a few seconds. Thank you.",reply_markup=reply_markup)
    elif len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        try:
            receiver_id = utils.checkIfreceiverMailExists(args[0])
            lever = utils.getLeverage(receiver_id)
            logger.info("LEVERAGE VALUE: "+str(lever))

            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Current leverage: "+str(lever),reply_markup=reply_markup)
        
        except ValueError:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Leverage could not be retrieved correctly. Please, try again in a few seconds. Thank you.",reply_markup=reply_markup)
 
def getPerk(bot,update,args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    if len(args) == 0:
        try:
            sett1 = 'PERK'
            perk = utils.getSettings(user_id,sett1).replace('"', '').strip()
            logger.info("Result from getting perk: " + str(perk))
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Current perk: "+str(perk),reply_markup=reply_markup)
        except:
            logger.info("USER: Perk value could not be retrieved correctly for user "+str(user_id))
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Perk value could not be retrieved correctly",reply_markup=reply_markup)
    elif len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        receiver_id = args[0]
        try:
            sett1 = 'PERK'
            perk = utils.getSettings(receiver_id,sett1).replace('"', '').strip()
            logger.info("Result from getting perk for "+str(receiver_id)+": " + str(perk))
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Current perk for user "+str(receiver_id)+": "+str(perk),reply_markup=reply_markup)
        except:
            logger.info("ADMIN: Perk value could not be retrieved correctly for user "+str(receiver_id))
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("Perk value could not be retrieved correctly",reply_markup=reply_markup)

def statusFunc(update, user_id):
   
    try: 
        lever, position, balance = utils.getOperationsData(user_id)

        output = 'Current Balance: '+str(balance / 1e8)
        output3 = 'Current Contract Position: '+str(position)

        bashCommand2 = './status '+str(user_id)
        process2 = subprocess.Popen(bashCommand2.split(), stdout=subprocess.PIPE)
        output2, error = process2.communicate()

        outputSecond = str(output2.decode('utf-8'))
        creditsInAccount = utils.getCreditsActive(user_id)
        dateNow = str(datetime.datetime.now()) 
        logger.info("Result from getting leverage: " + str(lever))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount), reply_markup=reply_markup)

    except:
        logger.info("Problem when getting status: an ERROR occurred")
        logger.info("Result from getting leverage: " + str(lever))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("ERROR: your status value WASN'T retrieved correctly. Please, try again.",reply_markup=reply_markup)

def status(bot, update, args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    try: 
        if len(args) == 0 or (mailUsed != "juanba.tomas@gmail.com" and mailUsed != "Julian.b@bitcoin.org.hk"):
            if (utils.checkAccountStateID(user_id) > 1):
                lever, position, balance = utils.getOperationsData(user_id)
                account_state = utils.getAccountState(user_id)
                stop_state = utils.getStopState(user_id)

                output = 'Current Balance: '+str(balance / 1e8)
                output3 = 'Current Contract Position: '+str(position)
                
                bashCommand2 = './statusPython '+str(user_id)+' '+str(account_state)+' '+str(stop_state)
                process2 = subprocess.Popen(bashCommand2.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
                output2, error = process2.communicate()

                outputSecond = str(output2.decode('utf-8'))
                creditsInAccount = utils.getCreditsActive(user_id)
                dateNow = str(datetime.datetime.now()) 
                logger.info("Result from getting leverage: " + str(lever))
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                if 'not found' in outputSecond:
                    update.message.reply_text("WARNING: Make sure your API keys are setup correctly before continuing", reply_markup=reply_markup)
                #elif 'not currently running' not in outputSecond and 'overloaded' not in outputSecond:
                elif lever > 0:
                    update.message.reply_text(outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount), reply_markup=reply_markup)
                else:
                    update.message.reply_text(outputSecond, reply_markup=reply_markup)
            else:
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                update.message.reply_text("WARNING: you need to set API keys first to get your status", reply_markup=reply_markup)
        elif len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
            receiver_mail = str(args[0])
            receiver_id = utils.checkIfreceiverMailExists(receiver_mail)
            if receiver_id is not None:
                if (utils.checkAccountState(receiver_mail) > 1):
                    lever, position, balance = utils.getOperationsData(receiver_id)
                    account_state = utils.getAccountState(receiver_id)
                    stop_state = utils.getStopState(receiver_id)

                    output = 'Current Balance: '+str(balance / 1e8)
                    output3 = 'Current Contract Position: '+str(position)

                    bashCommand2 = './statusPython '+str(receiver_id)+' '+str(account_state)+' '+str(stop_state)
                    logger.info(bashCommand2)
                    process2 = subprocess.Popen(bashCommand2.split(), stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                    output2, error = process2.communicate()

                    outputSecond = str(output2.decode('utf-8'))
                    creditsInAccount = utils.getCreditsActive(receiver_id)
                    dateNow = str(datetime.datetime.now()) 
                    logger.info("Result from getting leverage: " + str(lever))
                    custom_keyboard = [['/bot','/setup','/statistics']]
                    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                    if 'not found' in outputSecond:
                        update.message.reply_text("WARNING: Make sure your API keys are setup correctly before continuing", reply_markup=reply_markup)
                    #elif ('not currently running' not in outputSecond and 'overloaded' not in outputSecond):
                    elif lever > 0:
                        update.message.reply_text(outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount), reply_markup=reply_markup)
                    else:
                        update.message.reply_text(outputSecond, reply_markup=reply_markup)
                else:
                    custom_keyboard = [['/bot','/setup','/statistics']]
                    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                    update.message.reply_text("WARNING: you need to set API keys first to get your status", reply_markup=reply_markup)
            else: 
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                update.message.reply_text("User entered does not exist", reply_markup=reply_markup)
        else:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("ERROR: please, use correct format", reply_markup=reply_markup)

    except ValueError:
        logger.info("Problem when getting status: an ERROR occurred")
        logger.info("Result from getting leverage: " + str(lever))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("ERROR: your status value WASN'T retrieved correctly. Please, try again.", reply_markup=reply_markup)


def statusTrue(bot, update, args):
   
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    try:
        
        if len(args) == 0 or (mailUsed != "juanba.tomas@gmail.com" and mailUsed != "Julian.b@bitcoin.org.hk"):
            lever, position, balance = utils.getOperationsData(user_id)

            output = 'Current Balance: '+str(balance / 1e8)
            output3 = 'Current Contract Position: '+str(position)
            process2 = subprocess.Popen(['./status', str(user_id)], stdout=subprocess.PIPE)
            output2, error = process2.communicate()
            logger.info("Output2: " + str(output2))

            outputSecond = str(output2.decode('utf-8'))
            creditsInAccount = utils.getCreditsActive(user_id)
            dateNow = str(datetime.datetime.now()) 
            logger.info("Result from getting leverage: " + str(lever))
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            if 'not currently running' not in outputSecond:
                update.message.reply_text(outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount), reply_markup=reply_markup)
            else:
                update.message.reply_text(outputSecond, reply_markup=reply_markup)
        elif len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
            receiver_mail = str(args[0])
            receiver_id = utils.checkIfreceiverMailExists(receiver_mail)
            if receiver_id is not None:
                lever, position, balance = utils.getOperationsData(receiver_id)

                output = 'Current Balance: '+str(balance / 1e8)
                output3 = 'Current Contract Position: '+str(position)
                process2 = subprocess.Popen(['./status', str(receiver_id)], stdout=subprocess.PIPE)
                output2, error = process2.communicate()
                logger.info("Output2: " + str(output2))

                outputSecond = str(output2.decode('utf-8'))
                creditsInAccount = utils.getCreditsActive(receiver_id)
                dateNow = str(datetime.datetime.now()) 
                logger.info("Result from getting leverage: " + str(lever))
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                if 'not currently running' not in outputSecond:
                    update.message.reply_text(outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount), reply_markup=reply_markup)
                else:
                    update.message.reply_text(outputSecond, reply_markup=reply_markup)
            else: 
                custom_keyboard = [['/bot','/setup','/statistics']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
                update.message.reply_text("User entered does not exist", reply_markup=reply_markup)
        else:
            custom_keyboard = [['/bot','/setup','/statistics']]
            reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
            update.message.reply_text("ERROR: please, use correct format", reply_markup=reply_markup)
    except:
        logger.info("Problem when getting status: an ERROR occurred")
        logger.info("Result from getting leverage: " + str(lever))
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text("ERROR: your status value WASN'T retrieved correctly. Please, try again.", reply_markup=reply_markup)

def notifications(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("Profile folder didn't exist before, cannot continue: "+str(user_id))
        textToParse = MESSAGES['notifyNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Profile folder exists, let's ask user confirmation: "+str(user_id))
        textToParse = MESSAGES['notifyConfirmation']
        custom_keyboard = [['Yes', 'No']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return NOTIFY

def notifyConf(bot,update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    message = str(update.message.text)

    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    
    if (message.lower() == 'yes' or message.lower() == 'y'):
        logger.info("Notifications enabled, as decided by user "+str(user_id))
        textToParse = MESSAGES['notifyYes']
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        utils.setNotifications(user_id, 1)
        return ConversationHandler.END
    elif (message.lower() == 'no' or message.lower() =='n'):
        logger.info("Notifications disabled, as decided by user "+str(user_id))
        textToParse = MESSAGES['notifyNotNot']
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        utils.setNotifications(user_id, 0)
        return ConversationHandler.END
    else:
        logger.info("Notifications disabled, as decided by user "+str(user_id))
        textToParse = MESSAGES['notifyNotValidCommand']
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        utils.setNotifications(user_id, 0)
        return ConversationHandler.END

def botGroup(bot, update):
    user_id = update.message.from_user.id
    user_id = utils.getMainID(user_id)
    if utils.checkIfreceiverIdExists(user_id) == 1:
        custom_keyboard = [['/refresh','/sendCredits','/delete']]
    else:
        custom_keyboard = [['/run', '/stop', '/status'], ['/refresh','/sendCredits','/delete']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text('Please, select your option below:',reply_markup=reply_markup)

def setupGroup(bot, update):
    user_id = update.message.from_user.id
    user_id = utils.getMainID(user_id)
    if utils.checkIfreceiverIdExists(user_id) == 1:
        custom_keyboard = [['/updateKeys', '/notifications']]
    else:
        custom_keyboard = [['/updateKeys', '/updateLever'],['/notifications','/updVolatility']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text('Please, select your option below:',reply_markup=reply_markup)

def statsGroup(bot, update):
    user_id = update.message.from_user.id
    user_id = utils.getMainID(user_id)
    if utils.checkIfreceiverIdExists(user_id) == 1:
        custom_keyboard = [['/BTC', '/mail', '/credits']]
    else:
        custom_keyboard = [['/BTC','/stats','/leverage'],['/mail','/credits']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text('Please, select your option below:',reply_markup=reply_markup)

def password(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    user_name = utils.get_name(update.message.from_user)

    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User " + user_name + " willing to update settings but profile does not exist: " + str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        #raw_password = utils.generate_pass()
        #stored_password = utils.hash_password(raw_password)
        #utils.insert_hash_in_ddbb(user_id, stored_password)
        textToParse = 'You are going to modify your password next. It has to be at least 8 characters long, and include one upper and lowercase, one number, and one special symbol at least. Please, type your new password next...'
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return PASSONE

def typePass(bot, update):
    telegram_id = update.message.from_user.id

    passw = update.message.text
    res = utils.checkPass(passw)

    if res == False:
        textToParse = "Password format is incorrect. Please, try again. NOTE: it has to be at least 8 characters long, and include one upper and lowercase, one number, and one special symbol at least."
        update.message.reply_text(textToParse)
        return PASSONE

    user = User(passw=str(passw))
    user_dict[telegram_id] = user

    update.message.reply_text("GOOD. Now type the password again...")
    return PASSCONF

def confirmPass(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    passw2 = update.message.text
    user = user_dict[telegram_id]
    passw = user.passw

    print("Password 1: "+str(passw))
    print("Password 2: " + str(passw2))

    if passw != passw2:
        textToParse = "Passwords mismatch. Please, begin again by typing the new password..."
        update.message.reply_text(textToParse)
        return PASSONE
    else:
        stored_password = utils.hash_password(passw)
        utils.insert_hash_in_ddbb(user_id, stored_password)
        textToParse = 'Password reset successfully completed. Thank you.'
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return ConversationHandler.END

def getBalances(bot, update):
    
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    MAX_MESSAGE_LENGTH = 4096
    if (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):

        update.message.reply_text("Generating the list. One moment, please...")
        bashCommand = './balances.py'
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        outputFirst = str(output.decode('utf-8'))
        if len(outputFirst) <= MAX_MESSAGE_LENGTH:
            update.message.reply_text(outputFirst)
        else:
            parts = []
            while len(outputFirst) > 0:
                if len(outputFirst) > MAX_MESSAGE_LENGTH:
                    part = outputFirst[:MAX_MESSAGE_LENGTH]
                    first_lnbr = part.rfind('\n')
                    if first_lnbr != -1:
                        parts.append(part[:first_lnbr])
                        outputFirst = outputFirst[first_lnbr:]
                    else:
                        parts.append(part)
                        outputFirst = outputFirst[MAX_MESSAGE_LENGTH:]
                else:
                    parts.append(outputFirst)
                    break
            msg = None
            for part in parts:
                logger.info('Printing inside here')
                update.message.reply_text(part)
                sleep(0.5)

    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    update.message.reply_text('Select option below: ',reply_markup=reply_markup)


def volatility(bot, update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    
    user_name = utils.get_name(update.message.from_user)
    chat_id = update.message.chat.id
    
    userExists = utils.checkIfreceiverIdExists(user_id)

    if userExists == 0:
        logger.info("User "+user_name+" willing to update settings but profile does not exist: "+str(user_id))
        textToParse = MESSAGES['updateAccountNot']
        custom_keyboard = [['/create', '/welcome']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("/updVolatility: Profile folder for user "+user_name+" already exists: "+str(user_id))
        textToParse = 'Select what type of volatility configuration you want for your bot. Please, enter your new value next... Type /cancel to return to default state of the bot.'
        custom_keyboard = [['High', 'Default','Low']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse, reply_markup=reply_markup)
        return VOLATILITY


def volConf(bot, update):

    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)

    username = update.effective_user.username
    volatility = update.message.text
   
    if volatility == 'High':
        dictChange = {
            'INTERVAL': str(0.01),
        }

        utils.updateSettings(user_id, dictChange)
        textToParse = 'Volatility updated to "High"'
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    elif volatility == 'Default':
        dictChange = {
            'INTERVAL': str(0.0049),
        }

        utils.updateSettings(user_id, dictChange)
        textToParse = 'Volatility updated to "Default"'
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    elif volatility == 'Low':
        dictChange = {
            'INTERVAL': str(0.0025),
        }

        utils.updateSettings(user_id, dictChange)
        textToParse = 'Volatility updated to "Low"'
        custom_keyboard = [['/bot','/setup','/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
        update.message.reply_text(textToParse,reply_markup=reply_markup)
        return ConversationHandler.END
    else:
        logger.info("Volatility value not set up correctly")
        textToParseNG = 'Volatility value not set up correctly. Please, try again.'
        custom_keyboard = [['High', 'Default','Low']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
        update.message.reply_text(textToParseNG, reply_markup=reply_markup)
        return VOLATILITY


def notify(bot, update):
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)

    if (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        update.message.reply_text("Please, type next the message you want to send to all users and hit enter...")
        return NOTIFYALL
    else:
        update.message.reply_text("You need to be admin to send notifications to users.")
        custom_keyboard = [['/bot', '/setup', '/statistics']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
        update.message.reply_text('Select option below: ', reply_markup=reply_markup)
        return ConversationHandler.END

def notifySend(bot, update):
    telegram_id = update.message.from_user.id
    message = str(update.message.text)

    setOfUsers = utils.getAllAccounts(2)
    logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + ": sending notification to users")
    score = 0
    for row in setOfUsers:
        try:
            user_id = row[0]
            telegram_id = utils.getTelegramID(user_id)
            logger.info("=======================================")
            logger.info("Sending notification to user: " + str(telegram_id))
            bot.send_message(chat_id=telegram_id, text=message)
            logger.info("=======================================")
            sys.stdout.flush()
        except:

            continue

    custom_keyboard = [['/bot', '/setup', '/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    return ConversationHandler.END

def statsUser(bot, update, args):
    
    telegram_id = update.message.from_user.id
    user_id = utils.getMainID(telegram_id)
    mailUsed = utils.getUserMail(user_id)
    
    custom_keyboard = [['/bot','/setup','/statistics']]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard,resize_keyboard=True)
    if len(args) == 0:
        mail = utils.getUserMail(user_id)
        if mail is not None:
            buffer = utils.generateGraphCreditsVsBalance(user_id)
            if buffer != -1:
                update.message.reply_text('Your stats ('+str(utils.getUserMail(user_id))+'):')
                buffer.seek(0)
                bot.send_photo(chat_id=telegram_id, photo=buffer)

                # Credits and balance
                buffer2 = utils.generateGraphPositionVsPerformance(user_id)
                buffer2.seek(0)
                bot.send_photo(chat_id=telegram_id, photo=buffer2)
            
                # Show numerical data
                statusFunc(update, user_id)
            else:
                update.message.reply_text('No data to represent. Please, wait until more stats are gathered.', reply_markup=reply_markup)

        else:  
            update.message.reply_text('Your account does not exist yet. Type /create and follow the steps.', reply_markup=reply_markup)

    elif len(args) == 1 and (mailUsed == "juanba.tomas@gmail.com" or mailUsed == "Julian.b@bitcoin.org.hk"):
        receiver_mail = str(args[0])
        receiver_id = utils.checkIfreceiverMailExists(receiver_mail)
        if receiver_id is not None:
            buffer = utils.generateGraphCreditsVsBalance(receiver_id)
            if buffer != -1:
                buffer.seek(0)
                update.message.reply_text('Sztats for user '+str(receiver_mail)+' :')
                bot.sendPhoto(chat_id=telegram_id, photo=buffer)

                # Credits and balance
                buffer2 = utils.generateGraphPositionVsPerformance(receiver_id)
                buffer2.seek(0)
                bot.send_photo(chat_id=telegram_id, photo=buffer2)
            
                # Show numerical data
                statusFunc(update, receiver_id)
            else:
                update.message.reply_text('No data to represent. Please, wait until more stats are gathered.', reply_markup=reply_markup)

        elif os.path.exists(PATH+'/'+str(args[0])):
            # Credits and balance
            receiver_id = int(args[0])
            buffer = utils.generateGraphCreditsVsBalance(receiver_id)
            if buffer != -1:
                buffer.seek(0)
                update.message.reply_text('STATS FOR USER '+str(utils.getUserMail(receiver_id))+' :')
                bot.send_photo(chat_id=telegram_id, photo=buffer)

                # Credits and balance
                buffer2 = utils.generateGraphPositionVsPerformance(receiver_id)
                buffer2.seek(0)
                bot.send_photo(chat_id=telegram_id, photo=buffer2)

                # Show numerical data
                statusFunc(update, receiver_id)
            else:
                update.message.reply_text('No data to represent. Please, wait until more stats are gathered.', reply_markup=reply_markup)
        else:
            update.message.reply_text('User '+str(receiver_mail)+' does not exist',reply_markup=reply_markup)
    else:
        update.message.reply_text('Just type /stats without arguments for proper visualization', reply_markup=reply_markup)

def main():
    """Start the bot."""
    logger.info("Setting command handlers")
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot=bot)#, request_kwargs={'proxy_url': 'http://lum-customer-stabilizer-zone-zoneee:s4jj9mb72odh@zproxy.lum-superproxy.io:22225'})

    # Get the dispatcher to register handlers:
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("welcome", welcome))
    dp.add_handler(CommandHandler("run", run))
    dp.add_handler(CommandHandler("BTC",USDBTCget))
    dp.add_handler(CommandHandler("status",status,pass_args=True))
    dp.add_handler(CommandHandler("leverage",getLeverage,pass_args=True))
    dp.add_handler(CommandHandler("perk",getPerk,pass_args=True))
    dp.add_handler(CommandHandler("refresh",refresh))
    dp.add_handler(CommandHandler("refreshAdmin",refreshAdmin,pass_args=True))
    dp.add_handler(CommandHandler("refreshPerk",refreshPerk,pass_args=True))
    dp.add_handler(CommandHandler("refreshOP",refreshOP,pass_args=True))
    dp.add_handler(CommandHandler("updateOP",updateOP,pass_args=True))
    dp.add_handler(CommandHandler("mail",getId))
    dp.add_handler(CommandHandler("credits",getCredits))
    dp.add_handler(CommandHandler("balances",getBalances))
    dp.add_handler(CommandHandler("bot",botGroup))
    dp.add_handler(CommandHandler("setup",setupGroup))
    dp.add_handler(CommandHandler("statistics",statsGroup))
    dp.add_handler(CommandHandler("stats",statsUser, pass_args=True))

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('create', create)],

        states={
            MAIL: [MessageHandler(Filters.text, mail)],
            KEY: [MessageHandler(Filters.text, key)],
            SECRET: [MessageHandler(Filters.text, secret)],
            LEVERAGE: [MessageHandler(Filters.text, leverage)],
            MIGRATE: [MessageHandler(Filters.text, migrate)]
        },

        conversation_timeout=180.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancelCreate)]
    )

    conv_handler_two = ConversationHandler(
        entry_points=[CommandHandler('updateKeys', updateKeys)],

        states={
            KEY: [MessageHandler(Filters.text, keyUpd)],
            SECRET: [MessageHandler(Filters.text, secretUpd)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_three = ConversationHandler(
        entry_points=[CommandHandler('updateLever', updateLeverage)],

        states={
            LEVERAGE: [MessageHandler(Filters.text, leverageUpd)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_four = ConversationHandler(
        entry_points=[CommandHandler('delete', deleteProf)],

        states={
            DELETE: [MessageHandler(Filters.text, deleteConf)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_six = ConversationHandler(
        entry_points=[CommandHandler('sendCredits', sendCredits)],

        states={
            RECEIVER: [MessageHandler(Filters.text, receiver)],
            CREDITS: [MessageHandler(Filters.text, credits)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_seven = ConversationHandler(
        entry_points=[CommandHandler('notifications', notifications)],

        states={
            NOTIFY: [MessageHandler(Filters.text, notifyConf)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_eight = ConversationHandler(
        entry_points=[CommandHandler('stop', stop)],

        states={
            POSCLOSES: [MessageHandler(Filters.text, stopConfirm)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_nine = ConversationHandler(
        entry_points=[CommandHandler('updVolatility', volatility)],

        states={
            VOLATILITY: [MessageHandler(Filters.text, volConf)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_ten = ConversationHandler(
        entry_points=[CommandHandler('loopInterval', updateLoopInterval)],

        states={
            LOOP_INTERVAL: [MessageHandler(Filters.text, updLoopInterval)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_eleven = ConversationHandler(
        entry_points=[CommandHandler('minSpread', updateMinSpread)],

        states={
            SPREAD_MIN: [MessageHandler(Filters.text, updMinSpread)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_twelve = ConversationHandler(
        entry_points=[CommandHandler('closeSign', closePositionsAccordingToSign)],

        states={
            POSSIGN: [MessageHandler(Filters.text, closePosSignConfirm)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )
    
    conv_handler_thirteen = ConversationHandler(
        entry_points=[CommandHandler('updatePerk', updatePerk)],

        states={
            PERK: [MessageHandler(Filters.text, perkUpd)],
            PERKU: [MessageHandler(Filters.text, perkUpd2)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_fourteen = ConversationHandler(
        entry_points=[CommandHandler('updateRisk', updatePerkAndOP)],

        states={
            PERKI: [MessageHandler(Filters.text, perkUpdRow)],
            PERKII: [MessageHandler(Filters.text, perkUpdRow2)],
            OPENKI: [MessageHandler(Filters.text, opUpdRow)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_fifteen = ConversationHandler(
        entry_points=[CommandHandler('pass', password)],

        states={
            PASSONE: [MessageHandler(Filters.text, typePass)],
            PASSCONF: [MessageHandler(Filters.text, confirmPass)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_sixteen = ConversationHandler(
        entry_points=[CommandHandler('notify', notify)],

        states={
            NOTIFYALL: [MessageHandler(Filters.text, notifySend)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler_seventeen= ConversationHandler(
        entry_points=[CommandHandler('updatePositionWeight', updateOrderStartStepSize)],

        states={
            OSS: [MessageHandler(Filters.text, oss_one)],
            OSST: [MessageHandler(Filters.text, oss_two)]
        },

        conversation_timeout=60.0,
        allow_reentry=True,
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)
    dp.add_handler(conv_handler_two)
    dp.add_handler(conv_handler_three)
    dp.add_handler(conv_handler_four)
    dp.add_handler(conv_handler_six)
    dp.add_handler(conv_handler_seven)
    dp.add_handler(conv_handler_eight)
    dp.add_handler(conv_handler_nine)
    dp.add_handler(conv_handler_ten)
    dp.add_handler(conv_handler_eleven)
    dp.add_handler(conv_handler_twelve)
    dp.add_handler(conv_handler_thirteen)
    dp.add_handler(conv_handler_fourteen)
    dp.add_handler(conv_handler_fifteen)
    dp.add_handler(conv_handler_sixteen)
    dp.add_handler(conv_handler_seventeen)

    dp.add_handler(MessageHandler(Filters.command, default))
    
    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
