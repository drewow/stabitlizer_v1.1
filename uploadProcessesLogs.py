import dropbox
import os
import sys
import shutil

def main():

    fileName = sys.argv[1]

    dbx = dropbox.Dropbox("Fh8xUU6Sm6sAAAAAAABYXrDXcGXgqJ88KTvxsZ08Q6eBq6qomOr7Nbbf33Pv3I9C")
    dbx.users_get_current_account()

    file_path = '/home/ubuntu/StaBitLizer_v1.1/logs/'+fileName
    dest_path = '/StaBitLizer Processes/'+fileName

    print(file_path)
    f = open(file_path)
    file_size = os.path.getsize(file_path)

    CHUNK_SIZE = 4 * 1024 * 1024

    if file_size <= CHUNK_SIZE:

        print(dbx.files_upload(f.read(), dest_path, mode=dropbox.files.WriteMode.overwrite))

    else:

        upload_session_start_result = dbx.files_upload_session_start(f.read(CHUNK_SIZE))
        cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,
                                               offset=f.tell())
        commit = dropbox.files.CommitInfo(path=dest_path)

        while f.tell() < file_size:
            if ((file_size - f.tell()) <= CHUNK_SIZE):
                print(dbx.files_upload_session_finish(f.read(CHUNK_SIZE),
                                                cursor,
                                                commit))
            else:
                dbx.files_upload_session_append(f.read(CHUNK_SIZE),
                                                cursor.session_id,
                                                cursor.offset)
                cursor.offset = f.tell()

if __name__ == '__main__':
    main()
