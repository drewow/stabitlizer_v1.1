#!/usr/bin/env python

# WS server example

import asyncio
import websockets

import utils
import persistqueue
import logging
import sys

"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
logger.info("Running "+sys.argv[0])


async def hello(websocket, path):
    print("Conectado desde: "+str(websocket.remote_address[0]))
    name = await websocket.recv()

    name2 = name.split(",")
    stringArray = name2[1].split(';')

    if len(stringArray) == 3:
        # First close sign
        if 'cn' in stringArray[0]:
            closeSign('-')
        elif 'cp' in stringArray[0]:
            closeSign('+')
        elif 'ca' in stringArray[0]:
            closeSign('=')

        # Perk positive
        pp = float(stringArray[1])
        pn = float(stringArray[2])
        changePerkForAll([pp,pn])

    greeting = f"{name}!"
    logger.info(greeting+" - IP: "+str(websocket.remote_address[0]))

    await websocket.send(greeting)
    #print(f"> {greeting}")

def closeSign(message):

    messageP = ''
    ackqNEG = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVE')
    ackqNEU = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEUTRAL')
    ackqNEGMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVEMP')
    ackqPOSMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVEMP')
    ackqPOS = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVE')
    
    if message.lower() == '-':
        messageP = 'NEGATIVE'
        usersHere = utils.getUsersInPos(-1)
        for uid in usersHere:
            print("Closing NEG for user "+str(uid))
            ackqNEG.put(uid)
    elif message.lower() == '=':
        messageP = 'NEUTRAL'
        usersHere = utils.getAllAccounts()
        us = 0
        for uid_ in usersHere:
            uid = uid_[0]
            ackqNEU.put(uid)
    elif message.lower() == '-mp':
        messageP = 'NEGATIVE MP'
        usersHere = utils.getUsersInPos(-1)
        print("EEEE"+str(usersHere))
        for uid in usersHere:
            ackqNEGMP.put(uid)
            print("Closing NEG MP for user "+str(uid))
    elif message.lower() == '+mp':
        messageP = 'POSITIVE MP'
        usersHere = utils.getUsersInPos(+1)
        print(str(usersHere))
        for uid in usersHere:
            ackqPOSMP.put(uid)
            print("Closing POS MP for user "+str(uid))
    elif message.lower() == '+':
        messageP = 'POSITIVE'
        usersHere = utils.getUsersInPos(+1)
        for uid in usersHere:
            print("Closing POS for user "+str(uid))
            ackqPOS.put(uid)


def perkUpd(user_id, value):

    try:
        print("Perk value"+str(value))
        dictChange = {
            'PERK':str(value)
        }
        utils.updateSettings(user_id, dictChange)

    except ValueError:
        print("Exception when calling PositionApi->position_update_leverage")

def changePerkForAll(value):
    setOfUsers = utils.getAllAccounts(2)
    userNum = 0
    for row in setOfUsers:
        try:
            user_id = row[0]
            sett1 = 'PERK'
            userNum += 1
            print("Updating for user: "+str(user_id))
            perkUpd(user_id, value)
            print("Now with user: "+str(userNum))
            print("--------------------------------------")

        except SyntaxError:
            print("Continuing for user: "+str(user_id))
            print("Now with user: "+str(userNum))
            print("--------------------------------------")
            continue


start_server = websockets.serve(hello, "0.0.0.0", 12431)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
