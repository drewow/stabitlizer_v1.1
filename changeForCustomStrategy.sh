#!/bin/bash

# This script was made to insert necessary changes (PERK) in market_maker

users="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT user_id from user_data;')"
num=0
for D in `echo ${users}`
do
    docker_instance="$(docker ps -a | grep $D | awk '{print $1}')"
    docker cp /home/ubuntu/StaBitLizer_v1.1/DockerExp/StaBitLizer/market_maker/custom_strategy.py $D:/StaBitLizer/StaBitLizer/market_maker/custom_strategy.py
    printf "$docker_instance\n"
done
