#!/usr/bin/env python3.6

import utils


def main():

    users = utils.getAllAccounts()
   
    dictChange = {
        'BASE_URL': 'https://www.bitmex.com/api/v1/',
    }
    for user in users:
        user_id = user[0]
        try:
            base_url = utils.getSettings(user_id, 'BASE_URL')
            if 'testnet' in base_url:
                print('================User '+str(user_id)+' has testnet setup')
                utils.updateSettings(user_id, dictChange)
        except:
            continue

if __name__ == '__main__':
    main()
