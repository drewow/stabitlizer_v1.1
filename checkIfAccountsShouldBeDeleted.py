#!/usr/bin/env python3.6

import os
import sys
import yaml
import ccxt
import logging
import bitmex
import docker
import subprocess
import json
from datetime import datetime
from datetime import timedelta
import requests
import math
from pathlib import Path
import datetime
import MySQLdb
import telegram
import random
import boto3
import time
import string
import utils
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from telegram import MessageEntity, TelegramObject, ChatAction

PATH = os.path.dirname(os.path.abspath(__file__))

def getBotID():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 5;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


def getBotMexDir():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 4;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(utils.getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=getBotID())

def getActiveInstanceID(user_id):

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT instance_id FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()

def getTempSettingsFile(size=6, chars=string.ascii_uppercase + string.digits):

    return ''.join(random.choice(chars) for _ in range(size))

def updateSettings(user_id,dict):

    #TODO: change path by that extracted from DDBB
    tempSettingsFile = '/home/ubuntu/StaBitLizer_v1.1/tempSettings/'+ getTempSettingsFile()
    logger.info("Settings file temporal: "+str(tempSettingsFile))

    execSh = 'docker cp '+str(user_id)+':/StaBitLizer/StaBitLizer/settings.py '+tempSettingsFile
    subprocess.call([execSh], shell=True)

    try:
        with open(tempSettingsFile,"rb") as file:
            content = file.readlines()

        with open(tempSettingsFile, 'w') as f:
            for item in content:
                theKey = item.decode('utf-8').split("=", 1)[0]
                for k,v in dict.items():
                    if str(k).strip() == str(theKey).strip():
                        logger.info("This is k: "+k+" , and theKey: "+theKey)
                        if ("API_KEY" in k or "API_SECRET" in k or "BASE_URL" in k):
                            item = (k+" = \""+v+"\"\n").encode('utf-8')
                        else:
                            item = (k+" = "+v+"\n").encode('utf-8')
                f.write("%s" % item.decode('utf-8'))
        logger.info("Settings file CORRECTLY MODIFIED")
    except:
        logger.info("Settings file not found")
    
    
    execSh = 'docker cp '+tempSettingsFile+' '+str(user_id)+':/StaBitLizer/StaBitLizer/settings.py'
    subprocess.call([execSh], shell=True)

def getSettings(user_id,sett):

    #TODO: change path by that extracted from DDBB
    tempSettingsFile = '/home/ubuntu/StaBitLizer_v1.1/tempSettings/'+ getTempSettingsFile()
    logger.info("Settings file temporal: "+str(tempSettingsFile))

    execSh = 'docker cp '+str(user_id)+':/StaBitLizer/StaBitLizer/settings.py '+tempSettingsFile
    subprocess.call([execSh], shell=True)

    try:
        with open(tempSettingsFile,"rb") as file:
            content = file.readlines()
            for item in content:
                theKey = item.decode('utf-8').split("=", 1)[0]
                logger.info("Value of theKey: "+theKey)
                try:
                    theValue = item.decode('utf-8').split("=", 1)[1]
                    #logger.info("Value of theValue: "+theValue)
                    if sett in theKey:
                        return theValue
                except:
                    continue
                    
        logger.info("Settings value correctly retrieved")
    except:
        logger.info("Settings value PROBLEM while retrieving")

def getActiveCredits(user_id):

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()
    logger.info("getActiveCredits()")

    try:
        sql = "SELECT credits_active FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()

def refresh(user_id, credits):

    sett1 = 'API_KEY'
    sett2 = 'API_SECRET'
    apiKey = getSettings(user_id,sett1).replace('"', '').strip()
    apiSecret = getSettings(user_id,sett2).replace('"', '').strip()
    
    try:

        # Leverage value of the user
        client = bitmex.bitmex(test=getTypeOfNet(), api_key=apiKey, api_secret=apiSecret)
        logger.info("Connected to BitMex as client: "+str(client))
        res = client.Position.Position_get().response().result
        leverageValue = res[0]['leverage']
       
        # Current price of bitcoin 
        priceBitcoin = getBTCUSDprice()

        bitcoin = float(credits)/10.0
        startStepValue = math.floor(float(priceBitcoin)/89.0*bitcoin*float(leverageValue))        
        logger.info("startStepValue set up to: "+str(startStepValue)+", priceBitcoin: "+str(priceBitcoin)+", number bitcoins: "+str(bitcoin)+", leverage: "+str(leverageValue))
 
        dictChange = {
            'ORDER_START_SIZE':str(startStepValue), 
            'ORDER_STEP_SIZE':str(startStepValue)
        }
        updateSettings(user_id, dictChange)
        
    except:
        logger.info("Exception when calling PositionApi->position_update_leverage")

def getBTCUSDprice():
    
    link = "https://api.coindesk.com/v1/bpi/currentprice/USD.json" 
    data = json.loads(requests.get(link).text)
    priceBitcoin = float(data['bpi']['USD']['rate_float'])
    return priceBitcoin


def checkIfUserHasCreditsForNextUpdate(user_id):

    # Get current funds of user_id
    balance = getFundsFrom(user_id)
    logger.info("checkIfUserHasCreditsForNextUpdate()")

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()
    try:
        sql = "SELECT credits_open FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        credits_open = int(result[0])
        creds = math.ceil(balance * 10)
        needed = abs(creds - credits_open)
        if credits_open >= creds:
            return True, creds, balance, needed
        else:
            return False, creds, balance, needed
    except:
        db.rollback()
        db.close()
        logger.info("Problem while retrieving data from MySQL")

def getTypeOfNet():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT value FROM config WHERE id = 6;"
        cursor.execute(sql)
        result = cursor.fetchone()
        if int(result[0]) == 1:
            return False
        else:
            return True
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()


def getFundsFrom(user_id):
    sett1 = 'API_KEY'
    sett2 = 'API_SECRET'
    apiKey = getSettings(user_id, sett1).replace('"', '').strip()
    apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

    try:
        bitmexx = ccxt.bitmex({
            'apiKey': apiKey,
            'secret': apiSecret,
        })
        # In case we have Keys from Testnet
        if getTypeOfNet():
            if 'test' in bitmexx.urls:
                bitmexx.urls['api'] = bitmexx.urls['test']

        bodyBalance = bitmexx.fetch_balance()

        # Funds in the account of the user and associated credits to the account
        funds = float(bodyBalance['BTC']['total'])
        return funds
    except:
        return -1

def getAllAccounts():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id, creation_date FROM user_data;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        logger.info("Problem while retrieving data from MySQL")
    db.close()

def checkIfUserMeets(user_id):
    f = '%Y-%m-%d %H:%M:%S'
    today = datetime.datetime.today()
    dateBarrier_ = '2019-04-30 00:00:01'
    dateBarrier = datetime.datetime.strptime(dateBarrier_, f)
    dateLast_ = str(utils.getLastCreditInsertion(user_id))
    dateLast = datetime.datetime.strptime(dateLast_, f)
    dateLastNM = dateLast
    
    userHasCredits = utils.checkIfUserHasCredits(user_id)
    #logger.info("dateLast: "+str(dateLast)+", DateLastNM: "+str(dateLastNM)+", dateBarrier: "+str(dateBarrier))
    if userHasCredits == 1:
        return 1, None
    else:
        if dateLast < dateBarrier:
            dateNextExp_ = '2019-05-07 00:00:01'
            dateNextExp = datetime.datetime.strptime(dateNextExp_, f)
            dateLast = dateBarrier
        else:
            dateNextExp = dateLast + timedelta(days=7)
            #if dateLast > dateNextExp:
            #    dateNextExp = datetime.datetime(dateLast.year, (dateLast.month+1)%12, dateLast.day+7)
            #if dateLast > dateNextExp:
            #    dateNextExp = datetime.datetime(dateLast.year+1, (dateLast.month+1)%12, dateLast.day+7)

    # Now we subtract the next date minus today to know how many days are left
    days = (dateNextExp - today).days

    if days >= 0:
        logger.info("=======================================")
        logger.info("dateLast: "+str(dateLast)+", DateLastNM: "+str(dateLastNM)+", DateNextExp: "+str(dateNextExp)+" Number of pending days: "+str(days))
        return 0, days
    else:
        logger.info("=======================================")
        logger.info("dateLast: "+str(dateLast)+", DateLastNM: "+str(dateLastNM)+", DateNextExp: "+str(dateNextExp)+" Number of pending days: "+str(days))
        return -1, days

def main():
        setOfUsers = getAllAccounts()
        logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+": routine to check time until deadline for users")
        for row in setOfUsers:
            try:
                user_id = row[0]
                outOf, days = checkIfUserMeets(user_id)
                if outOf == 1:
                    logger.info("=======================================")
                    logger.info("Checking user: "+str(user_id))
                    logger.info("User "+str(user_id)+' has enough credits. No need to send any notification')
                    logger.info("=======================================")
                    continue;
                elif outOf == 0:
                    logger.info("Checking user: "+str(user_id))
                    logger.info("User "+str(user_id)+' does not have credits, still before the deadline.')
                    bot.send_message(chat_id=user_id, text="WARNING: Dear User, as part of the new StaBitLizer user policy, we urge you to refill your credits as soon as possible. You still have "+str(days)+" day/s to do it, otherwise, your entire account (including API keys if existing) will be deleted. Please, contact your agent.")
                    logger.info("=======================================")
                else:
                    logger.info("=======================================")
                    logger.info("Checking user: "+str(user_id))
                    logger.info("User "+str(user_id)+' does not have credits, account is deleted.')
                    bot.send_message(chat_id=user_id, text="IMPORTANT: Dear User, in accordance with StaBitLizer's user policy, your account's credits have not been refilled for a period longer than 7 days. Thus your account has been entirely deleted from our system. In case you want to open a new account in StaBitLizer, please, use /create command. Thank you for your comprehension.")
                    logger.info("=======================================")
                    #utils.deleteContainerForUser(user_id)

                    # Let's remove from database
                    #utils.removeFromDB(user_id) 
                sys.stdout.flush() 
            except:
                logger.info("CRITICAL: Bot: "+str(row[0])+" not accepting messages")
                logger.info("Profile deleted, not accepting messages "+str(user_id))
                #utils.deleteContainerForUser(user_id)

                # Let's remove from database
                #utils.removeFromDB(user_id) 

                continue
  
if __name__== "__main__":
    main()
