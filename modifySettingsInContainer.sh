#!/bin/bash

leadingDir=$(printf "%07d" $1)

docker cp $leadingDir:/StaBitLizer/StaBitLizer/settings.py /home/ubuntu/setTemp/$leadingDir
vim /home/ubuntu/setTemp/$leadingDir
docker cp /home/ubuntu/setTemp/$leadingDir $leadingDir:/StaBitLizer/StaBitLizer/settings.py
