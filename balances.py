#!/usr/bin/env python3.6

import datetime
import logging
import os
import subprocess
import sys
import time

import MySQLdb
import telegram
import re
import utils

PATH = os.path.dirname(os.path.abspath(__file__))

def getBalanceData(user_id, db, cursor, priceBTCUSD):

    try:
        cmdScreen = 'screen -list | grep "'+str(format(int(user_id), '07'))+'"'
        outputC,error = subprocess.Popen(cmdScreen, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        symbol = str(utils.getSymbol())
        symbolOne = str(utils.getSymbol(1))
        if (os.path.isdir(PATH+"/"+str(user_id)) and outputC != b''):
            sql = "SELECT mail FROM user_data WHERE user_id="+str(user_id)+";"
            cursor.execute(sql)
            result = cursor.fetchone()
            value = str(result[0])
            cmd = 'tac '+PATH+'/'+str(user_id)+'/screenlog.0 | grep -m 1 -B 9999 -- "'+symbol+' Ticker" | grep "'+symbolOne+' Balance" -m 1 | sed "s/INFO - market_maker - //g"'
            output,error = subprocess.Popen(cmd, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            cmd2 = 'tac '+PATH+'/'+str(user_id)+'/screenlog.0 | grep -m 1 -B 9999 -- "'+symbol+' Ticker" | grep "Current Contract Position" -m 1 | sed "s/.*market_maker //g"'
            output2,error2 = subprocess.Popen(cmd2, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

            numChars = len(value)
            output = output.decode('utf-8').replace('Current ','')
            output = re.sub(',.*?-',' -', output)
            balanceFrom = float(re.findall("\d+\.\d+", output)[0])

            numChars2 = len(str(output).rstrip())
            
            tab1 = ""
            tab2 = ""

            if numChars > 23:
                tab1 = "\t"
            else:
                tab1 = "\t\t"

            if numChars2 > 55:
                tab2 = "\t"
            else:
                tab2 = "\t\t"

            output2 = output2.decode('utf-8').replace('Current Contract Position','Contracts')
            contractsFrom = float(re.findall(r'\d+', output2)[0])
            leverage = contractsFrom / (balanceFrom * priceBTCUSD) 
            retVal = value + tab1 + str(output).rstrip() + tab2 + str(output2).rstrip()+"\t"+" - Leverage: "+str("{:.2f}".format(leverage))

            return retVal
        elif (outputC != b''):
            sql = "SELECT user_id FROM user_data WHERE mail='"+str(user_id)+"';"
            cursor.execute(sql)
            result = cursor.fetchone()
            intuid = int(result[0])

            cmd = 'tac '+PATH+'/'+str(intuid)+'/screenlog.0 | grep -m 1 -B 9999 -- "'+symbol+' Ticker" | grep "'+symbolOne+' Balance" -m 1 | sed "s/INFO - market_maker - //g"'
            output,error = subprocess.Popen(cmd, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            cmd2 = 'tac '+PATH+'/'+str(intuid)+'/screenlog.0 | grep -m 1 -B 9999 -- "'+symbol+' Ticker" | grep "Current Contract Position" -m 1 | sed "s/.*market_maker //g"'
            output2,error2 = subprocess.Popen(cmd2, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            
            numChars = len(user_id)
            numChars2 = len(str(output.decode('utf-8')).rstrip())
            
            tab1 = ""
            tab2 = ""

            if numChars > 23:
                tab1 = "\t"
            else:
                tab1 = "\t\t"

            if numChars2 > 55:
                tab2 = "\t"
            else:
                tab2 = "\t\t"

            retVal = str(user_id) + tab1 + str(output.decode('utf-8')).rstrip() + tab2 + str(output2.decode('utf-8')).rstrip()+"\t"+" - Leverage: "+str(leverage)
            return retVal
        else:
            pass
        
    except:
        pass

def main():
    try:
        setOfUsers = utils.getAllAccountsActive(2)
        # MySQL statements
        db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
        cursor = db.cursor()
        priceBTCUSD = utils.getBTCUSDprice()
        
        for row in setOfUsers:
            user_id = row[0]
            val = getBalanceData(user_id, db, cursor, priceBTCUSD)
            if val is not None:
                print(str(val)+"\n")
        db.close()
        sys.stdout.flush() 
    except ValueError:
        logger.info("Problem while checking licenses")
  
if __name__== "__main__":
    main()
