#!/bin/bash

if ! pgrep -f 'ackqNEGMP.py'
then
    nohup /home/ubuntu/StaBitLizer_v1.1/ackqNEGMP.py & >> /home/ubuntu/StaBitLizer_v1.1/logs/ackqNEGMP.py.log
else
    echo "running"
fi

