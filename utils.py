import string
from sqlalchemy import create_engine
import pandas as pd
import matplotlib.pyplot as plt
from urllib.parse import quote_plus as urlquote
import random
import subprocess
import json
from io import BytesIO
import math
import MySQLdb
import requests
from requests import get
import secrets
import binascii, os
import boto3
import time
import urllib.request
import datetime
from bravado.exception import HTTPUnauthorized, HTTPForbidden, HTTPTooManyRequests
import re
import hmac
import urllib
from botocore.exceptions import ClientError
import jwt
import binascii
import hashlib
from datetime import timedelta
import persistqueue

PATH = os.path.dirname(os.path.abspath(__file__))
URL = get('https://api.ipify.org').text


############################
# Utils library

# Resolve message data to a readable name
def get_name(user):
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    try:
        name = user.first_name
    except (NameError, AttributeError):
        try:
            name = user.username
        except (NameError, AttributeError):
            return ""
    return name


def getTempSettingsFile(size=6, chars=string.ascii_uppercase + string.digits):
    """Returns random settings filename for recently captured settings.py file from Docker container

    Parameters
    ----------
    size : int, optional
        The total char length of the filename

    chars : char array, optional
        The array of chars from which the filename is generated

    Returns
    -------
    filename
        The temporal filename
    """

    return ''.join(random.choice(chars) for _ in range(size))


def updateSettings(user_id, dict):
    """Returns settings.py file from Docker container assigned to an account, modifies the particular settings embedded in the dict parameter, and moves it back to the container as settings.py

    Parameters
    ----------
    user_id : int, required
        ID of user in BotMex database

    dict : dictionary, required
        Dictionary object with parameters key:value to change in settings.py
    """
    tempSettingsFile = PATH + '/tempSettings/' + getTempSettingsFile()

    execSh = 'docker cp ' + str(format(int(user_id), '07'))  + ':/StaBitLizer/StaBitLizer/settings.py ' + tempSettingsFile
    print("Updating for user "+str(user_id)+" with execSh command as: "+str(execSh))
    subprocess.call([execSh], shell=True)
    updatedKeys = []

    try:
        with open(tempSettingsFile, "rb") as file:
            content = file.readlines()

        with open(tempSettingsFile, 'w') as f:
            for item in content:
                theKey = item.decode('utf-8').split("=", 1)[0]
                for k, v in dict.items():
                    if str(k).strip() == str(theKey).strip():
                        if ("API_KEY" in k or "API_SECRET" in k or "BASE_URL" in k):
                            item = (k + " = \"" + v + "\"\n").encode('utf-8')
                            updatedKeys.append(k)
                        else:
                            item = (k + " = " + v + "\n").encode('utf-8')
                            updatedKeys.append(k)
                f.write("%s" % item.decode('utf-8'))

            # We add keys that were not present before
            for k, v in dict.items():
                if k in updatedKeys:
                    continue
                else:
                    item = (k + " = " + v + "\n").encode('utf-8')
                    f.write("%s" % item.decode('utf-8'))
    except:
        print("Settings file not found")

    execSh = 'docker cp ' + tempSettingsFile + ' ' + str(format(int(user_id), '07')) + ':/StaBitLizer/StaBitLizer/settings.py'
    subprocess.call([execSh], shell=True)
    os.unlink(tempSettingsFile)


def getSettings(user_id, sett):
    """Returns value of parameter in settings.py file held inside the docker container of user with id user_id

    Parameters
    ----------
    user_id : int, required
        ID of user in BotMex database

    sett : str, required
        The parameter whose value has to be returned

    Returns
    -------
    theValue
        The value of the parameter sett in settings.py inside the docker container
    """

    tempSettingsFile = PATH + '/tempSettings/' + getTempSettingsFile()

    try:
        execSh = 'docker cp ' + str(format(int(user_id), '07'))  + ':/StaBitLizer/StaBitLizer/settings.py ' + tempSettingsFile
        subprocess.call([execSh], shell=True)

        with open(tempSettingsFile, "rb") as file:
            content = file.readlines()
            for item in content:
                theKey = item.decode('utf-8').split("=", 1)[0]
                try:
                    theValue = item.decode('utf-8').split("=", 1)[1]
                    if sett in theKey:
                        return theValue
                except IndexError:
                    continue
        os.unlink(tempSettingsFile)

    except (IOError, AttributeError):
        print("Settings value PROBLEM while retrieving")


def closePos(user_id):
    """Cancels all pending orders

    Parameters
    ----------
    user_id : int, required
        ID of user in BotMex database

    Returns
    -------
    1
        If operation was successful
    0
        If Docker instance didn't exist
    -1
        If API keys are not valid
    -2
        If access to BitMex API has been momentarily blocked
    """
    try:
        res = cancelOrders(user_id)
        print(str(res))
        return 1
    except IOError:
        print("ERROR: docker instance for user not created")
        return 0
    except HTTPUnauthorized:
        print("ERROR: API Keys not valid, we continue to the following one")
        return -1
    except (HTTPForbidden, HTTPTooManyRequests):
        print("ERROR: access to BitMex API was forbidden. Changing IP.")
        return -2


def closePosYES(user_id):
    """Closes current positions at market price

    Parameters
    ----------
    user_id : int, required
        ID of user in BotMex database

    Returns
    -------
    1
        If operation was successful
    0
        If Docker instance didn't exist
    -1
        If API keys are not valid
    -2
        If access to BitMex API has been momentarily blocked
    """
    try:
        symbol = getSymbol()
        execInst = 'Close'
        ordType = 'Market'
        res = closePosition(user_id=user_id, symbol=symbol, orderType=ordType, execInst=execInst)
        print(str(res))
        return 1
    except IOError:
        print("ERROR: docker instance for user not created")
        return 0
    except HTTPUnauthorized:
        print("ERROR: API Keys not valid, we continue to the following one")
        return -1
    except (HTTPForbidden, HTTPTooManyRequests):
        print("ERROR: access to BitMex API was forbidden. Changing IP.")
        return -2


def closePosSign(user_id, sign):
    """General function for closing positions of users depending on the sign of their position

    Parameters
    ----------
    user_id : int, required
        ID of user in BotMex database

    Returns
    -------
    1
        If operation was successful
    0
        If Docker instance didn't exist
    -1
        If API keys are not valid
    -2
        Different error
    """
    try:

        priceSell, priceBuy = getOrderBookL2(user_id)
        print(str(user_id) + ": priceSell / priceBuy: " + str(priceSell) + " and " + str(priceBuy))

        currentBTCUSD = getBTCUSDprice()
        funds = fetch_balance(user_id)
        funds = float(funds) / 1e8
        print(str(user_id) + ": " + str(funds))

        # Parameters
        symbol = getSymbol()
        ordQty = math.ceil(currentBTCUSD * funds)
        execInst = 'Close'

        if sign == +1:
            ordType = 'Limit'
            side = 'Sell'
            res = closePosition(user_id=user_id, symbol=symbol, orderType=ordType, orderQuantity=ordQty, side=side,
                                price=priceSell, execInst=execInst)
            print(str(user_id) + ": " + str(res))
        elif sign == 0:
            ordType = 'Market'
            res = closePosition(user_id=user_id, symbol=symbol, orderType=ordType, execInst=execInst)
            print(str(user_id) + ": " + str(res))
        elif sign == -1:
            ordType = 'Limit'
            side = 'Buy'
            res = closePosition(user_id=user_id, symbol=symbol, orderType=ordType, orderQuantity=ordQty, side=side,
                                price=priceSell, execInst=execInst)
            print(str(user_id) + ": " + str(res))
        return 1
    except (IOError, AttributeError):
        print("ERROR: docker instance for user not created")
        return 0
    except HTTPUnauthorized:
        print("ERROR: API Keys not valid, we continue to the following one")
        return -1
    except (HTTPForbidden, HTTPTooManyRequests):
        print("ERROR: access to BitMex API was forbidden. Changing IP.")
        time.sleep(3)
        return -2
    except SyntaxError:
        print("ERROR: Different error")


def createAccountDB(mail, telegram_id=None, password=None):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        credits_active = 0
        credits_open = 0
        if mail == "Julian.b@bitcoin.org.hk":
            credits_open = 1000000
        if telegram_id is None:
            telegram_id = 'NULL'
        if password is None:
            password = 'NULL'
        sql = "INSERT INTO user_data (telegram_id, mail, creation_date, credits_active, credits_open, pass) VALUES (" + str(
                telegram_id) + ",'" + str(mail) + "', NOW(), " + str(credits_active) + ", " + str(credits_open) + ",'" + str(
                password) + "');"
        print(sql)
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


def updateCreationDate(user_id):
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET creation_date = NOW() WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()

def getAllTelegramIDs(state=None):
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = ""
        if state is not None:
            sql = "SELECT user_id FROM user_data WHERE account_state = " + str(state) + " and credits_active > 0;"
            print(sql)
        else:
            sql = "SELECT telegram_id, user_id FROM user_data"
        cursor.execute(sql)
        result = cursor.fetchall()
        print(str(result))
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getAllAccounts(state=None):
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = ""
        if state is not None:
            sql = "SELECT user_id FROM user_data WHERE account_state = " + str(state) + " and credits_active > 0;"
            print(sql)
        else:
            sql = "SELECT user_id FROM user_data"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getAllAccountsActive(state=None):
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = ""
        if state is not None:
            sql = "SELECT user_id FROM user_data WHERE account_state = " + str(state) + " and credits_active > 0 ORDER BY mail;"
        else:
            sql = "SELECT user_id FROM user_data"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAllAccountsNonVerified():
    """Returns name from Telegram user. If not available, it tries to return username. Otherwise, it returns nothing

    Parameters
    ----------
    user : obj, required
        User object

    Returns
    -------
    name
        The name of the user
    """
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = ""
        sql = "SELECT user_id FROM user_data WHERE verified = 0;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAllAccountsWithDate():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id, creation_date FROM user_data;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAllTransactions():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT * FROM transactions_to_active;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAllTransfers():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT * FROM transactions"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def removeFromDB(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "INSERT INTO transactions_history SELECT * FROM transactions WHERE origin = " + str(user_id) + ";"
        sql2 = "INSERT INTO transactions_history SELECT * FROM transactions WHERE benefitiary = " + str(user_id) + ";"
        sql3 = "INSERT INTO transactions_to_active_history SELECT * FROM transactions_to_active WHERE user_id = " + str(
            user_id) + ";"
        sql4 = "DELETE FROM transactions WHERE origin = " + str(user_id) + ";"
        sql5 = "DELETE FROM transactions WHERE benefitiary = " + str(user_id) + ";"
        sql6 = "DELETE FROM transactions_to_active WHERE user_id = " + str(user_id) + ";"
        sql7 = "INSERT INTO user_data_history (user_id, telegram_id, setup_type, stop_state, account_state, credits_active, credits_open, creation_date, deletion_date) SELECT user_id, telegram_id, setup_type, stop_state, account_state, credits_active, credits_open, creation_date, NOW() FROM user_data WHERE user_id = " + str(
            user_id) + ";"
        sql8 = "DELETE FROM user_data WHERE user_id = " + str(user_id) + ";"

        cursor.execute(sql)
        cursor.execute(sql2)
        cursor.execute(sql3)
        cursor.execute(sql4)
        cursor.execute(sql5)
        cursor.execute(sql6)
        cursor.execute(sql7)
        cursor.execute(sql8)
        db.commit()
    except ValueError:
        print("Error when deleting")
        db.rollback()
    db.close()


def modifySettingsUpdateTypeInDB(user_id, typeUpd):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET setup_type = " + str(typeUpd) + " WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


def setNotifications(user_id, notif):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET notifications = " + str(notif) + " WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


def refreshCredits(user_id):
    state = int(getAccountState(user_id))

    if state == 1:
        return -1
    else:
        try:

            margin = fetch_balance(user_id)
            bitcoin = float(margin) / 1e8

            # Funds in the account of the user
            creds = math.ceil(bitcoin * 10)
            status, creds, balance, needed = moveCreditsFromOpen(user_id, bitcoin)

            # Leverage value of the user
            leverageValue = getLeverage(user_id)

            # Current price of bitcoin
            priceBitcoin = getBTCUSDprice()

            if (leverageValue > 5 or leverageValue < 1):
                leverageValue = 5
            startStepValue = math.floor(float(priceBitcoin) * bitcoin * 0.03)
            print("startStepValue set up to: " + str(startStepValue) + ", priceBitcoin: " + str(
                priceBitcoin) + ", number bitcoins: " + str(bitcoin) + ", leverage: " + str(leverageValue))

            dictChange = {
                'ORDER_START_SIZE': str(startStepValue),
                'ORDER_STEP_SIZE': str(startStepValue)
            }
            if status == 1:
                updateSettings(user_id, dictChange)
                print("Settings were adapted correctly. You consumed " + str(
                    needed) + " additional credits, according to your current balance: " + str(balance))
            elif status == 2:
                updateSettings(user_id, dictChange)
                print("Settings were adapted correctly. You consumed " + str(
                    abs(needed)) + " additional credits, according to your current balance: " + str(balance))
            elif status == 3:
                print(
                    "NOTE: you have the exact number of credits corresponding to your current balance. No need to refresh.")
            else:
                print(
                    "WARNING: your settings were not adapted. You don't have enough credits for your current balance.")
        except SyntaxError:
            print("Exception when calling /refresh function")


def refreshStopState(user_id, notif):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET stop_state = " + str(notif) + " WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


def setAccountState(user_id, value):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET account_state = " + str(value) + " WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


def sendCreditsToDB(user_id, receiver_id, credits):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET credits_open = credits_open - " + str(credits) + " WHERE user_id = " + str(
            user_id) + ";"
        sql2 = "UPDATE user_data SET credits_open = credits_open + " + str(credits) + " WHERE user_id = " + str(
            receiver_id) + ";"
        sql3 = "INSERT INTO transactions (date, origin, benefitiary, credits) VALUES (NOW()," + str(
            user_id) + "," + str(receiver_id) + "," + str(credits) + ");"

        cursor.execute(sql)
        cursor.execute(sql2)
        cursor.execute(sql3)
        db.commit()
    except:
        db.rollback()
    db.close()


def getNotifAvail(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT notifications FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        if int(result[0]) == 1:
            return 1
        else:
            return 0
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAvailableCredits(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT credits_open FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getBotMexDir():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 4;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getCreditPrice():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT value FROM config WHERE id = 3;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAccountCreationDate(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT creation_date FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getAccountState(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT account_state FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getStopState(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT stop_state FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getBotID():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 5;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getSymbol(remove=None):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 8;"
        cursor.execute(sql)
        result = cursor.fetchone()
        if remove is None:
            return str(result[0])
        else:
            stringi = str(result[0])
            return stringi[:-3]
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getBotID2():
    # MySQL statements

    try:
        return "840601444:AAF9TFztnqI7JUmcvTUcKLRuz5g-vWRl3DI"
    except:
        print("Problem while retrieving data from MySQL")


def getTypeOfNet():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT value FROM config WHERE id = 6;"
        cursor.execute(sql)
        result = cursor.fetchone()
        if int(result[0]) == 1:
            return False
        else:
            return True
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getURLOfNet():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 7;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getEC2ID():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT description FROM config WHERE id = 7;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def checkIfreceiverIdExists(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT count(*) FROM user_data WHERE user_id = " + str(receiver_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        toReturn = int(result[0])
        if toReturn == 0:
            return toReturn
        else:
            sql = "SELECT account_state FROM user_data WHERE user_id = " + str(receiver_id) + ";"
            cursor.execute(sql)
            result = cursor.fetchone()
            return int(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def checkIfMailExists(mail):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT count(*) FROM user_data WHERE mail = '" + str(mail) + "';"
        cursor.execute(sql)
        result = cursor.fetchone()
        toReturn = int(result[0])
        return toReturn
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getVerified(mail):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT verified FROM user_data WHERE mail = '" + str(mail) + "';"
        cursor.execute(sql)
        result = cursor.fetchone()
        toReturn = int(result[0])
        return toReturn
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def checkIfreceiverMailExists(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id FROM user_data WHERE mail = '" + str(receiver_id) + "';"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("No id associated to this email or bad mail format")
        return None
    db.close()


def checkIfTelegramIdExists(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT count(*) FROM user_data WHERE telegram_id = " + str(receiver_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        toReturn = int(result[0])
        if toReturn == 0:
            return toReturn
        else:
            sql = "SELECT account_state FROM user_data WHERE telegram_id = " + str(receiver_id) + ";"
            cursor.execute(sql)
            result = cursor.fetchone()
            return int(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def checkAccountState(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT account_state FROM user_data WHERE mail = '" + str(receiver_id) + "';"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("No id associated to this email or bad mail format")
        return None
    db.close()


def checkAccountStateID(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT account_state FROM user_data WHERE user_id = " + str(receiver_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("No id associated to this email or bad mail format")
        return None
    db.close()


def getUserMail(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT mail FROM user_data WHERE user_id = " + str(receiver_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getUserID(mail):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = 'SELECT user_id FROM user_data WHERE mail = "' + str(mail) + '";'
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getTelegramID(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = 'SELECT telegram_id FROM user_data WHERE user_id = ' + str(user_id) + ';'
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

def getUsersInPos(val):
    users = getAllAccounts(2)
    usersList = list()
    print(str(users))
    s = 0
    for row in users:
        s += 1
        print("DEALING WITH" + str(s))
        uid = row[0]

        timestamp = datetime.datetime.now()
        minu = timestamp.minute
        seco = timestamp.second
        if ((minu + 1) % 5 == 0 and seco > 45):
            time.sleep(30)
        sign = getCurrentPosSign(uid)  # Here is where it blocks
        print("Getting current position for " + str(uid))
        if sign > 0 and val > 0:
            usersList.append(uid)
        elif sign == 0 and val == 0:
            usersList.append(uid)
        elif sign == -1 and val < 0:
            usersList.append(uid)
    print(str(usersList))
    return usersList

def getUsersInPosFromLog(val):
    users = getAllAccounts(2)
    usersList = list()
    print(str(users))
    s = 0
    for row in users:
        s += 1
        print("DEALING WITH" + str(s))
        uid = row[0]
        sign = getCurrentPosSignFromLog(uid)  # Here is where it blocks
        print("Getting current position for " + str(uid))
        if sign > 0 and val > 0:
            usersList.append(uid)
        elif sign == 0 and val == 0:
            usersList.append(uid)
        elif sign == -1 and val < 0:
            usersList.append(uid)
    print(str(usersList))
    return usersList


def getCurrentPosSign(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'GET'
        get_order_url = '/api/v1/position'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + '/api/v1/position', headers=headers, proxies=proxies)
        position = int(res.json()[0]['currentQty'])

        print("Position for " + str(user_id) + ": " + str(position))

        if position < 0:
            return -1
        elif position == 0:
            return 0
        elif position > 0:
            return 1
    except:
        return -2

def getCurrentPosSignFromLog(user_id):
    try:
        symbol = getSymbol()
        cmd = 'tac ' + PATH + '/' + str(user_id) + '/screenlog.0 | grep -m 1 -B 9999 -- "' + symbol + ' Ticker" | grep "Current Contract Position" -m 1 | sed "s/ - INFO.*//g"'
        timestamp, error = subprocess.Popen(cmd, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        format = '%Y-%m-%d %H:%M:%S'
        currentTime = datetime.datetime.strptime(str(timestamp.decode('utf-8').split(",")[0]), format)

        cmd2 = 'tac ' + PATH + '/' + str(user_id) + '/screenlog.0 | grep -m 1 -B 9999 -- "' + symbol + ' Ticker" | grep "Current Contract Position" -m 1 | sed "s/.*Position: //g"'
        contracts, error2 = subprocess.Popen(cmd2, shell=True, executable="/bin/bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        position = float(contracts)

        print("Position for " + str(user_id) + ": " + str(position) + " and currentTime: "+str(currentTime))
        compare = datetime.datetime.now() - currentTime#datetime.timedelta(minutes=5)

        print("Total seconds difference: "+str(compare.total_seconds()))

        if compare.total_seconds() <= 300:
            if position < 0:
                return -1
            elif position == 0:
                return 0
            elif position > 0:
                return 1
        else:
            return -2
    except:
        return -2


def generateContainerForUser(user_id):
    bashCommand = 'docker run -d -it --name ' + str(format(int(user_id), '07')) + ' stabitlizer:latest python3'
    print(bashCommand)
    process = subprocess.Popen(bashCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, error = process.communicate()
    output = str(output.decode('utf-8'))
    print(output)
    if ('Error response' not in output):
        updateCreationDate(user_id)
    return output


def deleteContainerForUser(user_id):
    try:
        execSh = 'docker rm --force $(docker ps -a | grep "' + str(format(user_id, '07')) + '" | awk \'{print $1}\')'
        subprocess.call([execSh], shell=True)
    except:
        print('NOTICE: container not deleted. It didn\'t exist before')


def getCreditsOpen(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()
    try:
        sql = "SELECT credits_open FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        credits_open = int(result[0])
        db.close()
        return credits_open
    except:
        print("getCreditsOpen(): Problem while retrieving data from MySQL")
        db.close()
        return -1


def getMainID(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()
    try:
        sql = "SELECT user_id FROM user_data WHERE telegram_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        user_id = int(result[0])
        db.close()
        return user_id
    except:
        print("getMainID(): Problem while retrieving data from MySQL")
        db.close()
        return -1


def getCreditsActive(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()
    try:
        sql = "SELECT credits_active FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        credits_active = int(result[0])
        db.close()
        return credits_active
    except:
        print("getCreditsActive(): Problem while retrieving data from MySQL")
        db.close()
        return -1


def getLicenseRenewAccounts():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id FROM user_data WHERE DAY(creation_date) = DAY(CURDATE()) AND creation_date < CURDATE();"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def moveCreditsFromOpen(user_id, balance):
    try:
        credits_open = getCreditsOpen(user_id)
        active = getCreditsActive(user_id)
        creds = math.ceil(balance * 10)
        needed = creds - active
        if credits_open >= needed & needed > 0:
            # MySQL statements
            db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
            cursor = db.cursor()
            sql = "UPDATE user_data SET credits_active = credits_active + " + str(needed) + " WHERE user_id = " + str(
                user_id) + ";"
            sql2 = "UPDATE user_data SET credits_open = credits_open - " + str(needed) + " WHERE user_id = " + str(
                user_id) + ";"
            sql3 = "INSERT INTO transactions_to_active (date, user_id, credits_to_active, type) VALUES (NOW()," + str(
                user_id) + "," + str(needed) + ",1);"
            cursor.execute(sql)
            cursor.execute(sql2)
            cursor.execute(sql3)
            db.commit()
            db.close()
            return 1, creds, balance, needed
        elif active > creds:
            input = active - creds
            db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
            cursor = db.cursor()
            sql = "UPDATE user_data SET credits_active = " + str(creds) + " WHERE user_id = " + str(user_id) + ";"
            sql2 = "INSERT INTO transactions_to_active (date, user_id, credits_to_active, type) VALUES (NOW()," + str(
                user_id) + "," + str(input) + ",2);"
            cursor.execute(sql)
            cursor.execute(sql2)
            db.commit()
            db.close()
            return 2, creds, balance, needed
        elif credits_open >= needed & needed == 0:
            return 3, creds, balance, needed
        else:
            return 4, creds, balance, needed
    except:
        db.rollback()
        db.close()
        print("Problem while retrieving data from MySQL")
        return False, -1, -1, -1


def moveCreditsFromOpenAdmin(user_id, balance):
    try:
        credits_open = getCreditsOpen(user_id)
        active = getCreditsActive(user_id)
        creds = math.ceil(balance * 10)
        needed = creds - active
        if credits_open >= needed & needed > 0:
            return 1, creds, balance, needed
        elif active > creds:
            return 2, creds, balance, needed
        elif credits_open >= needed & needed == 0:
            return 3, creds, balance, needed
        else:
            return 4, creds, balance, needed
    except:
        print("Problem in moveCreditsFromOpenAdmin")
        return False, -1, -1, -1


def getPerk(user_id):
    try:
        sett1 = 'PERK'
        perk = getSettings(user_id, sett1).replace('"', '').strip()

        return perk
    except:
        print("HTTPUnauthoried caught")
        return -1


def getOrderPairs(user_id):
    try:
        sett1 = 'ORDER_PAIRS'
        op = int(getSettings(user_id, sett1).replace('"', '').strip()) * 2

        return op
    except:
        print("HTTPUnauthoried caught")
        return -1


def getOperationsData(user_id):
    try:
        lever, position = getLeverageAndPosition(user_id)
        balance = fetch_balance(user_id)

        print("Result from getting data (leverage, position, and balance): " + str(lever) + ", " + str(
            position) + ", " + str(balance))
        return lever, position, balance
    except:
        print("Problem when getting leverage: an ERROR occurred")
        return -1, -1, -1


def checkIfAPIKeysValid(user_id):
    try:
        lever, position = getLeverageAndPosition(user_id)
        if lever == -1:
            return False
        else:
            return True
    except:
        print("API Keys not working")
        return False


def getAnalytics(user_id):
    # ====== Connection ====== #
    # Connecting to mysql by providing a sqlachemy engine
    engine = create_engine('mysql://juanba1984:%s@localhost:3306/BotMex' % urlquote('PePe8810!@'), echo=False)
    df = pd.read_sql('SELECT * FROM analytics WHERE user_id = ' + str(user_id), engine)
    df['date'] = pd.to_datetime(df['date'])
    df.index = df['date']
    del df['date']
    return df


def generateGraphCreditsVsBalance(user_id):
    df = getAnalytics(user_id)
    if not df.empty:
        df1 = df[['credits_active']]
        df2 = df[['balance']] / 1e8

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.set_ylabel("Credits")
        ax2.set_ylabel("Balance ("+str(getSymbol())+")")

        lns1 = df1.plot(ax=ax1, label='Credits')
        lns2 = df2.plot(ax=ax2, label='Balance ('+str(getSymbol())+')', ls='--')
        ax2.get_legend().remove()

        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        labels = ['Credits']
        labels2 = ['Balance ('+str(getSymbol())+')']
        ax1.legend(lines + lines2, labels + labels2, loc='upper center',
                   title='Comparison between balance and credits for current user', bbox_to_anchor=(0.5, 1.15), ncol=2,
                   borderaxespad=0, frameon=False)

        fig.tight_layout()  # otherwise the right y-label is slightly clipped

        buffer = BytesIO()
        fig.savefig(buffer, format='PNG')

        return buffer
    else:
        return -1


def generateGraphPositionVsPerformance(user_id):
    df = getAnalytics(user_id)
    if not df.empty:
        df1 = df[['position']]
        df2 = (df[['balance']] / df.loc[df.index.min()]['balance'] - 1.0) * 100

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.set_ylabel("Position")
        ax2.set_ylabel("Performance (%)")

        lns1 = df1.plot(ax=ax1, label='Position')
        lns2 = df2.plot(ax=ax2, label='Performance (%)', color='r')
        ax2.get_legend().remove()

        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        labels = ['Position']
        labels2 = ['Performance (%)']
        ax1.legend(lines + lines2, labels + labels2, loc='upper center',
                   title='Evolution of position and performance for current user', bbox_to_anchor=(0.5, 1.15), ncol=2,
                   borderaxespad=0, frameon=False)

        fig.tight_layout()  # otherwise the right y-label is slightly clipped

        buffer = BytesIO()
        fig.savefig(buffer, format='png')

        return buffer
    else:
        return -1


def generate_pass():
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(8))
    return password


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def insert_hash_in_ddbb(user_id, stored_password):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET pass = '" + stored_password + "' WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        db.commit()
    except:
        print("There was a problem")
        db.rollback()
    db.close()

def insert_hash_in_ddbb_mail(mail, stored_password):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    user_id = getUserID(mail)
    print(str(user_id))

    try:
        sql = "UPDATE user_data SET pass = '" + stored_password + "' WHERE mail = '"+ mail +"';"
        cursor.execute(sql)
        db.commit()
    except ValueError:
        print("There was a problem")
        db.rollback()
    db.close()

def getActiveCredits(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()
    print("getActiveCredits()")

    try:
        sql = "SELECT credits_active FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return int(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def setActiveCreditsToZero(user_id):
    credits_active = getActiveCredits(user_id)
    print("setActiveCreditsToZero()")
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "UPDATE user_data SET credits_active = 0 WHERE user_id = " + str(user_id) + ";"
        sql2 = "INSERT INTO transactions_to_active (date, user_id, credits_to_active, type) VALUES (NOW()," + str(
            user_id) + "," + str(credits_active) + ",2);"

        cursor.execute(sql)
        cursor.execute(sql2)
        db.commit()
    except:
        db.rollback()
        print("Problem while updating user account (settings type) in MySQL database")
    db.close()

def setVerificationTrue(mail):

    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = 'UPDATE user_data SET verified = 1 WHERE mail = "' + str(mail) + '";'

        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
        print("Problem while updating user account (settings type) in MySQL database")
    db.close()

def linkAccounts(user_id, telegram_id):

    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = 'UPDATE user_data SET telegram_id = '+str(telegram_id)+' WHERE user_id = ' + str(user_id) + ';'

        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
        print("Problem while updating user account (settings type) in MySQL database")
    db.close()


def getDockerStatus(user_id):
    bashCommand = 'docker ps -a | grep ' + str(format(int(user_id), '07'))
    process = subprocess.Popen(bashCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, error = process.communicate()
    output = str(output.decode('utf-8'))

    if str(user_id) in output:
        return True
    else:
        return False


def getBotStatus(user_id):
    bashCommand = '/home/ubuntu/StaBitLizer_v1.1/statusNumber ' + str(user_id)
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                               stdin=subprocess.PIPE)
    output, error = process.communicate()
    output = str(output.decode('utf-8'))

    return output


def getAccountCreationDate(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT creation_date FROM user_data WHERE user_id = " + str(receiver_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def checkIfUserHasCredits(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT credits_open, credits_active FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        credits_open = result[0]
        credits_active = result[1]
        if (credits_open == 0 and credits_active == 0):
            return 0
        else:
            return 1
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def getLastCreditInsertion(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT date FROM transactions WHERE benefitiary = " + str(user_id) + " ORDER BY id DESC LIMIT 1;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        sql = "SELECT creation_date FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return (result[0])

    db.close()


def getFirstCreditInsertion(user_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT date FROM transactions WHERE benefitiary = " + str(user_id) + " ORDER BY id ASC LIMIT 1;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        sql = "SELECT creation_date FROM user_data WHERE user_id = " + str(user_id) + ";"
        cursor.execute(sql)
        result = cursor.fetchone()
        return (result[0])

    db.close()


def getAllAccountsAndMails():
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id, mail FROM user_data;"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()


def generate_signature(secret, verb, url, nonce, data):
    # Parse the url so we can remove the base and extract just the path.
    parsedURL = urllib.parse.urlparse(url)
    print(parsedURL)
    path = parsedURL.path
    if parsedURL.query:
        path = path + '?' + parsedURL.query
    # print "Computing HMAC: %s" % vesourb + path + str(nonce) + data
    message = (verb + path + str(nonce) + data).encode('utf-8')

    signature = hmac.new(secret.encode('utf-8'), message, digestmod=hashlib.sha256).hexdigest()
    return signature


def fetch_balance(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'GET'
        get_order_url = '/api/v1/user/margin'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + '/api/v1/user/margin', headers=headers, proxies=proxies)
        balance = res.json()['marginBalance']

        return balance
    except:
        return -1


def fetch_balanceWithKeys(apiKey, apiSecret):
    try:
        api_method = 'GET'
        get_order_url = '/api/v1/user/margin'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + '/api/v1/user/margin', headers=headers, proxies=proxies)
        balance = res.json()['marginBalance']

        return balance
    except ValueError:
        return -1

def getLeverageFromAnalytics(user_id, db, cursor):

    try:
        sql = "SELECT leverage FROM analytics WHERE user_id = "+str(user_id)+" ORDER BY date DESC LIMIT 1;"
        cursor.execute(sql)
        result = cursor.fetchone()
        return float(result[0])
    except:
        print("Problem while retrieving data from MySQL")

def getLeverage(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'GET'
        get_order_url = '/api/v1/position'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + '/api/v1/position', headers=headers, proxies=proxies)
        leverage = res.json()[0]['leverage']

        return leverage
    except:
        return -1


def getLeverageAndPosition(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'GET'
        get_order_url = '/api/v1/position'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + '/api/v1/position', headers=headers, proxies=proxies)
        leverage = res.json()[0]['leverage']
        position = res.json()[0]['currentQty']

        return (leverage, position)
    except ValueError:
        return (-1, -1)


def cancelOrders(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()
        api_method = 'DELETE'
        get_order_url = '/api/v1/order/all'

        expires = int(round(time.time() + 3600))
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, '')
        print(apiSignature)

        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.delete(getURLOfNet() + get_order_url, headers=headers, proxies=proxies)
        print(res.text)

        return res.text
    except:
        return -1


def closePosition(user_id, symbol, orderType, execInst, orderQuantity=None, side=None, price=None):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'POST'
        get_order_url = '/api/v1/order'

        expires = int(round(time.time() + 3600))
        data = {}
        if price is None:
            data = {
                "symbol": symbol,
                "ordType": orderType,
                "execInst": execInst
            }
        else:
            data = {
                "symbol": symbol,
                "ordType": orderType,
                "execInst": execInst,
                "orderQty": orderQuantity,
                "side": side,
                "price": price
            }

        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, json.dumps(data))
        print(apiSignature)

        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey,
            'Content-Type': 'application/json',
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.post(getURLOfNet() + get_order_url, data=json.dumps(data), headers=headers, proxies=proxies)

        return res.text
    except:
        return -1


def getOrderBookL2(user_id):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()
        api_method = 'GET'
        get_order_url = '/api/v1/orderBook/L2'

        expires = int(round(time.time() + 3600))
        data = {
            "symbol": getSymbol(),
            "depth": 8
        }
        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, json.dumps(data))
        print(apiSignature)
        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey,
            'Content-Type': 'application/json',
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.get(getURLOfNet() + get_order_url, data=json.dumps(data), headers=headers, proxies=proxies)
        priceSell = res.json()[0]['price']
        priceBuy = res.json()[-1]['price']

        return (priceSell, priceBuy)
    except:
        return (-1, -1)


def updateLeverage(user_id, leverage):
    try:
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = getSettings(user_id, sett1).replace('"', '').strip()
        apiSecret = getSettings(user_id, sett2).replace('"', '').strip()

        api_method = 'POST'
        get_order_url = '/api/v1/position/leverage'

        expires = int(round(time.time() + 3600))

        data = {
            "symbol": getSymbol(),
            "leverage": str(leverage)
        }

        apiSignature = generate_signature(apiSecret, api_method, get_order_url, expires, json.dumps(data))
        print(apiSignature)

        headers = {
            'api-expires': str(expires),
            'api-signature': apiSignature,
            'api-key': apiKey,
            'Content-Type': 'application/json',
        }

        proxies = {
            'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
        }

        res = requests.post(getURLOfNet() + get_order_url, data=json.dumps(data), headers=headers, proxies=proxies)

        return res.text
    except:
        return -1


def getBTCUSDprice():
    symbol = getSymbol()
    symbol = symbol[:-3]
    link = "https://www.bitmex.com/api/v1/trade?symbol="+str(symbol)+"&count=1&reverse=true"

    proxies = {
        'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
    }

    res = json.loads(requests.get(link, proxies=proxies).text)
    priceBitcoin = float(res[0]['price'])

    return priceBitcoin


def getBTCUSDpriceAll():
    symbol = getSymbol()
    symbol = symbol[:-3]
    link = "https://www.bitmex.com/api/v1/trade?symbol="+str(symbol)+"&count=1&reverse=true"
    linkIP = "https://ipinfo.io/ip"

    proxies = {
        'https': 'https://lum-customer-stabilizer-zone-zone3:p0gc12l3dlye@zproxy.lum-superproxy.io:22225/'
    }

    res = json.loads(requests.get(link, proxies=proxies).text)

    priceBitcoin = str(res[0]['price'])
    timestamp = str(res[0]['timestamp'])

    return (priceBitcoin, timestamp)

def sendMigrationMail(mail, telegram_id, user_id):

    link = generateMigrationToken(mail, telegram_id, user_id)
    print(link)

    SENDER = "StaBitLizer Support <juanba.tomas@gmail.com>"
    AWS_REGION = "eu-west-1"
    SUBJECT = "StaBitLizer Telegram migration service"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("Dear User.\r\n"
                 "You received this mail because you decided to link your API and StaBitLizer accounts.\r\n"
                 "Please, proceed by clicking in the link below\r\n\n"
                 +link
                 )

    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h1>StaBitLizer Telegram migration service</h1>
      <p>You received this mail because you decided to link your API and StaBitLizer accounts.</p>
      <p>Please, proceed by clicking in the link below:
        <a href='"""+link+"""'>Migration link</a></p>
    </body>
    </html>
                """

    # The character encoding for the email.
    CHARSET = "UTF-8"

    client = boto3.client('ses', region_name=AWS_REGION)

    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    mail,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

def sendVerificationMail(recipient):

    link = generateRegToken(recipient)
    print(link)

    SENDER = "StaBitLizer Support <juanba.tomas@gmail.com>"
    AWS_REGION = "eu-west-1"
    SUBJECT = "StaBitLizer email verification service"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("Dear User.\r\n"
                 "You received this mail because you created an account in StaBitLizer.\r\n"
                 "Please, verify your addres by clicking in the link below\r\n\n"
                 +link
                 )

    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h1>StaBitLizer email verification service</h1>
      <p>You received this mail because you created an account in StaBitLizer.</p>
      <p>Please, verify your addres by clicking in the link below:
        <a href='"""+link+"""'>Validation Link</a></p>
    </body>
    </html>
                """

    # The character encoding for the email.
    CHARSET = "UTF-8"

    client = boto3.client('ses', region_name=AWS_REGION)

    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

def sendPassUpdateMail(mail, password):

    link = generateUpdToken(mail, password)
    print(link)

    SENDER = "StaBitLizer Support <juanba.tomas@gmail.com>"
    AWS_REGION = "eu-west-1"
    SUBJECT = "StaBitLizer password update service"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("Dear User.\r\n"
                 "You received this mail because you wish to update your password in StaBitLizer\r\n"
                 "Please, click in the link below to update your password with the credentials you submitted.\r\n\n"
                 +link
                 )

    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h1>StaBitLizer password update service</h1>
      <p>You received this mail because you wish to update your password in StaBitLizer</p>
      <p>Please, click in the link below to update your password with the credentials you submitted:
        <a href='"""+link+"""'>Link</a></p>
    </body>
    </html>
                """

    # The character encoding for the email.
    CHARSET = "UTF-8"

    client = boto3.client('ses', region_name=AWS_REGION)

    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    mail,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

def generateToken(user_id):
    encoded = jwt.encode({'user_id': str(user_id), 'expiration_time': str(datetime.datetime.now() + timedelta(minutes=30))},
                         'A76sh-@je!5', algorithm='HS256')
    print(encoded)
    return encoded


def generateRegToken(mail):
    encoded = jwt.encode({'mail': str(mail), 'expiration_time': str(datetime.datetime.now() + timedelta(minutes=10))},
                         'Bh98+-1dDO', algorithm='HS256')
    link = 'http://'+URL+':12430/api/v1.0/checkReg/'+encoded.decode("utf-8")
    return link

def generateUpdToken(mail, password):
    encoded = jwt.encode({'mail' : mail, 'password': str(password), 'expiration_time': str(datetime.datetime.now() + timedelta(minutes=10))},
                         'd7FJ8!d##3fJ', algorithm='HS256')
    link = 'http://'+URL+':12430/api/v1.0/applyPass/'+encoded.decode("utf-8")
    return link

def generateMigrationToken(mail, telegram_id, user_id):
    encoded = jwt.encode({'mail': str(mail), 'telegram_id': str(telegram_id), 'user_id': str(user_id), 'expiration_time': str(datetime.datetime.now() + timedelta(minutes=10))},
                         'j?(Hk5423', algorithm='HS256')
    link = 'http://'+URL+':12430/api/v1.0/checkMigration/'+encoded.decode("utf-8")
    return link

def decodeToken(encoded):
    decoded = jwt.decode(encoded, 'A76sh-@je!5', algorithms=['HS256'])
    print(decoded)
    return decoded

def decodeRegToken(encoded):
    decoded = jwt.decode(encoded, 'Bh98+-1dDO', algorithms=['HS256'])
    print(decoded)
    return decoded

def decodeMigToken(encoded):
    decoded = jwt.decode(encoded, 'j?(Hk5423', algorithms=['HS256'])
    print(decoded)
    return decoded

def decodeUpdToken(encoded):
    decoded = jwt.decode(encoded, 'd7FJ8!d##3fJ', algorithms=['HS256'])
    print(decoded)
    return decoded

def verify_password(mail, provided_password):
    """Verify a stored password against one provided by user"""

    stored_password = getPassFromUserMail(mail)
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

def checkPass(password):

    r_p = re.compile('^(?=\S{8,20}$)(?=.*?\d)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^A-Za-z\s0-9])')
    res = re.search(r_p, password)
    if res is None:
        return False
    else:
        return True

def getPassFromUserMail(receiver_id):
    # MySQL statements
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT pass FROM user_data WHERE mail = '" + str(
            receiver_id) + "';"
        cursor.execute(sql)
        result = cursor.fetchone()
        return str(result[0])
    except:
        print("No id associated to this email or bad mail format")
        return None
    db.close()


def createAccount(apiKey, apiSecret, user_id):
    generateContainerForUser(user_id)

    # At account creation, we must decide if we are in testnet or not
    if getTypeOfNet():
        base_url = 'https://testnet.bitmex.com/api/v1/'
    else:
        base_url = 'https://www.bitmex.com/api/v1/'

    dictChange = {
        'API_KEY': apiKey,
        'API_SECRET': apiSecret,
        'BASE_URL': base_url
    }

    # Now that the instance and the account have been created, now settings have to be updated too
    updateSettings(user_id, dictChange)
    setAccountState(user_id, 1)


def checkIfDockerContainerExists(user_id):
    execSh = 'docker ps -a | grep ' + str(format(int(user_id), '07'))
    process = subprocess.Popen(execSh.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output is None:
        return False
    else:
        return True


def closeSign(message):
    messageP = ''
    ackqNEG = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVE')
    ackqNEU = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEUTRAL')
    ackqNEGMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queueNEGATIVEMP')
    ackqPOSMP = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVEMP')
    ackqPOS = persistqueue.SQLiteAckQueue('/home/ubuntu/StaBitLizer_v1.1/queuePOSITIVE')

    if message.lower() == '-':
        print("Closing NEG")
        usersHere = getUsersInPosFromLog(-1)
        for uid in usersHere:
            print("Closing NEG for user " + str(uid))
            ackqNEG.put(uid)
    elif message.lower() == '=':
        print("Closing NEU")
        usersHere = getAllAccounts()
        us = 0
        for uid_ in usersHere:
            uid = uid_[0]
            ackqNEU.put(uid)
    elif message.lower() == '-mp':
        usersHere = getUsersInPosFromLog(-1)
        print("EEEE" + str(usersHere))
        for uid in usersHere:
            ackqNEGMP.put(uid)
            print("Closing NEG MP for user " + str(uid))
    elif message.lower() == '+mp':
        usersHere = getUsersInPosFromLog(+1)
        print(str(usersHere))
        for uid in usersHere:
            ackqPOSMP.put(uid)
            print("Closing POS MP for user " + str(uid))
    elif message.lower() == '+':
        print("Closing POS")
        usersHere = getUsersInPosFromLog(+1)
        for uid in usersHere:
            print("Closing POS for user " + str(uid))
            ackqPOS.put(uid)


def perkUpd(user_id, value):
    try:
        print("Perk value" + str(value))
        dictChange = {
            'PERK': str(value)
        }
        updateSettings(user_id, dictChange)

    except ValueError:
        print("Exception when calling PositionApi->position_update_leverage")


def changePerkForAll(value):
    setOfUsers = getAllAccounts(2)
    userNum = 0
    for row in setOfUsers:
        user_id = -1
        try:
            user_id = row[0]
            sett1 = 'PERK'
            userNum += 1
            print("Updating for user: " + str(user_id))
            perkUpd(user_id, value)
            print("Now with user: " + str(userNum))
            print("--------------------------------------")

        except SyntaxError:
            print("Continuing for user: " + str(user_id))
            print("Now with user: " + str(userNum))
            print("--------------------------------------")
            continue
