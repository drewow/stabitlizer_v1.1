#!/bin/bash

users="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT user_id from user_data;')"

for D in `echo ${users}`
do
      leadingDir=$(printf "%07d" $D)
      docker_instance="$(docker ps -a | grep $leadingDir | awk '{print $1}')"
      docker cp /home/ubuntu/StaBitLizer_v1.1/DockerExp/StaBitLizer/market_maker/custom_strategy.py $docker_instance:/StaBitLizer/StaBitLizer/market_maker/custom_strategy.py

done
