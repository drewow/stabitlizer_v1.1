#!/usr/bin/env python3.6

import os
import time
import sys
import yaml
import ccxt
import subprocess
import logging
import bitmex
import json
import string
import random
import requests
import math
from pathlib import Path
import datetime
import MySQLdb
import telegram
import utils
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from telegram import MessageEntity, TelegramObject, ChatAction

PATH = os.path.dirname(os.path.abspath(__file__))


def loopIntUpd(user_id):

    try:

        dictChange = {
            'LOOP_INTERVAL':str(5)
        }
        utils.updateSettings(user_id, dictChange)

    except ValueError:
        print("Exception when calling PositionApi->position_update_leverage")

def main():
    setOfUsers = utils.getAllAccounts()
    userNum = 0
    for row in setOfUsers:
        try:
            user_id = row[0]
            userNum += 1
            print("--------------------------------------")
            print("Checking perk for user: "+str(user_id))
            print("Updating for user: "+str(user_id))
            loopIntUpd(user_id)
            print("Now with user: "+str(userNum))
            print("--------------------------------------")

        except:
            print("Continuing for user: "+str(user_id))
            print("Now with user: "+str(userNum))
            print("--------------------------------------")
            continue
    sys.stdout.flush() 
  
if __name__== "__main__":
    main()
