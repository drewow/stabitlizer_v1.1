#!/usr/bin/env python3.6

import datetime
import logging
import math
import os
import sys
import MySQLdb
import telegram

import utils

PATH = os.path.dirname(os.path.abspath(__file__))

"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(utils.getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=utils.getBotID())


def checkIfUserHasCreditsForNextUpdate(user_id):

    # Get current funds of user_id
    balance = float(utils.fetch_balance(user_id)/1e8)
    logger.info("checkIfUserHasCreditsForNextUpdate()")

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()
    try:
        sql = "SELECT credits_open FROM user_data WHERE user_id = "+str(user_id)+";"
        cursor.execute(sql)
        result = cursor.fetchone()
        credits_open = int(result[0])
        creds = math.ceil(balance * 10)
        needed = abs(creds - credits_open)
        if credits_open >= creds:
            return True, creds, balance, needed
        else:
            return False, creds, balance, needed
    except:
        db.rollback()
        db.close()
        logger.info("Problem while retrieving data from MySQL")

def checkingAmongWeek(creation_date):
    today = datetime.datetime.today()
    f = '%Y-%m-%d %H:%M:%S'

    # Get day of creat_date
    dateNextExp = datetime.datetime(today.year, today.month, creation_date.day)

    # First check if the next expiration date is in the same month
    if today > dateNextExp:
        dateNextExp = datetime.datetime(today.year, (today.month+1)%12, creation_date.day)
        if today > dateNextExp:
            dateNextExp = datetime.datetime(today.year+1, (today.month+1)%12, creation_date.day)

    # Now we subtract the next date minus today to know how many days are left
    days = (dateNextExp - today).days
    logger.info("Number of pending days: "+str(days))

    if days <= 7:
        return True, days
    else:
        return False, days

def main():
    try:
        setOfUsers = utils.getAllAccountsWithDate()
        logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+": routine to check time until deadline for users")
        for row in setOfUsers:
            user_id = row[0]
            creation_date = row[1]
            logger.info("=======================================")
            logger.info("Checking user: "+str(user_id))
            among, days = checkingAmongWeek(creation_date)
            if not among:
                logger.info("User "+str(user_id)+' is NOT in the 7-day period of the ongoing month. No need to send any notification')
                continue;
            # Now, depending on the number of credits the user has in open credits, we need to update his active credits
            status, creds, balance, needed = checkIfUserHasCreditsForNextUpdate(user_id)
            if status == True:
                logger.info("User "+str(user_id)+' is in the 7-day period of the ongoing month. Sending notification (enough credits)')
                bot.send_message(chat_id=user_id, text="The current monthly period will finish in "+str(days)+" day/s. In accordance with your current balance ("+str(balance)+" BTC), you have enough credits to continue with the next period. However, you can purchase more credits if you wish so. In that case, please, contact your agent.")
            else:
                logger.info("User "+str(user_id)+' is in the 7-day period of the ongoing month. Sending notification (NOT enough credits)')
                bot.send_message(chat_id=user_id, text="WARNING: the current monthly period will finish in "+str(days)+" day/s. In accordance with your current balance ("+str(balance)+" BTC), you don't have enough credits ("+str(creds)+") to update your license after expiration of the current period. You need to purchase at least "+str(needed)+" more credits to keep your license active after the current period expires. Please, contact you agent.")

            logger.info("=======================================")
        sys.stdout.flush() 
    except:
        print("Problem while checking licenses")
  
if __name__== "__main__":
    main()
