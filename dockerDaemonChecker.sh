#!/bin/bash

bot="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT description FROM config WHERE id=5;')"
KEY="${bot}"
TIME="10"
URL="https://api.telegram.org/bot$KEY/sendMessage"
TEXT="ERROR: docker daemon stopped working. Trying to relaunch..."

if [ $(systemctl show --property ActiveState docker) = "ActiveState=active" ]; then 
	echo yup; 
else 
        curl -s --max-time $TIME -d "chat_id=423136569&disable_web_page_preview=1&text=$TEXT" $URL >/dev/null;
	systemctl restart docker
fi
