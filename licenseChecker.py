#!/usr/bin/env python3.6

import datetime
import logging
import os
import subprocess
import sys
import time

import MySQLdb
import telegram

import utils

PATH = os.path.dirname(os.path.abspath(__file__))


"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(utils.getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=utils.getBotID())

def getFromSettings(param, user_id):

    try:
        value = utils.getSettings(user_id,param).replace('"', '').strip()
        return value
    except:
        return -1

def sendAnalytics(user_id, credits_active, bot_running):
    
    try:
        #user_id
        dateTime = time.strftime('%Y-%m-%d %H:%M:%S')
        #bot_running
        credits_open = utils.getCreditsOpen(user_id)
        #credits_active
        bitcoin_price = utils.getBTCUSDprice()
        leverage, position, balance = utils.getOperationsData(user_id)
        ORDER_PAIRS = getFromSettings('ORDER_PAIRS',user_id)
        ORDER_START_SIZE = getFromSettings('ORDER_START_SIZE',user_id)
        ORDER_STEP_SIZE = getFromSettings('ORDER_STEP_SIZE',user_id)
        INTERVAL = getFromSettings('INTERVAL',user_id)
        MIN_SPREAD = getFromSettings('MIN_SPREAD',user_id)
        MAINTAIN_SPREADS = getFromSettings('MAINTAIN_SPREADS',user_id)
        RELIST_INTERVAL = getFromSettings('RELIST_INTERVAL',user_id)
        CHECK_POSITION_LIMITS = getFromSettings('CHECK_POSITION_LIMITS',user_id)
        MIN_POSITION = getFromSettings('MIN_POSITION',user_id)
        MAX_POSITION = getFromSettings('MAX_POSITION',user_id)
        POST_ONLY = getFromSettings('POST_ONLY',user_id)
        DRY_RUN = getFromSettings('DRY_RUN',user_id)
        LOOP_INTERVAL = getFromSettings('LOOP_INTERVAL',user_id)
        API_REST_INTERVAL = getFromSettings('API_REST_INTERVAL',user_id)
        API_ERROR_INTERVAL = getFromSettings('API_ERROR_INTERVAL',user_id)
        TIMEOUT = getFromSettings('TIMEOUT',user_id)
        RETRIES = getFromSettings('RETRIES',user_id)
        RETRY_DELAY = getFromSettings('RETRY_DELAY',user_id)
        DRY_BTC = getFromSettings('DRY_BTC',user_id)

        # MySQL statements
        db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
        cursor = db.cursor()

        sql = "INSERT INTO analytics (user_id, date, bot_running, credits_open, credits_active, balance, position, bitcoin_price, leverage, ORDER_PAIRS, ORDER_START_SIZE, ORDER_STEP_SIZE, INTERVAL_FIELD, MIN_SPREAD, MAINTAIN_SPREADS, RELIST_INTERVAL, CHECK_POSITION_LIMITS, MIN_POSITION, MAX_POSITION, POST_ONLY, DRY_RUN, LOOP_INTERVAL, API_REST_INTERVAL, API_ERROR_INTERVAL, TIMEOUT, RETRIES, RETRY_DELAY, DRY_BTC) VALUES ("+str(user_id)+",'"+str(dateTime)+"',"+str(bot_running)+", "+str(credits_open)+", "+str(credits_active)+", "+str(balance)+", "+str(position)+", "+str(bitcoin_price)+", "+str(leverage)+", "+str(ORDER_PAIRS)+", "+str(ORDER_START_SIZE)+", "+str(ORDER_STEP_SIZE)+", "+str(INTERVAL)+", "+str(MIN_SPREAD)+", "+str(MAINTAIN_SPREADS)+", "+str(RELIST_INTERVAL)+", "+str(CHECK_POSITION_LIMITS)+", "+str(MIN_POSITION)+", "+str(MAX_POSITION)+", "+str(POST_ONLY)+", "+str(DRY_RUN)+", "+str(LOOP_INTERVAL)+", "+str(API_REST_INTERVAL)+", "+str(API_ERROR_INTERVAL)+", "+str(TIMEOUT)+", "+str(RETRIES)+", "+str(RETRY_DELAY)+", "+str(DRY_BTC)+");"
        logger.info(sql)
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
        logger.info("Problem while inserting data into MySQL")
    db.close()

def screen_present(user_id):

    bashCommand = 'screen -ls'
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    var = str(output.decode('utf-8'))

    if "."+str(user_id)+"\t(" in var:
        return True
    else:
        return False

def main():
    try:
        setOfUsers = utils.getAllAccounts()
        logger.info("\n")
        logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+": license checking routine")
        for row in setOfUsers:
            user_id = row[0]
            logger.info("===================================================================================================================================================================================================")
            logger.info("Checking license for user: "+str(user_id))

            # Let's send analytics to DDBB
            user_credits = float(utils.getAvailableCredits(user_id))

            screen_pre = screen_present(user_id)
            if screen_pre == False:
                sendAnalytics(user_id, user_credits, 0)
                logger.info("No screen running, continue to next user")
                logger.info("=======================================")
                continue
            else:
                sendAnalytics(user_id, user_credits, 1)

            notif = utils.getNotifAvail(user_id)
            if user_credits == 0:
        
                # Let's start the process and inform the user
                execSh = PATH+'/kill '+str(user_id)
                subprocess.call([execSh], shell=True)

                logger.info(str(user_id)+": User does not have any credits, bot must stop")
                if notif == 1:
                    bot.send_message(chat_id=user_id, text="WARNING: you don't have enough active credits to keep launching the bot. Account suspended. Please, contact your partner to update your license.")
            else:
                logger.info("User has enough credits. Bot status correct.")

            logger.info("=======================================")
        sys.stdout.flush() 
    except ValueError:
        logger.info("Problem while checking licenses")
  
if __name__== "__main__":
    main()
