#!/bin/bash

docker exec 423136569 sh -c " 	
    cd 'StaBitLizer'

    # Let's check if previous log exists and gzip
    if [ ! -f ./screenlog.0 ];
    then
   	echo 'No previous session to gzip'
    else
	echo 'Compressing previous screenlog.0 session file'
	echo 'Starting new session...'
	gzip -c screenlog.0 > screenlog.0_`date '+%F_%T'`.gz
        mv screenlog.0_`date '+%F_%T'`.gz ./log/
	rm screenlog.0
    fi
"

