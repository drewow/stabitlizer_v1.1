#!/usr/bin/env python3.6

import os
import subprocess
import sys

import MySQLdb

import utils

PATH = os.path.dirname(os.path.abspath(__file__))

modifyInitial = True


def getFromSettings(param, user_id):
    try:
        value = utils.getSettings(user_id, param).replace('"', '').strip()
        return value
    except:
        return -1

def initialChangesInDB():
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    try:
        # MySQL statements
        cursor = db.cursor()

        sql = "ALTER TABLE `BotMex`.`user_data` ADD COLUMN `telegram_id` INT(11) NULL DEFAULT NULL AFTER `user_id`, ADD UNIQUE INDEX `telegram_id_UNIQUE` (`telegram_id` ASC); ALTER TABLE `BotMex`.`user_data` CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL AUTO_INCREMENT ;"
        sql2 = "ALTER TABLE `BotMex`.`transactions` DROP FOREIGN KEY `benef_fk`,DROP FOREIGN KEY `orig_fk`;ALTER TABLE `BotMex`.`transactions` ADD CONSTRAINT `benef_fk`  FOREIGN KEY (`benefitiary`)  REFERENCES `BotMex`.`user_data` (`user_id`)  ON DELETE CASCADE  ON UPDATE CASCADE,ADD CONSTRAINT `orig_fk`  FOREIGN KEY (`origin`)  REFERENCES `BotMex`.`user_data` (`user_id`)  ON DELETE CASCADE  ON UPDATE CASCADE;"
        sql3 = "ALTER TABLE `BotMex`.`transactions_to_active` DROP FOREIGN KEY `type_fk`,DROP FOREIGN KEY `user_id_fk`;ALTER TABLE `BotMex`.`transactions_to_active` ADD CONSTRAINT `type_fk`  FOREIGN KEY (`type`)  REFERENCES `BotMex`.`type_active` (`id`)  ON DELETE CASCADE  ON UPDATE CASCADE,ADD CONSTRAINT `user_id_fk`  FOREIGN KEY (`user_id`)  REFERENCES `BotMex`.`user_data` (`user_id`)  ON DELETE CASCADE  ON UPDATE CASCADE;"
        sql4 = "ALTER TABLE `BotMex`.`authentication` ADD CONSTRAINT `user_id_user_data`  FOREIGN KEY (`user_id`)  REFERENCES `BotMex`.`user_data` (`user_id`)  ON DELETE CASCADE  ON UPDATE CASCADE;"
        sql5 = "ALTER TABLE `BotMex`.`user_data_history` ENGINE = MyISAM ;"
        sql6 = "ALTER TABLE `BotMex`.`transactions_history` ENGINE = MyISAM ;"
        sql7 = "ALTER TABLE `BotMex`.`user_data_history` ADD COLUMN `telegram_id` INT(11) NULL DEFAULT NULL AFTER `user_id`;"

        cursor.execute(sql)
        cursor.execute(sql2)
        cursor.execute(sql3)
        cursor.execute(sql4)
        cursor.execute(sql5)
        cursor.execute(sql6)
        cursor.execute(sql7)
        db.commit()
    except:
        db.rollback()
        print("Problem while inserting data into MySQL")
    db.close()

def replace_userId_by_aiid(user_id, aiid):
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    try:
        # MySQL statements
        cursor = db.cursor()

        sql = "UPDATE user_data SET telegram_id = "+str(user_id)+", user_id = "+str(aiid)+" WHERE user_id = "+str(user_id); # Mirar aquí con un print el porqué falla

        cursor.execute(sql)
        db.commit()
    except SyntaxError:
        db.rollback()
        print("replace_userId_by_aiid: Problem while inserting data into MySQL")
    db.close()

def replace_userId_by_aiid_in_history(user_id, aiid):
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    try:
        # MySQL statements
        cursor = db.cursor()

        sql = "UPDATE user_data_history SET telegram_id = "+str(user_id)+", user_id = "+str(aiid)+" WHERE user_id = "+str(user_id); # Mirar aquí con un print el porqué falla

        cursor.execute(sql)
        db.commit()
    except SyntaxError:
        db.rollback()
        print("replace_userId_by_aiid: Problem while inserting data into MySQL")
    db.close()

def replace_myISAM_tables(user_id, aiid):
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    try:
        # MySQL statements
        cursor = db.cursor()

        sql = "UPDATE analytics SET user_id = "+str(aiid)+" WHERE user_id = "+str(user_id)+";"  # analytics
        sql2 = "UPDATE transactions_history SET origin = " + str(aiid) + " WHERE origin = " + str(user_id)+";"  # transactions_history
        sql3 = "UPDATE transactions_history SET benefitiary = " + str(aiid) + " WHERE benefitiary = " + str(user_id) + ";"  # transactions_history
        sql4 = "UPDATE transactions_to_active_history SET user_id = " + str(aiid) + " WHERE user_id = " + str(user_id) + ";"  # transactions_to_active_history
        sql5 = "UPDATE user_data_history SET telegram_id = "+str(user_id)+", user_id = " + str(aiid) + " WHERE user_id = " + str(user_id) + ";"  # user_data_history

        cursor.execute(sql)
        cursor.execute(sql2)
        cursor.execute(sql3)
        cursor.execute(sql4)
        cursor.execute(sql5)
        db.commit()
    except SyntaxError:
        db.rollback()
        print("replace_userId_by_aiid: Problem while inserting data into MySQL")
    db.close()

def replace_myISAM_tables_history(user_id, aiid):
    db = MySQLdb.connect("localhost", "juanba1984", "PePe8810!@", "BotMex")
    try:
        # MySQL statements
        cursor = db.cursor()

        sql = "UPDATE analytics SET user_id = "+str(aiid)+" WHERE user_id = "+str(user_id)+";"  # analytics
        sql2 = "UPDATE transactions_history SET origin = " + str(aiid) + " WHERE origin = " + str(user_id)+";"  # transactions_history
        sql3 = "UPDATE transactions_history SET benefitiary = " + str(aiid) + " WHERE benefitiary = " + str(user_id) + ";"  # transactions_history
        sql4 = "UPDATE transactions_to_active_history SET user_id = " + str(aiid) + " WHERE user_id = " + str(user_id) + ";"  # transactions_to_active_history

        cursor.execute(sql)
        cursor.execute(sql2)
        cursor.execute(sql3)
        cursor.execute(sql4)
        db.commit()
    except SyntaxError:
        db.rollback()
        print("replace_userId_by_aiid: Problem while inserting data into MySQL")
    db.close()

def changeDockerContainerName(user, aiid):
    #bashCommand2 = 'docker rename '+str(user)+' '+str(format(int(aiid), '07'))
    utils.generateContainerForUser(aiid)
    bashCommand2 = 'docker cp /home/ubuntu/backupKeys/'+str(user) + " " + str(format(int(aiid), '07')) + ':/StaBitLizer/StaBitLizer/settings.py'
    print(bashCommand2)
    process2 = subprocess.Popen(bashCommand2, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output2, error = process2.communicate()
    outputSecond = str(output2.decode('utf-8'))
    print(outputSecond)

def deleteContainerForUser(telegram_id):
    try:
        execSh = 'docker rm --force $(docker ps -a | grep "' + str(telegram_id) + '" | awk \'{print $1}\')'
        print(execSh)
        subprocess.call([execSh], shell=True)
    except:
        print('NOTICE: container not deleted. It didn\'t exist before')

def changeNameOfFolder(user, aiid):
    bashCommand2 = 'mv ./'+str(user)+' ./'+str(aiid)
    process2 = subprocess.Popen(bashCommand2, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output2, error = process2.communicate()
    outputSecond = str(output2.decode('utf-8'))
    print(outputSecond)

def main():
    try:
        #if modifyInitial:
        #    initialChangesInDB()

        # Steps 1, 2, 3, 4, 5, and 6
        setOfUsers = utils.getAllTelegramIDs()
        print("\n")
        ai_id = 0
        for row in setOfUsers:
            #ai_id += 1

            user_id = row[0]
            aiid = row[1]

            print(
                "===================================================================================================================================================================================================")
            print("Modifying data for user: " + str(user_id) + ": "+str(aiid))

            # Let's modify (step 7)
            #replace_userId_by_aiid(user_id, ai_id)

            # Let's modify (step 8)
            #replace_myISAM_tables(user_id, ai_id)

            # Step 9: change MASTER_ACCOUNT in config (by hand)

            # Step 10: adapt the whole script with new user_id

            # Step 11: change name of docker containers
            #changeDockerContainerName(user_id, aiid)
            deleteContainerForUser(user_id)

            # Step 12: change name of all folders
            #changeNameOfFolder(user_id, ai_id)

            print("=======================================")

        # setOfGoneUsers = getGoneAccounts()
        # print("\n")
        # ai_id = 0
        # for row in setOfGoneUsers:
        #     ai_id += 1
        #     user_id = row[0]
        #     print("===================================================================================================================================================================================================")
        #     print("Modifying data for user: " + str(user_id)+" in user_data_history")
        #
        #     # Let's modify (step 7)
        #     replace_userId_by_aiid_in_history(user_id, ai_id)
        #
        #     # Let's modify (step 8)
        #     replace_myISAM_tables_history(user_id, ai_id)
        #
        #     print("=======================================")

        sys.stdout.flush()
    except ValueError:
        print("Problem while checking licenses")

def getGoneAccounts():

    # MySQL statements
    db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
    cursor = db.cursor()

    try:
        sql = "SELECT user_id FROM user_data_history WHERE telegram_id IS NULL"
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except:
        print("Problem while retrieving data from MySQL")
    db.close()

if __name__ == "__main__":
    main()
