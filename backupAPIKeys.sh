#!/bin/bash

users="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT user_id from user_data;')"

for D in `echo ${users}`
do
      leadingDir=$(printf "%07d" $D)
	    docker_instance="$(docker ps -a | grep $leadingDir | awk '{print $1}')"
	    if [ ! -z "$docker_instance" ]; then
          docker cp $docker_instance:/StaBitLizer/StaBitLizer/settings.py /home/ubuntu/backupKeys/$D
      fi

done
