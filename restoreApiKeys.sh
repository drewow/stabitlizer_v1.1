#!/bin/bash

users="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT user_id from user_data;')"
num=0
bot="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT description FROM config WHERE id=5;')"
KEY="${bot}"
URL="https://api.telegram.org/bot$KEY/sendMessage"
TIME="10"
TEXT="IMPORTANT NOTICE: due to some changes in BitMex policy, we urge you to modify your API keys to keep running your bot in StaBitLizer. Please, use /updateKeys command and do it as soon as possible. Thank you and sorry for the inconvenience."
for D in `echo ${users}`
do

        open="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT credits_open from user_data WHERE user_id = '$D';')"
        active="$(mysql --defaults-extra-file=/home/ubuntu/.my.config.cnf BotMex -s -N -e 'SELECT credits_active from user_data WHERE user_id = '$D';')"
	if [ "$open" -gt 0 ] || [ "$active" -gt 0 ]; then
		result="$(./statusNumber $D | grep 'OK')"
	    if [ -z "$result" ]; then
	        printf "Bot $D not running\n"
                #curl -s --max-time $TIME -d "chat_id=$D&disable_web_page_preview=1&text=$TEXT" $URL >/dev/null;
            fi
	fi
done
