#!/bin/bash

if ! pgrep -f 'ackqPOSMP.py'
then
    nohup /home/ubuntu/StaBitLizer_v1.1/ackqPOSMP.py & >> /home/ubuntu/StaBitLizer_v1.1/logs/ackqPOSMP.py.log
else
    echo "running"
fi

