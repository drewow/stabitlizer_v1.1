#!/usr/bin/env python3.6

import utils
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import subprocess
import os
import mmap
import datetime
import math

def main():

    scope = ['https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/StaBitLizer_v1.1/client_secret.json', scope)
    client = gspread.authorize(creds)

    # Open a worksheet from spreadsheet with one shot
    sheet = client.open("SBLAccountStatusSummary")
    worksheet2 = sheet.worksheet('SBLStatus')

    #list_of_hashes = worksheet2.get_all_records()
    #print(list_of_hashes)

    users = utils.getAllAccounts()
    cells = []
    dateNow = datetime.datetime.now()
    cells.append(gspread.Cell(1, 1, "LAST UPDATE: "+str(dateNow)))
    row_num = 1
    lenUsers = len(users)
    print("This is the length of users array: "+str(lenUsers))
    rounds = math.floor(lenUsers / 50)
    modul = lenUsers % 50
    print("This is the number of rounds of users array: "+str(rounds))
    print("This is the mod of users array: "+str(modul))
    for k in range(rounds):
        cells = []
        lim_min = k * 50
        lim_max = (k +1) * 50 - 1 
        if lim_max > lenUsers - 1:
            lim_max = lenUsers - 1
        for user in users[lim_min:lim_max]:
            print("\n\n\n\n\n\nEEEEEEEEEEEEEEEEEEE: "+str(row_num))
            user_id = user[0]
            print("====== User "+str(user)+"=======")
            mail = utils.getUserMail(user_id)
            openCredits = utils.getCreditsOpen(user_id)
            activeCredits = utils.getCreditsActive(user_id)
            state = utils.getAccountState(user_id)
            dockerStatus = utils.getDockerStatus(user_id)
            status = utils.getBotStatus(user_id)
            statusKeys = utils.checkIfAPIKeysValid(user_id)
            creationDate = utils.getAccountCreationDate(user_id)
            oss1 = utils.getSettings(user_id, 'ORDER_START_SIZE')
            oss2 = utils.getSettings(user_id, 'ORDER_STEP_SIZE')
            leverage = utils.getLeverage(user_id)
            perk = utils.getPerk(user_id)
            op = utils.getOrderPairs(user_id)

            cells.append(gspread.Cell(row_num + 1, 1, str(user_id)))
            cells.append(gspread.Cell(row_num + 1, 2, mail))
            cells.append(gspread.Cell(row_num + 1, 3, str(openCredits)))
            cells.append(gspread.Cell(row_num + 1, 4, str(activeCredits)))
            cells.append(gspread.Cell(row_num + 1, 5, str(state)))
            cells.append(gspread.Cell(row_num + 1, 6, str(dockerStatus)))
            cells.append(gspread.Cell(row_num + 1, 7, str(status)))
            cells.append(gspread.Cell(row_num + 1, 8, str(statusKeys)))
            cells.append(gspread.Cell(row_num + 1, 9, str(creationDate)))
            cells.append(gspread.Cell(row_num + 1, 14, str(oss1)))
            cells.append(gspread.Cell(row_num + 1, 15, str(oss2)))
            cells.append(gspread.Cell(row_num + 1, 16, str(leverage)))
            cells.append(gspread.Cell(row_num + 1, 17, str(perk)))
            cells.append(gspread.Cell(row_num + 1, 18, str(op)))
        

            if not dockerStatus:
                cells.append(gspread.Cell(row_num + 1, 10, -4))
                cells.append(gspread.Cell(row_num + 1, 11, "NOTE: Docker container not ready"))
            else:
                if state == 1:
                    cells.append(gspread.Cell(row_num + 1, 10, -3))
                    cells.append(gspread.Cell(row_num + 1, 11, "IMPORTANT: User didn't initially set up API keys. Necessary to continue"))
                else:
                    if statusKeys == False:
                        cells.append(gspread.Cell(row_num + 1, 10, -2))
                        cells.append(gspread.Cell(row_num + 1, 11, "WARNING: the user already inserted API keys, but they are not valid anymore. He/She needs to do it again"))
                    else:
                        if activeCredits == 0 and openCredits == 0:
                            cells.append(gspread.Cell(row_num + 1, 10, -1))
                            cells.append(gspread.Cell(row_num + 1, 11, "WARNING: the user can't run his/her bot because he doesn't have any credits"))
                        elif activeCredits == 0 and openCredits > 0:
                            cells.append(gspread.Cell(row_num + 1, 10, 0))
                            cells.append(gspread.Cell(row_num + 1, 11, "WARNING: the user has credits, but needs to /refresh to redeem them."))
                        elif activeCredits > 0:
                            if 'OK' in status:
                                cells.append(gspread.Cell(row_num + 1, 10, 1))
                                cells.append(gspread.Cell(row_num + 1, 11, 'GOOD: the bot is running OK'))
                            else:
                                cells.append(gspread.Cell(row_num + 1, 10, 2))
                                try:
                                    with open('./'+str(user_id)+'/screenlog.0', 'rb', 0) as file, \
                                        mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                                        if s.find(b'out of funds') != -1:
                                            cells.append(gspread.Cell(row_num + 1, 11,'WARNING: this account is out of funds. Please, refill your account in BitMex'))
                                        elif s.find(b'Access Denied') != -1:
                                            cells.append(gspread.Cell(row_num + 1, 11,'WARNING: this account\'s API access has been blocked by BitMex. Please, contact BitMex tech support '))
                                        else:
                                             cells.append(gspread.Cell(row_num + 1, 11,'WARNING: your bot is stopped manually by you. You will need to type /run to relaunch it.'))
                                except:
                                     cells.append(gspread.Cell(row_num + 1, 11,'WARNING: your bot is stopped manually by you. You will need to type /run to relaunch it.'))
                        else:
                            cells.append(gspread.Cell(row_num + 1, 10, -10))
                            cells.append(gspread.Cell(row_num + 1, 11,'Other info -10'))
            row_num += 1

        print("\n\n\n\n\n\ndsfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfEEEEEEEEEEEEEEEEEEE: ")
        try:
            worksheet2.update_cells(cells)
        except:
            sleep(10)
            scope = ['https://www.googleapis.com/auth/drive']
            creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/StaBitLizer_v1.1/client_secret.json', scope)
            client = gspread.authorize(creds)

            # Open a worksheet from spreadsheet with one shot
            sheet = client.open("SBLAccountStatusSummary")
            worksheet2 = sheet.worksheet('SBLStatus')

            worksheet2.update_cells(cells)


if __name__ == '__main__':
    main()
