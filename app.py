#!/usr/bin/env python3.6
import binascii
import hashlib
from datetime import datetime
from datetime import timedelta

import requests
import MySQLdb
import math
import subprocess
import jwt
import os
import textVars
from flask import Flask, jsonify, make_response, request
import csv

import utils

app = Flask(__name__)
#CORS(app, support_credentials=True)

PATH = os.path.dirname(os.path.abspath(__file__))

@app.route('/api/v1.0/start/<string:token>', methods=['GET'])
def start(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            user_id = utils.getUserID(mail)
            userExists = utils.checkIfreceiverIdExists(user_id)
            message = ''

            if userExists == 0:
                textToParse = textVars.MESSAGES['welcomeNOT'] % "User"
                textToParse2 = textVars.MESSAGES['disclaimer']
                textToParse3 = "NOTE: by creating your account you accept the disclaimer and terms of use of StaBitLizer"
                message = '\n\n'.join([textToParse, textToParse2, textToParse3])
            else:
                if userExists == 1:
                    textToParse = textVars.MESSAGES['welcome1'] % "User"
                    textToParse2 = textVars.MESSAGES['disclaimer']
                    message = '\n\n'.join([textToParse, textToParse2])
                else:
                    textToParse = textVars.MESSAGES['welcome'] % "User"
                    textToParse2 = textVars.MESSAGES['disclaimer']
                    message = '\n\n'.join([textToParse, textToParse2])

            return jsonify({'Message' : message})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/welcome/<string:token>', methods=['GET'])
def welcome(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            user_id = utils.getUserID(mail)
            userExists = utils.checkIfreceiverIdExists(user_id)
            textToParse = ''

            if userExists == 0:
                textToParse = textVars.MESSAGES['welcomeNOT'] % "User"
            else:
                if userExists == 1:
                    textToParse = textVars.MESSAGES['welcome1'] % "User"
                elif (user_id == 13053900 or user_id == 423136569):
                    textToParse = textVars.MESSAGES['welcomeExt'] % "Admin"
                else:
                    textToParse = textVars.MESSAGES['welcome'] % "User"
            return jsonify({'Message' : textToParse})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/graphs/<string:token>', methods=['GET'])
def get_graph(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif mail == 'juanba.tomas@gmail.com' or mail == 'Julian.b@bitcoin.org.hk':
            db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
            cur = db.cursor()
            cur.execute("SELECT user_data.mail, date, balance FROM analytics INNER JOIN user_data ON user_data.user_id = analytics.user_id;")
            r = [dict((cur.description[i][0], value) for i, value in enumerate(row)) for row in cur.fetchall()]
            r3 = [{s['mail'] : {k : v for k, v in s.items() if k != 'mail'}} for s in r]
            r2 = {}  
            for s in r:
                if s['mail'] in r2:
                    r2[s['mail']].append({k : v for k, v in s.items() if k != 'mail'})
                else:
                    r2[s['mail']] = [{k : v for k, v in s.items() if k != 'mail'}]

            if len(r2) == 0 :
                return jsonify({'Error' : 'No analytics for this user yet'})
            else:
                return jsonify({'analytics' : r2})
        else:
            db = MySQLdb.connect("localhost","juanba1984","PePe8810!@","BotMex")
            cur = db.cursor()
            cur.execute("SELECT balance, user_id, date FROM analytics WHERE (balance <> -1 AND user_id = (SELECT user_id FROM user_data WHERE mail = '"+str(decoded['user_id'])+"'));")
            r = [dict((cur.description[i][0], value) for i, value in enumerate(row)) for row in cur.fetchall()]
            if len(r) == 0 :
                return jsonify({'Error' : 'No analytics for this user yet'})
            else:
                return jsonify({'analytics' : r})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/leverage/<string:token>', methods=['GET'])
def leverage(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(utils.getUserID(mail)):
            return jsonify({'error': "docker container not existing"})
        else:
            leverage = utils.getLeverage(utils.getUserID(mail))
            if leverage == -1:
                return jsonify({'Error' : 'Leverage was not taken correctly'})
            else:
                return jsonify({'leverage' : leverage})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/bias/<string:token>', methods=['GET'])
def bias(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(utils.getUserID(mail)):
            return jsonify({'error': "docker container not existing"})
        else:
            perk = utils.getPerk(utils.getUserID(mail))
            if perk == -1:
                return jsonify({'Error' : 'Bias value was not taken correctly'})
            else:
                return jsonify({'perk' : perk})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/mail/<string:token>', methods=['GET'])
def mail(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            return jsonify({'mail' : mail})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/credits/<string:token>', methods=['GET'])
def credits(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            user_id = utils.getUserID(mail)
            balance = float(utils.fetch_balance(user_id)) / 1e8
            credits_open = utils.getCreditsOpen(user_id)
            credits_active = utils.getCreditsActive(user_id)
            credits_needed = math.ceil(balance * 10)
            additional_credits = credits_needed - credits_active
            creds_to_buy = credits_needed - credits_open

            formatDate = '%Y-%m-%d %H:%M:%S'
            creation_date = datetime.strptime(utils.getAccountCreationDate(user_id), formatDate)
            current_date = datetime.now()
            # TODO Replace 30 by the real number of days until the next license revision
            pendingDays = (creation_date.day - current_date.day) % 30
            if pendingDays == 0:
                pendingDays = 30

            if credits_open is None:
                return jsonify({'Message' : 'Account not existing yet in StaBitLizer'})
            else:
                if additional_credits > 0:
                    if additional_credits <= credits_open:
                        return jsonify({'Message' : "Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. User can type /refresh to activate unused credits in his/her account and catch up with current balance (still "+str(pendingDays)+" days remaining until the current license period expires)"})
                    else:
                        return jsonify({'Message' : "Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. User does not have enough credit reserves to catch up with current balance. User shall contact agent to acquire at least "+str(creds_to_buy)+" credits to match current balance (still "+str(pendingDays)+" remaining days until the current license period expires)."})
                else:
                    return jsonify({'Message' : "Active credits: "+str(credits_active)+". Unused credits: "+str(credits_open)+". Account balance: "+str(balance)+" BTC. No need to refresh. NOTE: still "+str(pendingDays)+" remaining days until the current license period expires."})

    except ValueError:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/usdbtc/<string:token>', methods=['GET'])
def usdbtc(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        exists = utils.checkIfreceiverMailExists(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now() or not exists):
            return jsonify({'error' : "invalid token"})
        else:
            price = utils.getBTCUSDprice()
            return jsonify({'USDBTC' : price})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/balances/<string:token>', methods=['GET'])
def balances(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        print(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(utils.getUserID(mail)):
            return jsonify({'error': "docker container not existing"})
        else:
            sender_id = int(utils.getUserID(mail))
            print(sender_id)
            if sender_id == 13053900 or sender_id == 423136569:
                bashCommand = PATH +'/balancesAPI'
                process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()
                outputFirst = str(output.decode('utf-8'))
                return jsonify({'Message': outputFirst})
            else:
                return jsonify({'Message': 'You must be ADMIN to run this command'})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/status/<string:token>', methods=['GET'])
def status(token):
    try:
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(utils.getUserID(mail)):
            return jsonify({'error': "docker container not existing"})
        else:
            user_id = int(utils.getUserID(mail))
            print(str(user_id))

            if (utils.checkAccountStateID(user_id) > 1):
                lever, position, balance = utils.getOperationsData(user_id)
                account_state = utils.getAccountState(user_id)
                stop_state = utils.getStopState(user_id)

                output = 'Current Balance: ' + str(balance / 1e8)
                output3 = 'Current Contract Position: ' + str(position)

                bashCommand2 = './statusPython ' + str(user_id) + ' ' + str(account_state) + ' ' + str(stop_state)
                process2 = subprocess.Popen(bashCommand2.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
                output2, error = process2.communicate()

                outputSecond = str(output2.decode('utf-8'))
                creditsInAccount = utils.getCreditsActive(user_id)
                dateNow = str(datetime.now())

                if 'not found' in outputSecond:
                    return jsonify({'Message': "WARNING: Make sure your API keys are setup correctly before continuing"})
                elif 'not currently running' not in outputSecond:
                    return jsonify(
                        {'Message': outputSecond+"\nDate: "+dateNow.split('.')[0]+"\n\n"+output+"\n\n"+output3+"\n\nCurrent credits balance: "+str(creditsInAccount)})
                else:
                    return jsonify({'Message': outputSecond})
            else:
                return jsonify({'Message': "WARNING: you need to set API keys first to get your status"})
    except:
        return jsonify({'error' : "invalid token"})

@app.route('/api/v1.0/checkReg/<string:token>', methods=['GET'])
#@cross_origin(supports_credentials=True)
def checkReg(token):
    try:
        decoded = utils.decodeRegToken(token)
        mail = decoded['mail']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "token expired"})
        elif not utils.checkIfMailExists(mail):
            return jsonify({'error': "this mail does not belong to any user"})
        else:
            utils.setVerificationTrue(mail)
            return jsonify({'message' : 'account verified'})
    except:
        return jsonify({'error': "invalid credentials"})

@app.route('/api/v1.0/checkMigration/<string:token>', methods=['GET'])
#@cross_origin(supports_credentials=True)
def checkMigration(token):
    try:
        decoded = utils.decodeMigToken(token)
        mail = decoded['mail']
        telegram_id = decoded['telegram_id']
        user_id = decoded['user_id']
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "token expired"})
        elif not utils.checkIfMailExists(mail):
            return jsonify({'error': "this mail does not belong to any user"})
        else:
            utils.linkAccounts(user_id, telegram_id)
            return jsonify({'message' : 'accounts linked'})
    except:
        return jsonify({'error': "invalid credentials"})

@app.route('/api/v1.0/applyPass/<string:token>', methods=['GET'])
#@cross_origin(supports_credentials=True)
def applyPass(token):
    try:
        decoded = utils.decodeUpdToken(token)
        password = decoded['password']
        mail = decoded['mail']

        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "token expired"})
        elif not utils.checkIfMailExists(mail):
            return jsonify({'error': "this mail does not belong to any user"})
        else:
            stored_password = utils.hash_password(password)
            utils.insert_hash_in_ddbb_mail(mail, stored_password)
            return jsonify({'message' : 'password updated correctly'})
    except:
        return jsonify({'error': "invalid process"})

@app.route('/api/v1.0/create', methods=['POST'])
#@cross_origin(supports_credentials=True)
def create():
    try:
        if not request.json or not 'mail' in request.json or not 'password' in request.json:
            return jsonify({'error' : "data missing"})
        else:
            apiKey = "secret_bf2db269199cbeca643e8126a3564a27"
            mailMsg = request.json['mail']
            mailExists = utils.checkIfMailExists(mailMsg)

            r = requests.post(
                "https://api.neverbounce.com/v4/single/check?key=" + apiKey + "&email=" + mailMsg + "").json()
            success = r['result']

            if 'invalid' in success:
                return jsonify({'error' : "invalid email address"})
            elif mailExists == True:
                return jsonify({'error' : "this account already exists"})
            elif not utils.checkPass(request.json['password']):
                return jsonify({'error': "password must contain at least 8 characters (including one upper and one lowercase, one number and one special character)"})
            else:
                stored_password = utils.hash_password(request.json['password'])
                utils.createAccountDB(mailMsg, password=stored_password)
                utils.sendVerificationMail(mailMsg)
                return jsonify({'message': "account created, verification pending"})
    except ValueError:
        return jsonify({'error': "invalid process"})

@app.route('/api/v1.0/login', methods=['POST'])
#@cross_origin(supports_credentials=True)
def login_user():
    try:
        if not request.json or not 'mail' in request.json or not 'password' in request.json:
            return jsonify({'error' : "data missing"})
        else:
            print("1")
            verification = utils.verify_password(request.json['mail'], request.json['password'])
            print("2")
            verified = utils.getVerified(request.json['mail'])
            print("3")
            if verification == False:
                return jsonify({'error' : "invalid credentials"})
            elif verified == False:
                return jsonify({'error': "user not verified yet"})
            else:
                token = utils.generateToken(request.json['mail'])
                print(token)
                if request.json['mail'] == 'juanba.tomas@gmail.com' or request.json['mail'] == 'Julian.b@bitcoin.org.hk':
                    return jsonify({'token' : token, 'user_type' : 'admin'})
                else:
                    return jsonify({'token' : token, 'user_type' : 'normal'})
    except:
        return jsonify({'error': "invalid credentials"})

@app.route('/api/v1.0/sendCredits', methods=['POST'])
#@cross_origin(supports_credentials=True)
def sendCredits():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            if not request.json or not 'mail' in request.json or not 'credits' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                receiver_id = utils.checkIfreceiverMailExists(request.json['mail'])
                if (receiver_id is None):
                    return jsonify({'Message' : "Email does not correspond to any user"})
                else:
                    creditsToSend = request.json['credits']
                    creditsInAccount = utils.getAvailableCredits(user_id)
                    mailMsg = utils.getUserMail(receiver_id)

                    if (int(creditsInAccount) >= int(creditsToSend)):
                        utils.sendCreditsToDB(user_id, receiver_id, creditsToSend)
                        creditsInAccountRec = utils.getAvailableCredits(receiver_id)
                        accountState = int(utils.getAccountState(receiver_id))

                        if (accountState < 2):
                            print("Creting docker")
                            # After receiving credits, the bot is properly created
                            apiKey = 'ss'
                            apiSecret = 'ss'

                            utils.createAccount(apiKey, apiSecret, receiver_id)

                        return jsonify({'Message': "User "+str(mailMsg)+" received "+str(creditsToSend)+" credits correctly."})

    except SyntaxError:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/run', methods=['POST'])
#@cross_origin(supports_credentials=True)
def run():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            userExists = utils.checkIfreceiverIdExists(user_id)
            if userExists <= 1:
                return jsonify({'error' : "User needs to insert API keys first"})
            else:
                active_credits = utils.getCreditsActive(user_id)

                # Let's start the process and inform the user
                if active_credits > 0:
                    execSh = PATH + '/start ' + str(user_id)
                    utils.refreshStopState(user_id, 0)
                    process2 = subprocess.Popen(execSh.split(), stdout=subprocess.PIPE)
                    output2, error = process2.communicate()
                    if 'bot already running' in str(output2):
                        return jsonify({'Message': "Bot already running previously"})
                    else:
                        return jsonify({'Message': "Bot running"})
                else:
                    return jsonify({'error': "User does not have credits. Can't run BOT."})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/stop', methods=['POST'])
#@cross_origin(supports_credentials=True)
def stop():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            userExists = utils.checkIfreceiverIdExists(user_id)
            if userExists == 0:
                return jsonify({'error' : "User profile does not exist yet"})
            elif userExists == 1:
                return jsonify({'error' : "User needs to set up API keys first"})
            else:
                bashCommand = PATH + '/status ' + str(user_id)
                process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()

                if 'not currently running' in str(output) or 'overloaded' in str(output):
                    return jsonify({'error': "Bot not running. Cannot stop."})
                else:
                    # Let's start the process and inform the user
                    execSh = PATH + '/kill ' + str(user_id)
                    rep = utils.closePos(user_id)
                    subprocess.call([execSh], shell=True)

                    if request.json and 'closePos' in request.json and int(request.json['closePos']) == 1:
                        rep = utils.closePosYES(user_id)

                        times = 0
                        while rep == -2:
                            rep = utils.closePosYES(user_id)
                            times += 1
                            if times == 5:
                                break

                        if rep == -2:
                            return jsonify({'WARNING': "Bot stopped. Positions NOT CLOSED: account blocked by BitMex."})
                        elif rep == -1:
                            return jsonify({'WARNING': "Bot stopped. Positions NOT CLOSED: API keys not valid."})
                        elif rep == 0:
                            return jsonify({'WARNING': "Bot stopped. Positions NOT CLOSED: account was not created yet."})
                        else:
                            return jsonify({'WARNING': "Bot stopped. Positions CLOSED."})
                    else:
                        return jsonify({'Message': "Bot stopped. Positions CLOSED SUCCESSFULLY"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/refresh', methods=['POST'])
#@cross_origin(supports_credentials=True)
def refresh():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            state = int(utils.getAccountState(user_id))

            if state == 1:
                return jsonify({'error': "User needs to set up API keys before continuing"})
            else:
                bitcoin = float(utils.fetch_balance(user_id)) / 1e8
                status, creds, balance, needed = utils.moveCreditsFromOpen(user_id, bitcoin)

                # Current price of bitcoin
                priceBitcoin = utils.getBTCUSDprice()

                startStepValue = math.floor(float(priceBitcoin) * bitcoin * 0.03)

                dictChange = {
                    'ORDER_START_SIZE': str(startStepValue),
                    'ORDER_STEP_SIZE': str(startStepValue)
                }

                if status == 1:
                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Settings were adapted correctly. Consumed " + str(
                        needed) + " additional credits, according to current balance: " + str(balance)})
                elif status == 2:
                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Settings were adapted correctly. Consumed " + str(
                        needed) + " additional credits, according to current balance: " + str(balance)})
                elif status == 3:
                    return jsonify({'Message': "Exact same number of credits corresponding to current balance. No need to refresh."})
                else:
                    return jsonify({'error': "Problem when refreshing. Contact agent."})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/closeSign', methods=['POST'])
#@cross_origin(supports_credentials=True)
def closeSign():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if (user_id == 13053900 or user_id == 423136569):
                if not request.json or 'type' not in request.json:
                    return jsonify({'error': "data missing"})
                else:
                    status = utils.closeSign(request.json['type'])
                    if status:
                        return jsonify({'Message': "closeSign running, wait for a couple of minutes to complete "})
                    else:
                        return jsonify({'error': "type provided is not correct"})
            else:
                return jsonify({'error': "User must be admin to run this command"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/delete', methods=['DELETE'])
#@cross_origin(supports_credentials=True)
def delete():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        #elif not utils.getDockerStatus(user_id):
        #    return jsonify({'error': "docker container not existing"})
        else:
            #userExists = utils.checkIfreceiverIdExists(user_id)
            #if userExists <= 1:
            #    return jsonify({'error' : "User needs to insert API keys first"})
            #else:
            utils.deleteContainerForUser(user_id)
            # Let's remove from database
            utils.removeFromDB(user_id)
            return jsonify({'Message': "Account deleted correctly"})

    except ValueError:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/updateKeys', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateKeys():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if not request.json or not 'key' in request.json or not 'secret' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                credits = utils.checkIfUserHasCredits(user_id)
                if credits == 0:
                    return jsonify({'Message' : "User does not have credits."})
                else:
                    dictChange = {
                        'API_KEY': request.json['key'],
                        'API_SECRET': request.json['secret']
                    }
                    balance = utils.fetch_balanceWithKeys(request.json['key'], request.json['secret'])
                    if balance == -1:
                        return jsonify({'Message': "API keys NOT LEGIT"})
                    utils.updateSettings(user_id, dictChange)
                    if utils.checkIfreceiverIdExists(user_id) == 1:
                        utils.setAccountState(user_id, 2)
                        utils.refreshCredits(user_id)
                    return jsonify({'Message': "Settings correctly updated"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/updateLeverage', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateLeverage():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if not request.json or not 'leverage' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                userExists = utils.checkIfreceiverIdExists(user_id)

                if userExists == 0:
                    return jsonify({'Message': "Profile does not exist"})
                elif userExists == 1:
                    return jsonify({'Message': "User needs to set up API keys first"})
                else:
                    leverageValue = request.json['leverage']

                    bitcoin = float(utils.fetch_balance(user_id) / 1e8)
                    priceBitcoin = utils.getBTCUSDprice()

                    if (int(leverageValue) > 0 and int(leverageValue) < 6):
                        startStepValue = math.floor(float(priceBitcoin) * bitcoin * 0.03)

                        dictChange = {
                            'ORDER_START_SIZE': str(startStepValue),
                            'ORDER_STEP_SIZE': str(startStepValue)
                        }
                        utils.updateSettings(user_id, dictChange)
                        res = utils.updateLeverage(user_id, leverageValue)

                        return jsonify({'Message': "Leverage updated correctly"})
                    else:
                        return jsonify({'Message': "Leverage NOT updated"})


    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/updateVolatility', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateVolatility():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if not request.json or not 'volatility' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                volatility = int(request.json['volatility'])

                if volatility == 1:
                    dictChange = {
                        'INTERVAL': str(0.01),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Volatility updated to HIGH = 0.01"})

                elif volatility == 0:
                    dictChange = {
                        'INTERVAL': str(0.0049),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Volatility updated to MEDIUM = 0.0049"})

                elif volatility == -1:
                    dictChange = {
                        'INTERVAL': str(0.0025),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Volatility updated to HIGH = 0.0025"})
                else:
                    return jsonify({'error': "invalid value for volatility"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/updateLoopInterval', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateLoopInterval():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if not request.json or not 'loopInterval' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                loopInterval = int(request.json['loopInterval'])

                if isinstance(loopInterval, float):
                    dictChange = {
                        'LOOP_INTERVAL': str(loopInterval),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Loop interval correctly updated"})
                else:
                    return jsonify({'error': "invalid value for loop interval"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/minSpread', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateMinSpread():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            if not request.json or not 'minSpread' in request.json:
                return jsonify({'error' : "data missing"})
            elif not utils.getDockerStatus(user_id):
                return jsonify({'error': "docker container not existing"})
            else:
                minSpread = float(request.json['minSpread'])
                print(str(minSpread))

                if isinstance(minSpread, float):
                    dictChange = {
                        'MIN_SPREAD': str(minSpread),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Min spread correctly updated"})
                else:
                    return jsonify({'error': "invalid value for min spread"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/bias', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updateBias():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            if not request.json or not 'bias' in request.json:
                return jsonify({'error' : "data missing"})
            elif not utils.getDockerStatus(user_id):
                return jsonify({'error': "docker container not existing"})
            else:
                bias = float(request.json['bias'])
                print(str(bias))

                if isinstance(bias, float):
                    dictChange = {
                        'PERK': str(bias),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Bias correctly updated"})
                else:
                    return jsonify({'error': "invalid value for min spread"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/adminRiskAll', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def adminRiskAll():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            if not request.json or not 'bias' in request.json or not 'orderpairs' in request.json:
                return jsonify({'error' : "data missing"})
            elif not utils.getDockerStatus(user_id):
                return jsonify({'error': "docker container not existing"})
            else:
                bias = float(request.json['bias'])
                orderPairs = float(request.json['orderpairs'])
                if abs(bias) > 2 or orderPairs < 1 or orderPairs > 6:
                    return jsonify({'error': "data not correctly set up"})
                else:
                    dictChange = {
                        'PERK': str(bias),
                        'ORDER_PAIRS': str(orderPairs)
                    }
                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "Bias and order pairs correctly updated"})
    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/orderPairs', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def orderPairs():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )
        print(str(date))
        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        elif not utils.getDockerStatus(user_id):
            return jsonify({'error': "docker container not existing"})
        else:
            if not request.json or not 'orderpairs' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                orderpairs = int(request.json['orderpairs'])
                print(str(orderpairs))

                if orderpairs > 0 and orderpairs < 7:
                    dictChange = {
                        'ORDER_PAIRS': str(orderpairs),
                    }

                    utils.updateSettings(user_id, dictChange)
                    return jsonify({'Message': "ORDER_PAIRS correctly updated"})
                else:
                    return jsonify({'error': "invalid value for min spread"})

    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/notifications', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def notifications():
    try:
        token = request.headers['Authorization']
        decoded = utils.decodeToken(token)
        mail = decoded['user_id']
        user_id = utils.getUserID(mail)
        nofrag, frag = decoded['expiration_time'].split('.')
        date = datetime.strptime(nofrag, '%Y-%m-%d %H:%M:%S' )

        if (date < datetime.now()):
            return jsonify({'error' : "invalid token"})
        else:
            if not request.json or not 'notifications' in request.json:
                return jsonify({'error' : "data missing"})
            else:
                notif = int(request.json['notifications'])
                if notif == 1:
                    utils.setNotifications(user_id, 1)
                    return jsonify({'Message': "User Telegram notifications ON"})
                elif notif == 0:
                    utils.setNotifications(user_id, 0)
                    return jsonify({'Message': "User Telegram notifications OFF"})
                else:
                    return jsonify({'Message': "Value must be either 0 or 1"})
    except:
        return jsonify({'error': "invalid token"})

@app.route('/api/v1.0/updatePass', methods=['PUT'])
#@cross_origin(supports_credentials=True)
def updatePass():
    try:
        if not request.json or not 'mail' in request.json or not 'password' in request.json:
            return jsonify({'error' : "data missing"})
        elif not utils.checkPass(request.json['password']):
            return jsonify({'error': "password must contain at least 8 characters (including one upper and one lowercase, one number and one special character)"})
        else:
            mail = request.json['mail']
            password = request.json['password']
            utils.sendPassUpdateMail(mail, password)
            return jsonify({'Message': "Update Password Mail submitted"})

    except:
        return jsonify({'error': "invalid call"})

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=12430)
