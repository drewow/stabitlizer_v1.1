#!/bin/bash

containers_ids="$(docker ps -a | grep 'Exited' | awk '{print $1}')"
ITER=0
for i in ${containers_ids[@]}; 
do
    container_name="$(docker ps -a | grep $i | awk '{print $NF}')"
    printf "${i}-${container_name}\n"
    docker start $i
done

