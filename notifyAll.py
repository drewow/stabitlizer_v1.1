#!/usr/bin/env python3.6

import datetime
import logging
import os
import subprocess
import sys
import time

import MySQLdb
import telegram

import utils

PATH = os.path.dirname(os.path.abspath(__file__))


"""
# Configure logging
"""
FORMAT = '%(asctime)s -- %(levelname)s -- %(module)s %(lineno)d -- %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('root')
fh = logging.FileHandler(utils.getBotMexDir()+'logs/licenseChecker.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

bot = telegram.Bot(token=utils.getBotID())

def main():
    setOfUsers = utils.getAllAccounts()
    logger.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + ": sending notification to users")
    message = "UPDATE TO LAST MESSAGE: Dear StaBitLizer user. Due to an important update in StaBitLizer, the bot will be down for maintenance from 5:50pm to 6:30pm (CEST) today. This update is aimed at reinforcing the security of the platform and introducing new functionalities that will improve usability and stability. Thanks."
    for row in setOfUsers:
        try:
            user_id = row[0]
            telegram_id = utils.getTelegramID(user_id)
            logger.info("=======================================")
            logger.info("Sending notification to user: " + str(telegram_id))
            bot.send_message(chat_id=telegram_id, text=message)
            logger.info("NOTIFICATION SENT===============================")
            sys.stdout.flush()
        except:
            logger.info("Notification not sent for this user")
            continue


if __name__ == "__main__":
    main()
