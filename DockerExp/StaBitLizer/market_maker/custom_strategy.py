import sys
import math
from market_maker.settings import settings

from market_maker.market_maker import OrderManager
import bitmex

class CustomOrderManager(OrderManager):
    """A sample order manager for implementing your own custom strategy"""

    def leverageUpd(self):

        leverageValue = 0 # CrossMaring
        typeOfNet = 1

        if 'testnet' in settings.BASE_URL:
            typeOfNet = 1
        else:
            typeOfNet = 0

        try:

            apiKey = settings.API_KEY #utils.getSettings(user_id,sett1).replace('"', '').strip()
            apiSecret = settings.API_SECRET #utils.getSettings(user_id,sett2).replace('"', '').strip()

            client = bitmex.bitmex(test=typeOfNet, api_key=apiKey, api_secret=apiSecret)
            res = client.Position.Position_updateLeverage(symbol='XBTUSD',leverage=leverageValue).result()
    
        except ValueError:
        
            print("Exception when calling PositionApi->position_update_leverage")

    def place_orders(self) -> None:
        # implement your custom strategy here

        buy_orders = []
        sell_orders = []
        perk = settings.PERK

        if perk is None:
            perkPos = 1
            perkNeg = 1
        else:
            perkNeg = perk[0]
            perkPos = perk[1]

        for i in reversed(range(1, settings.ORDER_PAIRS + 1)):
            if not self.long_position_limit_exceeded():
                buy_orders.append(self.prepare_order(-i, perkNeg))
            if not self.short_position_limit_exceeded():
                sell_orders.append(self.prepare_order(i, perkPos))

        self.converge_orders(buy_orders, sell_orders)


    def prepare_order(self, index, perk):
        """Create an order object."""

        if settings.RANDOM_ORDER_SIZE is True:
            quantity = math.ceil((1 + (abs(perk) - 1)) * (random.randint(settings.MIN_ORDER_SIZE, settings.MAX_ORDER_SIZE)))
        else:
            quantity = math.ceil((1 + (abs(perk) - 1)) * (settings.ORDER_START_SIZE + ((abs(index) - 1) * settings.ORDER_STEP_SIZE)))

        price = self.get_price_offset(index)

        return {'price': price, 'orderQty': quantity, 'side': "Buy" if index < 0 else "Sell"}


def run() -> None:
    order_manager = CustomOrderManager()

    # Try/except just keeps ctrl-c from printing an ugly stacktrace
    try:
        order_manager.run_loop()
    except (KeyboardInterrupt, SystemExit):
        sys.exit()
