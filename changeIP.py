#!/usr/bin/env python3.6

import boto3
import time
import utils
import bitmex
import urllib.request
import subprocess

def changeIP():

    client = boto3.client('ec2')
    addresses_dict = client.describe_addresses()
    response = client.describe_instances(InstanceIds=['i-03707c50ab33590d2'])
    ipRe = str(response['Reservations'][0]['Instances'][0]['PublicIpAddress'])
    for eip_dict in addresses_dict['Addresses']:
        print(str(eip_dict))
        if eip_dict['PublicIp'] == ipRe:
            print("Yes")
            addr = client.allocate_address(Domain='vpc')
            print(addr['PublicIp'])
            #contents = urllib.request.urlopen("http://4ceffe18.ngrok.io/ChangePageAUTO/changeIP.php?ip1="+str(ipRe)+"&ip2="+str(addr['PublicIp'])).read()
            response = client.associate_address(AllocationId=addr['AllocationId'], InstanceId='i-03707c50ab33590d2')
            response2 = client.release_address(AllocationId=eip_dict['AllocationId'])
            addresses_dict = client.describe_addresses()
            time.sleep(5)
            subprocess.Popen('/home/ubuntu/StaBitLizer_v1.1/introduceMailsWithAPICalls.py')
            for eip_dict in addresses_dict['Addresses']:
                print(eip_dict['PublicIp'])

def getLeverage():
   
    try: 
        releaseUnassigned()
        time.sleep(5)
        user_id = 13053900
        sett1 = 'API_KEY'
        sett2 = 'API_SECRET'
        apiKey = utils.getSettings(user_id,sett1).replace('"', '').strip()
        apiSecret = utils.getSettings(user_id,sett2).replace('"', '').strip()

        client = bitmex.bitmex(test=utils.getTypeOfNet(), api_key=apiKey, api_secret=apiSecret)
        print("Connected to BitMex as client: "+str(client))
        
        res = client.Position.Position_get().response().result
        lever = res[0]['leverage']

    except:
        changeIP()

def releaseUnassigned():

    client = boto3.client('ec2')
    addresses_dict = client.describe_addresses()
    print(str(addresses_dict))
    for eip_dict in addresses_dict['Addresses']:
        if "NetworkInterfaceId" not in eip_dict:
            print(eip_dict['PublicIp'])
            client.release_address(AllocationId=eip_dict['AllocationId'])

def main():
    getLeverage()

if __name__== "__main__":
    main()
