#!/usr/bin/env python3.6

import utils
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import subprocess
import os
import mmap
import datetime

def main():

    scope = ['https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/StaBitLizer_v1.1/client_secret.json', scope)
    client = gspread.authorize(creds)

    # Open a worksheet from spreadsheet with one shot
    sheet = client.open("SBLAccountStatusSummary")
    worksheet2 = sheet.worksheet('CreditsActivatedLog')

    transactions = utils.getAllTransactions()
    cells = []
    row_num = 1
    for transaction in transactions:
        f = '%Y-%m-%d %H:%M:%S'
        
        dateTransaction = str(transaction[1])
        dateTransaction = datetime.datetime.strptime(dateTransaction, f)
        dateTransaction_day = dateTransaction.day
        
        user_id = transaction[2]
        mail = utils.getUserMail(user_id)
        
        creation_date = str(utils.getAccountCreationDate(user_id))
        creation_date = datetime.datetime.strptime(creation_date, f)
        creation_day = creation_date.day


        if transaction[4] == 2:
            credits = -transaction[3]
        else:
            credits = transaction[3]
        
        if dateTransaction_day == creation_day:
            sortOfRefresh = 'Cycle completed'
        else:
            sortOfRefresh = '/refresh command issued'

        cells.append(gspread.Cell(row_num + 1, 1, str(user_id)))
        cells.append(gspread.Cell(row_num + 1, 2, mail))
        cells.append(gspread.Cell(row_num + 1, 3, str(dateTransaction)))
        cells.append(gspread.Cell(row_num + 1, 4, str(credits)))
        cells.append(gspread.Cell(row_num + 1, 5, str(sortOfRefresh)))


        row_num += 1

    worksheet2.update_cells(cells)

if __name__ == '__main__':
    main()
