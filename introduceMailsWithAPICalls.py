#!/usr/bin/env python3.6

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from requests import get

import utils


def main():

    my_ip = get('https://api.ipify.org').text
    scope = ['https://www.googleapis.com/auth/drive']
    apiAdd = 'http://'+str(my_ip)+':5000/api/v1.0/graphsBalance/'
    creds = ServiceAccountCredentials.from_json_keyfile_name('/home/ubuntu/StaBitLizer_v1.1/client_secret.json', scope)
    client = gspread.authorize(creds)

    # Open a worksheet from spreadsheet with one shot
    sheet = client.open("SBLAccountStatusSummary")
    worksheet2 = sheet.worksheet('SBLBalancesGraphs')

    users = utils.getAllAccountsAndMails()
    cells = []
    row_num = 1
    for user in users:
        
        user_id = int(user[0])
        mail = str(user[1])
        df = utils.getAnalytics(user_id)
        balance = df.loc[df.index.max()]['balance']
        if balance == -1:
            balance = "-"
        else:
            balance = balance / 1e8

        cells.append(gspread.Cell(row_num + 1, 1, '=HYPERLINK("'+apiAdd+str(user_id)+'","'+mail+'")'))
        cells.append(gspread.Cell(row_num + 1, 3, str(balance)))

        row_num += 1

    worksheet2.update_cells(cells, value_input_option='USER_ENTERED')

if __name__ == '__main__':
    main()
