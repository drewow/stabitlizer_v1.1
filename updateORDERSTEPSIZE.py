#!/usr/bin/env python3.6

import math
import os
import sys

import utils

PATH = os.path.dirname(os.path.abspath(__file__))


def stepSizeUpd(user_id):

    try:

        balance = utils.fetch_balance(user_id)
        priceBitCoin = utils.getBTCUSDprice()

        value = math.floor(balance/1e8 * priceBitCoin * 0.03)

        if value == 0:
            value = 1

        dictChange = {
            'ORDER_START_SIZE':str(value),
            'ORDER_STEP_SIZE':str(value)
        }
        print("Balance and value: "+str(balance)+", "+str(value))
        utils.updateSettings(user_id, dictChange)

    except ValueError:
        print("Exception when calling PositionApi->position_update_leverage")

def main():
    setOfUsers = utils.getAllAccounts()
    userNum = 0
    for row in setOfUsers:
        user_id = row[0]
        userNum += 1
        print("--------------------------------------")
        print("Checking license for user: "+str(user_id))
        try:
            print("Updating for user: "+str(user_id))
            stepSizeUpd(user_id)
            print("Now with user: "+str(userNum))
            print("--------------------------------------")
        except:
            print("Continuing for user: "+str(user_id))
            print("Now with user: "+str(userNum))
            print("--------------------------------------")
            continue
    sys.stdout.flush() 
  
if __name__== "__main__":
    main()
